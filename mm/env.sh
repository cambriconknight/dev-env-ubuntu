# -------------------------------------------------------------------------------
# Filename:    env.sh
# Revision:    1.0.0
# Date:        2024/11/11
# Description: Common Environment variable
# Example:
# Depends:
# Notes:
# -------------------------------------------------------------------------------
#################### version ####################
## 以下信息,根据各个版本中文件实际名词填写.
#Version
VER="v24.10-magicmind1.13"
Owner="kang"
TestModel=""
#################### docker ####################
#Work
PATH_WORK="magicmind_container"
VERSION="${VER}-magicmind_container1.0.0-x86_64-ubuntu22.04-py310"
#Docker image
MY_IMAGE="magicmind_container/release/x86_64/magicmind_container"
#Docker image name
NAME_IMAGE="${MY_IMAGE}:${VERSION}"
#FileName DockerImage  magicmind_container-v24.10-magicmind1.13-magicmind_container1.0.0-x86_64-ubuntu22.04-py310.tar.gz
FILENAME_IMAGE="${PATH_WORK}-${VERSION}.tar.gz"
FULLNAME_IMAGE="./docker/${FILENAME_IMAGE}"
#Docker container name
MY_CONTAINER="${PATH_WORK}-${VER}${TestModel}-${Owner}"
#################### color ####################
#Font color
none="\033[0m"
black="\033[0;30m"
dark_gray="\033[1;30m"
blue="\033[0;34m"
light_blue="\033[1;34m"
green="\033[0;32m"
light_green="\033[1;32m"
cyan="\033[0;36m"
light_cyan="\033[1;36m"
red="\033[0;31m"
light_red="\033[1;31m"
purple="\033[0;35m"
light_purple="\033[1;35m"
brown="\033[0;33m"
yellow="\033[1;33m"
light_gray="\033[0;37m"
white="\033[1;37m"
