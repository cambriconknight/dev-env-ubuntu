#!/bin/bash
set -ex

# 创建build目录
if [ ! -d "./build" ]; then mkdir -p "./build";fi
pushd build
cmake .. && make
popd