#!/bin/bash
set -ex
# build
gcc example4cndev.c \
    -fPIC -shared -std=c99 \
    -I ${NEUWARE_HOME}/include \
    -L ${NEUWARE_HOME}/lib64 \
    -lcndev \
    -o libexample4cndev.so
