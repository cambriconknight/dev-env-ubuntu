import ctypes
from ctypes import *

libexample4cndev = ctypes.CDLL("./libexample4cndev.so")

def example():
    libexample4cndev.DevInit()
    # devId
    handle = libexample4cndev.MLU_init(0)
    # get cardname by devId
    libexample4cndev.GetDevName.restype = c_char_p
    cardName = libexample4cndev.GetDevName(handle)
    print("CardName: {} ;".format(cardName.decode()))
    # get card[x]'s SN
    libexample4cndev.GetCardSN.restype = c_char_p
    cardSN = libexample4cndev.GetCardSN(handle)
    print("CardSN: {} ;".format(cardSN.decode()))
    # get card[x]'s TotalMemory
    totalTotalMemory =libexample4cndev.GetTotalMemoryInfo(handle)
    print("TotalTotalMemory: {}MB ;".format(totalTotalMemory))
    # get card[x]'s UsedMemory
    usedMemoryInfo =libexample4cndev.GetUsedMemoryInfo(handle)
    print("UsedMemoryInfo: {}MB ;".format(usedMemoryInfo))
    # get card[x]'s power usage info
    powerUsage = libexample4cndev.GetPowerUsage(handle)
    print("PowerUsage: {} W ;".format(powerUsage))
    # get card[x]'s MLU utilization
    cardUtilization = libexample4cndev.GetUtilization(handle)
    print("CardUtilization: {}% ;".format(cardUtilization))

if __name__ == "__main__":
    example()