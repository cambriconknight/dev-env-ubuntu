<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/mm/cndev">
        <h1 align="center">CNDev设备管理接口验证教程</h1>
    </a>
</p>

**该教程仅仅用于学习，打通流程； 不对效果负责，不承诺商用。**

# 1. 概述

[CNDev](https://www.cambricon.com/docs/sdk_1.15.0/cntoolkit_3.7.2/cndev_3.7.0/index.html) 是一套支持主机端应用程序获取MLU芯片硬件信息的软件接口。 通过CNDev接口，用户可以对设备管理实现定制化需求，比如云服务平台的设备管理、自动化测试平台的设备管理等。 [CTypes](https://docs.python.org/zh-cn/3/library/ctypes.html#module-ctypes) 是 Python 的外部函数库。它提供了与 C 兼容的数据类型，并允许调用 DLL 或共享库中的函数。

[本文中Python样例](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/mm/cndev#32-python%E6%A0%B7%E4%BE%8B) 是使用 CTypes 模块以纯 Python 形式对 CNDev 库进行封装， 并基于此做了简单的几个接口封装实例，如有其他需要可自行开发扩展。

CNDev主要功能：
- 获取设备硬件信息，比如：设备名称、设备利用率、板卡功耗、板卡温度、板卡带宽、风扇转速等。
- 评测应用程序在寒武纪硬件设备上的开销，比如：内存占用、MLU利用率等。
- 提供获取设备间拓扑逻辑以及CPU亲和性等相关信息的功能。
- 获取板间通讯MLU-Link相关信息。比如：MLU-Link连接速度、MLU-Link连接状态、以及MLU-Link连接设备等相关信息。

参考资料：
- [CNDev开发者手册](https://www.cambricon.com/docs/sdk_1.15.0/cntoolkit_3.7.2/cndev_3.7.0/index.html)
- [ctypes教程](https://docs.python.org/zh-cn/3.7/library/ctypes.html)
- [使用ctypes访问C代码](https://python3-cookbook.readthedocs.io/zh-cn/latest/c15/p01_access_ccode_using_ctypes.html)

# 2. 环境搭建

## 2.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 开发主机/服务器  | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| MLU370        | 一套       |机箱风扇需满足MLU370板卡被动散热要求|

## 2.2. 软件环境

| 名称                   | 版本/文件                           | 备注                                 |
| :-------------------- | :-------------------------------   | :---------------------------------- |
| Linux OS              | Ubuntu/CentOS                      | 宿主机操作系统                         |
| Driver_MLU370         | cambricon-mlu-driver-centos7-5.10.31-1.x86_64.rpm | 依操作系统选择                         |
| Docker Image          | magicmind_1.10.0-1_ubuntu22.04.tar.gz     | 官方发布的 MagicMind 框架 Docker 镜像文件 |
| SDK                   | cntoolkit_3.10.2-1.ubuntu22.04_amd64.deb  | 已内置与官方镜像，也可联系官方技术服务人员获取单独安装包 |

注: 以上软件环境中文件名词, 如有版本升级及名称变化, 可以在 [env.sh](./env.sh) 中进行修改。

## 2.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/mm
```
## 2.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/mm
#加载Docker镜像
#./load-image-dev.sh ./dependent_files/magicmind_1.10.0-1_ubuntu22.04.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 2.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【mm工具集目录】
cd ./dev-env-ubuntu/mm
#启动容器
./run-container-dev.sh
```
以上基于官方正式发布的Docker镜像，方便后续直接测试验证。

# 3. 运行样例

以下操作步骤均是在官方提供的Docker容器中进行。

## 3.1. C样例

C 样例运行实例如下：

```bash
#进入测试目录
cd /home/share/mm/cndev/c
#清理
./clean.sh
#编译
./build.sh
#运行样例
./build/devicequery
```

## 3.2. Python样例

Python 样例运行实例如下：

```bash
#进入测试目录
cd /home/share/mm/cndev/python
#清理并编译
./clean.sh && ./build.sh
#运行样例
python example4cndev.py
```

**运行实例**
```bash
root@worker1:/home/share/mm/cndev/python# ./clean.sh && ./build.sh
removed 'libexample4cndev.so'
+ gcc example4cndev.c -fPIC -shared -std=c99 -I /usr/local/neuware/include -L /usr/local/neuware/lib64 -lcndev -o libexample4cndev.so
root@worker1:/home/share/mm/cndev/python# python example4cndev.py
CardName: MLU370-X8 ;
CardSN: 542207300057 ;
TotalTotalMemory: 23374MB ;
UsedMemoryInfo: 18772MB ;
PowerUsage: 107 W ;
CardUtilization: 0% ;
```
