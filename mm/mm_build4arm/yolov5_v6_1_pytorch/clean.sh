#!/bin/bash
set -e

rm -rvf ./export_model/yolov5
rm -rvf ./data/models/*
