# YOLOv5_PyTorch

MagicMind 是面向寒武纪 MLU 的推理加速引擎。MagicMind 能将 AI 框架(Tensorflow,PyTorch,ONNX 等) 训练好的算法模型转换成 MagicMind 统一计算图表示,并提供端到端的模型优化、代码生成以及推理业务部署能力。

本 sample 探讨如何使用将 yolov5 网络的 PyTorch 实现转换为 MagicMind 模型，进而部署在寒武纪 MLU 板卡上。

## 目录

- [YOLOv5\_PyTorch](#yolov5_pytorch)
  - [目录](#目录)
  - [1. 模型概述](#1-模型概述)
  - [2. 前提条件](#2-前提条件)
  - [3. 快速使用](#3-快速使用)
    - [3.1 环境准备](#31-环境准备)
    - [3.2 下载仓库](#32-下载仓库)
    - [3.3 准备数据集和模型](#33-准备数据集和模型)
    - [3.4 编译 MagicMind 模型](#34-编译-magicmind-模型)

## 1. 模型概述

本例使用的 yolov5 实现来自 github 开源项目[https://github.com/ultralytics/yolov5](https://github.com/ultralytics/yolov5) 中的 v6.1 版本。下面将展示如何将该项目中 PyTorch 实现的 yolov5 模型转换为 MagicMind 的模型。

## 2. 前提条件

请移至[主页面 README.md](../../../../README.md)的`2.前提条件`

## 3. 快速使用

### 3.1 环境准备

请移至[主页面 README.md](../../../../README.md)的`3.环境准备`

### 3.2 下载仓库

```bash
# 下载仓库
git clone 本仓库
cd magicmind_cloud/buildin/cv/detection/yolov5_v6_1_pytorch
```

在开始运行代码前需要执行以下命令安装依赖：

```bash
pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple/
```

在开始运行代码前需要先检查 `env.sh` 里的环境变量，根据数据集实际路径修改 `env.sh` 内的 `COCO_DATASETS_PATH`, 并且执行以下命令：

```bash
# for instance, COCO_DATASETS_PATH is : /home/your_path/coco_datasets
#export COCO_DATASETS_PATH=/data/datasets/COCO
source env.sh
```

### 3.3 准备数据集和模型

```bash
#1.核对并修改脚本 ${PROJ_ROOT_PATH}/export_model/get_datasets_and_models.sh 中模型类别 yolov5s.pt yolov5m.pt .....
#2.核对并修改脚本 ${PROJ_ROOT_PATH}/export_model/run.sh 中模型类别 yolov5s.pt yolov5m.pt .....
cd ${PROJ_ROOT_PATH}/export_model
bash run.sh
```

### 3.4 编译 MagicMind 模型

```bash
#1.核对并修改脚本 ${PROJ_ROOT_PATH}/gen_model/run.sh 中模型类别 yolov5s_traced.pt .....
#2.修改脚本 ${PROJ_ROOT_PATH}/gen_model/gen_model.py 和 ${PROJ_ROOT_PATH}/gen_model/run.sh 增加cross_compile_toolchain_path target_host_arch 参数

cd ${PROJ_ROOT_PATH}/gen_model
# bash run.sh <magicmind_model> <precision> <batch_size> <dynamic_shape>
# 指定您想输出的magicmind_model路径，例如./model
#bash run.sh ${magicmind_model} force_float32 1 true
#bash run.sh ${magicmind_model} force_float16 1 true
bash run.sh ../data/models/yolov5s_arm_force_float16.mm force_float16 1 true
ls -la ../data/models/yolov5s_arm_force_float16.mm
```
