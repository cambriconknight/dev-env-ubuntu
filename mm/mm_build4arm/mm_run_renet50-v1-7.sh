#!/bin/bash
#set -ex
set -e

#export LD_LIBRARY_PATH=../lib64:${LD_LIBRARY_PATH}
#MODEL_FILE_PATH="resnet50_"$precision_type"_"$batch_num".mm"
device_id="0"
model_name="resnet50-v1-7"
model_name_onnx="${model_name}.onnx"
model_name_mm="${model_name}_force_float32_b1_mm"
array_precision_type=(force_float32 force_float16 qint8_mixed_float16)
array_bs=(1 16 32)

#mm_run --magicmind_model resnet50-v1-7_force_float32_b1.mm --batch_size 1 --devices 0 --threads 1 --iterations 1000
for precision_type in "${array_precision_type[@]}"; do
    for batch_num in "${array_bs[@]}"; do
        echo "======================================================="
        model_name_mm="${model_name}_${precision_type}_b${batch_num}_mm"
        log_filename="${model_name}_${precision_type}_b${batch_num}_mm_run.log"
        if [ ! -f ${model_name_mm} ];then echo "check file: ${model_name_mm}"; fi
        echo "mm_run: ${model_name_mm}"
        #echo "batch:'${batch_num}'; precision:'${precision_type} "
        #mm_run --magicmind_model ${model_name_mm} --input_dims 1,3,224,224 --iterations 100 --threads 1 --bind_cluster 0 &> ${log_filename}
        mm_run --magicmind_model ${model_name_mm} --batch_size ${batch_num} --devices ${device_id} \
            --iterations 1000 --threads 1 --bind_cluster 0 &> ${log_filename}
        #   --iterations 1000 --threads 1 --bind_cluster 0 2>&1 | tee ${log_filename}
        #检索关键信息
        model=(`grep -w magicmind_model ${log_filename}`)
        qps=(`grep -w qps ${log_filename}`)
        n=`grep -wn "MLU Compute (Launch jobs) Summary:" ${log_filename} | cut -f1 -d:`
        line=$(($n + 1))
        latency=(`head -${line} ${log_filename} | tail -1`)
        res='model name:'${model[2]}', qps:'${qps[2]}', mean:'${latency[8]}', min:'${latency[4]}', max:'${latency[6]}''
        echo $res
        echo "mm_run done: ${model_name_mm}"
        #echo "======================================================="
    done
done
