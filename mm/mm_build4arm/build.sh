export LD_LIBRARY_PATH=../lib64:${LD_LIBRARY_PATH}
MODEL_FILE_PATH="./model"

if [ -f ${MODEL_FILE_PATH} ];then
  rm ${MODEL_FILE_PATH}
fi

./onnx_build --onnx resnet50-v1-7.onnx --build_config config.json

if [ ! -f ${MODEL_FILE_PATH} ];then
  echo "build failed!"
else
  echo "build success"
fi
