<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/mm/mm_build4arm">
        <h1 align="center">ARM 版 MagicMind 模型转换说明</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 概述](#1-概述)
- [2. X86 平台操作说明](#2-x86-平台操作说明)
  - [2.1. 硬件环境](#21-硬件环境)
  - [2.2. 软件环境](#22-软件环境)
  - [2.3. 下载仓库](#23-下载仓库)
  - [2.4. 加载镜像](#24-加载镜像)
  - [2.5. 启动容器](#25-启动容器)
  - [2.6. 模型转换](#26-模型转换)
    - [2.6.1 mm\_build工具进行模型转换](#261-mm_build工具进行模型转换)
    - [2.6.2 modelzoo代码进行模型转换](#262-modelzoo代码进行模型转换)
- [3. ARM 平台操作说明](#3-arm-平台操作说明)
  - [3.1. 硬件环境](#31-硬件环境)
  - [3.2. 软件环境](#32-软件环境)
  - [3.3. 下载仓库](#33-下载仓库)
  - [3.4. 加载镜像](#34-加载镜像)
  - [3.5. 启动容器](#35-启动容器)
  - [3.6. 模型验证](#36-模型验证)
    - [3.6.1 resnet50模型验证](#361-resnet50模型验证)
    - [3.6.2 YOLOv5模型验证](#362-yolov5模型验证)

# 1. 概述

本目录以ONNX框架模型为例, 展示如何在 X86 服务器上转换出 ARM 平台可用的 MagicMind 模型，并使用 mm_run 进行性能压测。 主要步骤如下：
- 在 `X86 服务器`上基于官方发布的 docker 镜像环境，`编译 mm_build 系列工具`；
- 基于以上 `X86 服务器`环境，使用 `onnx_build `工具及相应json配置文件， 转出 ARM 平台可用的 `MagicMind 模型`；
- 在 `ARM 服务器`上基于官方发布的 ARM 版 docker 镜像环境， 使用自带的 `mm_run` 工具，对 MagicMind 模型进行`性能压测`；

以上验证过程，涉及到的官方镜像文件如下：
- X86 平台镜像（用于转出mm模型）： `magicmind_container-v24.10-magicmind1.13-magicmind_container1.0.0-x86_64-ubuntu22.04-py310.tar.gz`
- ARM 平台镜像（最终mm部署环境）： `magicmind_1.10.0-1_anolis8.8.tar.gz`

# 2. X86 平台操作说明

参考如下步骤在 X86 平台上对 ONNX模型进行 ARM 版 MagicMind 模型转换，转出的模型进行后续的 ARM 平台模型部署。

## 2.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| X86服务器      | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| MLU370        | 至少一张卡  | X8需使用板卡自带的8pin连接器连接主板电源 |

## 2.2. 软件环境

| 名称                   | 版本/文件                                                | 备注                                  |
| :-------------------- | :-------------------------------                        | :----------------------------------  |
| Linux OS              | Ubuntu22.04/CentOS7等                                    | 宿主机操作系统                  |
| Driver                | cambricon-mlu-driver-6.2.5-6.el7.x86_64.rpm              | 依操作系统选择         |
| Docker Image          | magicmind_container-v24.10-magicmind1.13-magicmind_container1.0.0-x86_64-ubuntu22.04-py310.tar.gz  | 官方发布的 MagicMind 框架 Docker 镜像文件 |
| 交叉编译器              | gcc-x86_64_aarch64-linux-gnu.tar.gz                     | 联系官方技术支持人员获取 |
| modelzoo模型转换代码     | [gitee:magicmind_cloud](https://gitee.com/cambricon/magicmind_cloud.git) 或 magicmind_cloud_commit72e35.tar.gz                     | 联系官方技术支持人员获取最新版本 |

注: 以上软件环境中文件名词, 如有版本升级及名称变化, 可以在 [env.sh](./env.sh) 中进行修改。

**参考资料:**
- 前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。
- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch

## 2.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
```
## 2.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/mm
#加载Docker镜像
#./load-image-dev.sh ./docker/magicmind_container-v24.10-magicmind1.13-magicmind_container1.0.0-x86_64-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 2.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/mm
#启动Docker容器
./run-container-dev.sh
```

## 2.6. 模型转换

由于要在ARM平台上做验证，所以做模型转换时，需要使用交叉编译工具链。

**下载并设置交叉编译工具链**
```bash
# 1. 下载gcc-x86_64_aarch64-linux-gnu并解压到当前目录（可联系官方技术支持人员获取）
cd /home/share/mm/mm_build4arm
tar zxvf gcc-x86_64_aarch64-linux-gnu.tar.gz
#后续可直接使用使用这个环境变量 ${CROSS_COMPILE_TOOLCHAIN_PATH}
export CROSS_COMPILE_TOOLCHAIN_PATH=/home/share/mm/mm_build4arm/gcc-linaro-6.2.1-2016.11-x86_64_aarch64-linux-gnu
```

### 2.6.1 mm_build工具进行模型转换

进入Docker容器后，就可以参考如下步骤进行模型转换，以[resnet50-v1-7.onnx模型](https://github.com/onnx/models/blob/main/validated/vision/classification/resnet/model/resnet50-v1-7.onnx)为例。

*注：以下操作步骤均是在官方提供的Docker容器中进行*

```bash
# 1. 编译mm_build
mkdir -p /usr/local/neuware/samples/magicmind/mm_build/build
cd /usr/local/neuware/samples/magicmind/mm_build/build
cmake .. && make
# 2. 设置软链接（此步骤可以忽略）
cd /home/share/mm/mm_build4arm
ln -s /usr/local/neuware/samples/magicmind/mm_build/build/onnx_build onnx_build
ln -s /usr/local/neuware/samples/magicmind/mm_build/build/pytorch_build pytorch_build
# 3. 下载测试模型 以 resnet50-v1-7.onnx 为例验证 https://github.com/onnx/models/blob/main/validated/vision/classification/resnet/model/resnet50-v1-7.onnx
wget https://github.com/onnx/models/blob/main/validated/vision/classification/resnet/model/resnet50-v1-7.onnx
# 4. 转换mm模型
./onnx_build --onnx resnet50-v1-7.onnx --build_config config.json
#也可参考以下一键转换 mm 模型，生成多种格式的 mm 模型，方便后期性能对比测试。
./build_renet50-v1-7.sh
```

### 2.6.2 modelzoo代码进行模型转换

进入Docker容器后，就可以参考如下步骤进行模型转换。

*注：以下操作步骤均是在官方提供的Docker容器中进行*

**下载ModelZoo仓库**
```bash
#进入对应目录
cd /home/share/mm
#下载仓库或压缩包magicmind_cloud_commit72e35.tar.gz
#git clone https://gitee.com/cambricon/magicmind_cloud.git
#cd magicmind_cloud && git checkout 72e35c1fadec4730a16523fd3138552f4ce385ed && cd -
#下载压缩包magicmind_cloud_commit72e35.tar.gz并解压到当前目录（可联系官方技术支持人员获取）
tar zxvf ./magicmind_cloud_commit72e35.tar.gz
ls -la /home/share/mm/magicmind_cloud
```
仓库下载完成后，参考仓库中 [README.md](https://gitee.com/cambricon/magicmind_cloud) 进行各个网络移植验证。

以下以 [yolov5_v6_1_pytorch](https://gitee.com/cambricon/magicmind_cloud/tree/master/buildin/cv/detection/yolov5_v6_1_pytorch) 模型为例进行说明。
```bash
# 1. 更新 yolov5_v6_1_pytorch 代码, 主要是增加交叉编译的选项。此外模型更改为例，yolov5m更改为yolov5s
cp -rvf /home/share/mm/mm_build4arm/yolov5_v6_1_pytorch /home/share/mm/magicmind_cloud/buildin/cv/detection/
# 2. 关闭tfu
vim /home/share/mm/magicmind_cloud/buildin/python_common/model_process.py
#取消541行注释：#assert config.parse_from_string('{"debug_config": {"fusion_enable": false}}').ok()
# 3. 进入yolov5_v6_1_pytorch ，进行一键模型转换。
# 需要替换自己的训练模型的话，进入模型目录：/home/share/mm/magicmind_cloud/buildin/cv/detection/yolov5_v6_1_pytorch/data/models/
ls -la /home/share/mm/magicmind_cloud/buildin/cv/detection/yolov5_v6_1_pytorch/data/models/
cd /home/share/mm/magicmind_cloud/buildin/cv/detection/yolov5_v6_1_pytorch
source env.sh
./run.sh
# 4. 查看转换好的模型
ls -la /home/share/mm/magicmind_cloud/buildin/cv/detection/yolov5_v6_1_pytorch/data/models/yolov5s_aarch64_force_float16_false_1
#cd /home/share/mm/magicmind_cloud/buildin/cv/detection/yolov5_v6_1_pytorch/gen_model
#bash run.sh ../data/models/yolov5s_aarch64_force_float16_false_1 force_float16 1 false
```
如有需要分步转换模型，可以参考说明 /home/share/mm/magicmind_cloud/buildin/cv/detection/yolov5_v6_1_pytorch/README.md


# 3. ARM 平台操作说明

参考如下步骤在 ARM 平台上对 MagicMind 模型进行性能压测及后续验证。

## 3.1. 硬件环境
| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| ARM服务器      | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| MLU370        | 一套       |机箱风扇需满足MLU370板卡被动散热要求|

## 3.2. 软件环境
| 名称           | 版本/文件                                                 | 备注                                 |
| :------------ | :------------------------------------------------------- | :---------------------------------- |
| Linux OS      | kylinV10                                                 | 宿主机操作系统                        |
| Driver        | cambricon-mlu-driver-kylinV10-5.10.34-1.ky10.aarch64.rpm | 依操作系统选择                          |
| Docker Image  | magicmind_1.10.0-1_anolis8.8.tar.gz                      | 官方发布的 MagicMind 框架 Docker 镜像文件 |

注: 以上软件环境中文件名词, 如有版本升级及名称变化, 可以在 [env.sh](./env.sh) 中进行修改。

**参考资料:**
- 前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。
- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch

## 3.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone https://gitee.com/cambriconknight/arm-env-runtime.git
```

## 3.4. 加载镜像
```bash
#加载Docker镜像
#./load-image-dev.sh ./docker/magicmind_1.10.0-1_anolis8.8.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```
```bash
[cambricon@bogon arm-env-runtime]# sudo docker load < magicmind_1.10.0-1_anolis8.8.tar.gz
[sudo] password for cambricon:
19a916b4d13d: Loading layer [==================================================>]  253.2MB/253.2MB
27673f55a27f: Loading layer [==================================================>]  13.82kB/13.82kB
......
......
ad21d32c0972: Loading layer [==================================================>]  87.62MB/87.62MB
41decbe0b787: Loading layer [==================================================>]   2.56kB/2.56kB
Loaded image: yellow.hub.cambricon.com/magicmind/release/aarch64/magicmind:1.10.0-aarch64-anolis8.8-py_3_10
[cambricon@bogon arm-env-runtime]#
```

## 3.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具集目录】
cd ./arm-env-runtime
#启动容器
./run-container-dev.sh
```
**实例**
```bash
[cambricon@bogon arm-env-runtime]# sudo ./run-container-dev.sh
[sudo] password for cambricon:
0
container-magicmind-1.10.0-aarch64-anolis8.8-py_3_10-kang
WARNING: Published ports are discarded when using host network mode
[root@bogon share]#
```

## 3.6. 模型验证

进入Docker容器后，就可以参考如下步骤对 MagicMind 模型 在ARM平台上进行性能压测及后续验证。

客户也可以把转换好的模型，直接拷贝到部署服务器上，进行针对性测试。

*注：以下操作步骤均是在官方提供的 ARM 版本 Docker容器中进行*

### 3.6.1 resnet50模型验证

使用2.6.1 章节转换好的 MagicMind 模型，拷贝到 ARM 部署环境，进行验证。

```bash
# 1. 拷贝之前转换好的 MagicMind 模型到 ARM 部署环境，进行验证。
scp resnet50-v1-7_*_mm username@192.168.0.110:/data/gitee/arm-runtime/test
# 2. 使用mm_run 压测mm模型
mm_run --magicmind_model resnet50-v1-7_force_float16_b1.mm --batch_size 1 --devices 0 --threads 1 --iterations 1000
#mm_run --magicmind_model resnet50-v1-7_force_float32_b1.mm --batch_size 1 --devices 0 --threads 1 --iterations 1000
##也可参考以下[一键测试脚本](./test/mm_run_renet50-v1-7.sh)，一键压测多种规格模型的性能。
cd /home/share/arm-runtime/test
./mm_run_renet50-v1-7.sh
```
**[一键测试脚本](./test/mm_run_renet50-v1-7.sh) 验证实例：**

```bash
[root@bogon test]# ./mm_run_renet50-v1-7.sh
=======================================================
mm_run: resnet50-v1-7_force_float32_b1_mm
model name:resnet50-v1-7_force_float32_b1_mm, qps:304.33, mean:3.2745, min:3.218, max:3.501
mm_run done: resnet50-v1-7_force_float32_b1_mm
=======================================================
=======================================================
mm_run: resnet50-v1-7_force_float32_b16_mm
model name:resnet50-v1-7_force_float32_b16_mm, qps:793.48, mean:20.152, min:15.393, max:25.645
mm_run done: resnet50-v1-7_force_float32_b16_mm
=======================================================
=======================================================
mm_run: resnet50-v1-7_force_float32_b32_mm
model name:resnet50-v1-7_force_float32_b32_mm, qps:754.65, mean:42.392, min:29.596, max:52.248
mm_run done: resnet50-v1-7_force_float32_b32_mm
=======================================================
=======================================================
mm_run: resnet50-v1-7_force_float16_b1_mm
model name:resnet50-v1-7_force_float16_b1_mm, qps:749.91, mean:1.3222, min:1.293, max:1.391
mm_run done: resnet50-v1-7_force_float16_b1_mm
=======================================================
=======================================================
mm_run: resnet50-v1-7_force_float16_b16_mm
model name:resnet50-v1-7_force_float16_b16_mm, qps:2330.2, mean:6.8543, min:5.646, max:8.747
mm_run done: resnet50-v1-7_force_float16_b16_mm
=======================================================
=======================================================
mm_run: resnet50-v1-7_force_float16_b32_mm
model name:resnet50-v1-7_force_float16_b32_mm, qps:2361.9, mean:13.536, min:10.008, max:17.801
mm_run done: resnet50-v1-7_force_float16_b32_mm
=======================================================
=======================================================
mm_run: resnet50-v1-7_qint8_mixed_float16_b1_mm
model name:resnet50-v1-7_qint8_mixed_float16_b1_mm, qps:1578.3, mean:0.62266, min:0.612, max:0.68
mm_run done: resnet50-v1-7_qint8_mixed_float16_b1_mm
=======================================================
=======================================================
mm_run: resnet50-v1-7_qint8_mixed_float16_b16_mm
model name:resnet50-v1-7_qint8_mixed_float16_b16_mm, qps:9113.2, mean:1.7444, min:1.735, max:1.767
mm_run done: resnet50-v1-7_qint8_mixed_float16_b16_mm
=======================================================
=======================================================
mm_run: resnet50-v1-7_qint8_mixed_float16_b32_mm
model name:resnet50-v1-7_qint8_mixed_float16_b32_mm, qps:10097, mean:3.1566, min:3.14, max:3.214
mm_run done: resnet50-v1-7_qint8_mixed_float16_b32_mm
=======================================================
```


### 3.6.2 YOLOv5模型验证

使用2.6.1 章节转换好的 MagicMind 模型，拷贝到 ARM 部署环境，进行验证。

```bash
# 1. 拷贝之前转换好的 MagicMind 模型到 ARM 部署环境，进行验证。
scp yolov5s_aarch64_force_float16_false_1 username@192.168.0.110:/data/gitee/arm-runtime/test
# 2. 使用mm_run 压测mm模型
mm_run --magicmind_model yolov5s_aarch64_force_float16_false_1 --input_dims 1,640,640,3 --batch_size 1 --devices 0 --threads 1 --iterations 1000
#../data/models/yolov5s_aarch64_force_float16_false_1
##也可参考以下[一键测试脚本](./test/mm_run_renet50-v1-7.sh)，一键压测多种规格模型的性能。
cd /home/share/arm-runtime/test
./mm_run_renet50-v1-7.sh
```
