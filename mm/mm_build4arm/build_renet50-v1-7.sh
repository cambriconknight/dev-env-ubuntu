export LD_LIBRARY_PATH=../lib64:${LD_LIBRARY_PATH}
#MODEL_FILE_PATH="resnet50_"$precision_type"_"$batch_num".mm"
model_name="resnet50-v1-7"
model_name_onnx="${model_name}.onnx"
model_name_mm="${model_name}_force_float32_b1.mm"
array_precision_type=(force_float32 force_float16 qint8_mixed_float16)
array_bs=(1 16 32)

#./onnx_build --onnx resnet50-v1-7.onnx --build_config config.json
for precision_type in "${array_precision_type[@]}"; do
    if [ -f config.json ];then
        cp -rvf config.json ${model_name}_${precision_type}.json
        sed -i "s/force_float16/$precision_type/g" ${model_name}_${precision_type}.json
    fi
    for batch_num in "${array_bs[@]}"; do
        echo "======================================================="
        model_name_mm="${model_name}_${precision_type}_b${batch_num}_mm"
        log_filename="${model_name}_${precision_type}_b${batch_num}_mm_build.log"
        if [ -f ${model_name_mm} ];then rm -rvf ${model_name_mm}; fi
        echo "build: ${model_name_onnx} ---> ${model_name_mm}"
        if [ $precision_type != qint8_mixed_float16 ]; then
            ./onnx_build --onnx ${model_name_onnx} --build_config ${model_name}_${precision_type}.json \
                 --input_dims ${batch_num},3,224,224 --magicmind_model ${model_name_mm} 2>&1 | tee ${log_filename}
        else
            # 设置 --random_calib_range 后，默认需 mlu 来进行量化
            ./onnx_build --onnx ${model_name_onnx} --build_config ${model_name}_${precision_type}.json \
                --input_dims ${batch_num},3,224,224 --calibration 1 --random_calib_range -10,10 \
                --magicmind_model ${model_name_mm} 2>&1 | tee ${log_filename}
        fi
        echo "build done: ${model_name_onnx} ---> ${model_name_mm}"
        if [ ! -f ${model_name_mm} ];then echo "build failed!"; else echo "build success";fi
        echo "======================================================="
    done
done
