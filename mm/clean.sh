#!/bin/bash
set -e

# 1.Source env
source ./env.sh $OSVer

# 2.rm docker container
#docker stop `docker ps -a | grep container-ubuntu16.04-caffe-v1.6.0 | awk '{print $1}'`
num_container=`docker ps -a | grep ${MY_CONTAINER} | awk '{print $1}'`
if [ $num_container ]; then docker stop $num_container;fi
#docker rm `docker ps -a | grep container-ubuntu16.04-caffe-v1.6.0 | awk '{print $1}'`
if [ $num_container ]; then docker rm $num_container;fi

# 3.rmi docker image
#docker rmi `docker images | grep cam/ubuntu16.04-caffe | awk '{print $3}'`
#num_images=`docker images | grep ${MY_IMAGE}  | grep ${VERSION} | awk '{print $3}'`
#if [ $num_images ]; then docker rmi $num_images;fi
