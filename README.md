<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu">
        <img alt="cambricon" src="./res/cambricon.jpg" height="140" />
        <h1 align="center">MLU常用算法移植及验证工具集</h1>
    </a>
</p>


- [1. 概述](#1-概述)
- [2. 大模型验证](#2-大模型验证)
- [3. 传统模型验证](#3-传统模型验证)
- [4. 开发资料下载](#4-开发资料下载)

# 1. 概述
本[工具集](https://gitee.com/cambriconknight/dev-env-ubuntu)主要基于寒武纪MLU进行移植环境的搭建及常用算法的移植与验证。力求压缩寒武纪MLU环境搭建与功能验证的时间成本, 以便快速上手[寒武纪MLU设备](https://www.cambricon.com/)。

<p align="left">
    <img alt="dev-env-ubuntu" src="./res/dev-env-ubuntu-1.png" height="360" />
</p>

>![](./res/note.gif) **功能说明：**
>- 可基于 Dockerfile 编译镜像，也可直接加载官网提供的各类镜像(推荐)。
>- 支持 PyTorch、TensorFlow 深度学习框架以及 MagicMind 推理加速引擎。
>- 支持常用算法的部署、移植、在线/离线推理等验证。

*本工具集仅用于个人学习，打通流程； 不对效果负责，不承诺商用。*

# 2. 大模型验证

*以下仅为常用大模型在MLU上的移植及验证教程，并未包含寒武纪支持的全部网络, 如有新网络需求, 可在 [issue](https://gitee.com/cambriconknight/dev-env-ubuntu/issues) 区讨论。*

|  框架 | 类别  | 官网链接  | 验证教程   | 环境搭建 | 推理验证 | 训练验证 |
|:----  |:-------  |:----   |:----   |:----:    |:----: |:----: |
| [EvalScope](./pytorch2.5/evalscope) | EvalScope  | [EvalScope](https://github.com/modelscope/evalscope)  | [验证教程](./pytorch2.5/evalscope)  |  ✅  |  ✅  |  -- |
| [vLLM](./pytorch2.5/vllm) | QwQ-32B  | [QwQ-32B](https://qwenlm.github.io/zh/blog/qwq-32b)  | [验证教程](./pytorch2.5/vllm/qwq-32b)  |  ✅  |  ✅  |  -- |
| [vLLM](./pytorch2.5/vllm) | QWen2-VL | [QWen2-VL](https://github.com/QwenLM/Qwen2.5/tree/44bf68571ae596c45b43c190b7963b6a6470b10b)  | [验证教程](./pytorch2.5/vllm/qwen2-vl)  |  ✅  |  ✅  |  -- |
| [vLLM](./pytorch2.5/vllm) | DeepSeek  | [DeepSeek-R1](https://github.com/deepseek-ai/DeepSeek-R1)  | [验证教程](./pytorch2.5/vllm/deepseek)  |  ✅  |  ✅  |  -- |
| [vLLM](./pytorch2.5/vllm) | Llama-3.1  | [Llama-3.1](https://github.com/meta-llama/llama3)  | [验证教程](./pytorch2.5/vllm/llama3.1)  |  ✅  |  ✅  |  -- |
| [vLLM](./pytorch2.5/vllm) | QWen2.5  | [QWen2.5](https://github.com/QwenLM/Qwen2.5)  | [验证教程](./pytorch2.5/vllm/qwen2.5)  |  ✅  |  ✅  |  -- |
| [Pytorch2.5](./pytorch2.5) | Langchain-Chatchat  | [Langchain-Chatchat](https://github.com/chatchat-space/Langchain-Chatchat)  | [验证教程](./pytorch2.5/langchain-chatchat)  |  ✅  |  ✅  |  -- |
| [Pytorch2.5](./pytorch2.5) | LLaMA-Factory  | [LLaMA-Factory](https://github.com/hiyouga/LLaMA-Factory)  | [验证教程](./pytorch2.5/llamafactory)  |  ✅  |  ✅  |  ✅ |
| [Pytorch2.5](./pytorch2.5) | QWen2.5-VL  | [QWen2.5-VL](https://github.com/QwenLM/Qwen2.5-VL)  | [验证教程](./pytorch2.5/qwen2.5-vl)  |  ✅  |  ✅  |  -- |
| [Pytorch2.5](./pytorch2.5) | YOLO11  | [YOLO11](https://github.com/ultralytics/ultralytics/releases/tag/v8.3.50)  | [验证教程](./pytorch2.5/yolo11)  |  ✅  |  ✅  |  -- |
| [Pytorch2.4](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.4) | Langchain-Chatchat  | [Langchain-Chatchat](https://github.com/chatchat-space/Langchain-Chatchat)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.4/langchain-chatchat)  |  ✅  |  ✅  |  -- |
| [Pytorch2.4](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.4) | LLaMA-Factory  | [LLaMA-Factory](https://github.com/hiyouga/LLaMA-Factory)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.4/llamafactory)  |  ✅  |  ✅  |  ✅ |
| [Pytorch2.1](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1) | LLaMA-Factory  | [LLaMA-Factory](https://github.com/hiyouga/LLaMA-Factory)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1/llamafactory)  |  ✅  |  ✅  |  ✅ |
| [Pytorch2.1](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1) | GroundingDINO  | [GroundingDINO](https://github.com/IDEA-Research/GroundingDINO)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1/groundingdino)  |  ✅  |  ✅  |  ✅ |
| [Pytorch2.1](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1) | GLM3  | [ChatGLM3-6B](https://github.com/THUDM/ChatGLM3)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1/chatglm3)  |  ✅  |  ✅  |  ✅ |
| [Pytorch2.1](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1) | Baichuan2  | [Baichuan2](https://github.com/baichuan-inc/Baichuan2)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1/baichuan2)  |  ✅  |  ✅  |  ✅ |
| [Pytorch2.1](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1) | QWen1.5  | [QWen1.5](https://github.com/QwenLM/Qwen1.5)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1/qwen1.5)  |  ✅  |  ✅  |  ✅ |
| [vLLM](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/llmspeed/vllm_mlu) | QWen2  | [QWen1.5](https://github.com/QwenLM/Qwen2)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/llmspeed/vllm_mlu/qwen2)  |  ✅  |  ✅  |  -- |
| [vLLM](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/llmspeed/vllm_mlu) | GLM3  | [ChatGLM3-6B](https://github.com/THUDM/ChatGLM3)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/llmspeed/vllm_mlu/chatglm3)  |  ✅  |  ✅  |  -- |
| [vLLM](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/llmspeed/vllm_mlu) | Baichuan2  | [Baichuan2](https://github.com/baichuan-inc/Baichuan2)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/llmspeed/vllm_mlu/baichuan2)  |  ✅  |  ✅  |  -- |
| [vLLM](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/llmspeed/vllm_mlu) | QWen1.5  | [QWen1.5](https://github.com/QwenLM/Qwen1.5)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/llmspeed/vllm_mlu/qwen1.5)  |  ✅  |  ✅  |  -- |
| [Pytorch1.13](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.13) | GLM3  | [ChatGLM3-6B](https://github.com/THUDM/ChatGLM3)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.13/chatglm3)  |  ✅  |  ✅  |  ✅ |
| [Pytorch1.13](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.13) | GLM2  | [ChatGLM2-6B](https://github.com/THUDM/ChatGLM3)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.13/benchmark/chatglm2)  |  ✅  |  ✅  |  ✅ |
| [Pytorch1.9](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9) | LLaMA  | [Chinese-LLaMA-Alpaca-2](https://github.com/ymcui/Chinese-LLaMA-Alpaca-2)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9/chinese-llama-alpaca-2)  |  ✅  |  ✅  |  ✅ |
| [Pytorch1.9](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9) | OB  | [OpenBioMed](https://github.com/PharMolix/OpenBioMed	)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9/openbiomed)  |  ✅  |  ✅  |  -- |
| [Pytorch1.9](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9) | GLM2  | [ChatGLM2-6B](https://github.com/THUDM/ChatGLM2-6B)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9/chatglm2)  |  ✅  |  ✅  |  -- |
| [Pytorch1.9](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9) | GLM  | [ChatGLM-6B](https://github.com/THUDM/ChatGLM-6B)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9/chatglm)  |  ✅  |  ✅  |  ✅ |
| [Pytorch1.9](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9) | Baichuan  | [Baichuan-7B](https://github.com/baichuan-inc/baichuan-7B)  | [验证教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch1.9/baichuan)  |  ✅  |  ✅  |  -- |

# 3. 传统模型验证

*以下仅为常用网络MLU移植教程，并未包含寒武纪支持的全部网络, 如有新网络需求, 可在 [issue](https://gitee.com/cambriconknight/dev-env-ubuntu/issues) 区讨论。*
*在Cambricon Caffe/TF/Pytorch 框架下可支持CPU、MLU（逐层模式）和MFUS（融合模式）三种模式上运行。*


|  AI框架 | 应用领域  | 原始网络  | 网络移植教程   | CPU | MLU | MFUS | OFFLINE |
|:----  |:-------  |:----   |:----   |:----:    |:----: |:----: |:----: |
| Caffe | 图像分类  | [AlexNet](https://github.com/BVLC/caffe/tree/master/models/bvlc_alexnet)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/alexnet)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 图像分类  | [ResNet-18](https://github.com/HolmesShuan/ResNet-18-Caffemodel-on-ImageNet)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/resnet18)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 图像分类  | [ResNet-50](https://github.com/KaimingHe/deep-residual-networks)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/resnet50)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 图像分类  | [VGG16](https://www.robots.ox.ac.uk/~vgg/research/very_deep/)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/vgg16)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 图像分类  | [MobileNetV1](https://github.com/chuanqi305/MobileNet-SSD)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/mobilenetv1)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 图像分类  | [MobileNetV2](https://github.com/eric612/MobileNet-SSD-windows/tree/master/models/MobileNetV2)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/mobilenetv2)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 目标检测  | [Faster R-CNN](https://github.com/rbgirshick/py-faster-rcnn)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/faster-rcnn)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 目标检测  | [RFCN](https://github.com/YuwenXiong/py-R-FCN)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/rfcn)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 目标检测  | [MTCNN](https://github.com/kpzhang93/MTCNN_face_detection_alignment)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/mtcnn)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 目标检测  | [SSD-VGG16](https://github.com/weiliu89/caffe/tree/ssd)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/ssd-vgg16)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 目标检测  | [YOLOv3](https://github.com/pjreddie/darknet)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/yolov3-416)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 目标检测  | [YOLOv4](https://github.com/pjreddie/darknet)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/yolov4‑mish‑416)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 语义分割  | [SegNet](https://github.com/alexgkendall/SegNet-Tutorial)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/segnet)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Caffe | 关键点检测  | [OpenPose](https://github.com/CMU-Perceptual-Computing-Lab/openpose)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/caffe/openpose)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 图像分类  | [ResNet-34](https://github.com/pytorch/vision)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/resnet34)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 图像分类  | [ResNet-50](https://github.com/pytorch/vision)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/resnet50)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 图像分类  | [ResNet-101](https://github.com/pytorch/vision)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/resnet101)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 图像分类  | [VGG16](https://github.com/pytorch/vision)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/vgg16)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 图像分类  | [MobileNetV1](https://modelzoo.co/model/pytorch-mobilenet)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/mobilenetv1)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 图像分类  | [MobileNetV2](https://modelzoo.co/model/mobilenetv2)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/mobilenetv2)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 图像分类  | [DenseNet121](https://github.com/pytorch/vision/blob/main/torchvision/models/densenet.py)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/densenet121)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 目标检测  | [Faster R-CNN](https://github.com/pytorch/vision)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/faster-rcnn)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 目标检测  | [Mask R-CNN](https://github.com/facebookresearch/maskrcnn-benchmark)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/mask-rcnn)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 目标检测  | [SSD-VGG16](https://github.com/amdegroot/ssd.pytorch)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/ssd-vgg16)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 目标检测  | [YOLOv3](https://modelzoo.co/model/pytorch-yolov3-pytorch)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/yolov3)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 目标检测  | [YOLOv4](https://github.com/AlexeyAB/darknet.git)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/yolov4)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 目标检测  | [YOLOv4-Tiny](https://github.com/AlexeyAB/darknet.git)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/yolov4-tiny)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 目标检测  | [YOLOv5](https://github.com/ultralytics/yolov5.git)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/yolov5)  |  ✅ |  ✅ |  ✅ |  ✅ |
| Pytorch | 目标检测  | [CenterNet](https://github.com/xingyizhou/CenterNet)  | [移植教程](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.17.0/pytorch/centernet)  |  ✅ |  ✅ |  ✅ |  ✅ |


# 4. 开发资料下载

注册开发者社区账号并下载 SDK 及文档: https://forum.cambricon.com/index.php?m=content&c=index&a=show&catid=169&id=2441

**下载地址:**
- 前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。
- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch
