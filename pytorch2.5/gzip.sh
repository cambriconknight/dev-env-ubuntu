#!/bin/bash
#set -ex
set -e
USERNAME=$(whoami)
CMD_TIME=$(date +%Y%m%d%H%M%S) # eg:20210402230402
# 0.Check param
if [[ $# -eq 1 ]];then CMD_TIME="${1}";fi

# 1.Source env
source "./env.sh"
# 2.gzip
ls -la $PATH_WORK-$VERSION-$CMD_TIME.tar.gz* \
    && mv $PATH_WORK-$VERSION-$CMD_TIME.tar.gz $PATH_WORK-$VERSION-$CMD_TIME.tar \
    && gzip $PATH_WORK-$VERSION-$CMD_TIME.tar \
    && md5sum $PATH_WORK-$VERSION-$CMD_TIME.tar.gz > $PATH_WORK-$VERSION-$CMD_TIME.tar.gz.md5sum \
    && ls -lh $PATH_WORK-$VERSION-$CMD_TIME.tar.gz*
