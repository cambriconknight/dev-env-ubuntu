<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/ffmpeg">
        <h1 align="center">基于PyTorch2.5镜像环境进行FFmpeg-MLU各功能验证</h1>
    </a>
</p>

**该教程仅仅用于学习，打通流程； 不对效果负责，不承诺商用。**

# 1. 概述

本工具集是基于官方发布的 PyTorch2.5 框架的 Docker 镜像环境进行[FFmpeg-MLU](https://github.com/Cambricon/ffmpeg-mlu)各功能验证。 

**说明:**

基于寒武纪® MLU硬件平台，寒武纪 FFmpeg-MLU使用纯C接口实现硬件加速的视频编解码。

## 1.1. 硬件环境准备

| 名称            | 数量       | 备注                |
| :-------------- | :--------- | :------------------ |
| 开发主机/服务器 | 一台       |主流配置即可；电源功率按需配置 |
| AI加速卡        | 一套      | 不同型号AI加速卡尽量避免混插混用；PCIe Gen.3 x16/Gen.4 x16 |

## 1.2. 软件环境准备

| 名称           | 版本/文件                                                 | 备注                                 |
| :------------ | :-------------------------------                         | :---------------------------------- |
| Linux OS      | CentOS7                                                  | 宿主机操作系统                         |
| Driver_MLU370 | cambricon-mlu-driver-centos7-6.*.*-10.x86_64.rpm        | 依操作系统选择                         |
| Docker Image  | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz    | 官方发布的 PyTorch2.5 框架 Docker 镜像文件 |
| FFmpeg-MLU    | FFmpeg-MLU   | commit： 68022d； 脚本会自动[下载](https://gitee.com/cambricon/ffmpeg-mlu)    |
| FFmpeg        | FFmpeg       | branch： 4.4； 脚本会自动[下载](https://gitee.com/mirrors/ffmpeg.git)    |

*官方发布的 Docker Image 已经默认集成了 FFmpeg-MLU，可直接使用。*

**AIKnight公众号**
>![](../../res/aiknight_wechat_172.jpg)

**技术参考:**
- 前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。
- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch


## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
```
## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#加载Docker镜像
#./load-image-dev.sh /data/ftp/docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz 
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#启动Docker容器
./run-container-dev.sh
```

# 2. 测试验证

*以下测试验证步骤都是基于Docker容器内环境.*

## 2.1. MLU解码
```bash
#基于 FFMPEG 命令行方式验证多路并行解码, 可用于上手阶段压测MLU板卡硬件解码能力.
cd /home/share/pytorch2.5/ffmpeg/cmd
./test-ffmpeg-mlu-cmd-decode.sh
#此plus脚本在运行核心业务过程中可以实时显示显示业务日志文件并记录到日志文件，并且还会打印cnmon相关信息到日志文件。
#./test-ffmpeg-mlu-cmd-decode-plus.sh ../data/jellyfish-3-mbps-hd-h264.mkv 0 128 h264_mludec
```
**MLU解码实例-解码128路高清视频**
<p align="left">
    <img alt="test-ffmpeg-mlu-cmd-decode" src="https://gitee.com/cambriconknight/dev-open-res/raw/main/ffmpeg-mlu-docker-image/res/test-ffmpeg-mlu-cmd-decode.gif" width="640" />
</p>

## 2.2. MLU编码
```bash
#基于 FFMPEG 命令行方式验证多路并行编码, 可用于上手阶段压测MLU板卡硬件编码能力.
cd /home/share/pytorch2.5/ffmpeg/cmd
./test-ffmpeg-mlu-cmd-encode.sh
```
**MLU编码实例-编码24路高清视频**
<p align="left">
    <img alt="test-ffmpeg-mlu-cmd-encode" src="https://gitee.com/cambriconknight/dev-open-res/raw/main/ffmpeg-mlu-docker-image/res/test-ffmpeg-mlu-cmd-encode.gif" width="640" />
</p>

## 2.3. MLU转码
```bash
#基于FFMPEG转码有两种方式
#1、命令行方式
cd /home/share/pytorch2.5/ffmpeg/cmd
./test-ffmpeg-mlu-cmd.sh
#基于 FFMPEG 命令行方式验证多路并行转码, 可用于上手阶段压测MLU板卡硬件编解码能力.
#./test-ffmpeg-mlu-cmd-transcode.sh
#此plus脚本在运行核心业务过程中可以实时显示显示业务日志文件并记录到日志文件，并且还会打印cnmon相关信息到日志文件。
#./test-ffmpeg-mlu-cmd-transcode-plus.sh ../data/jellyfish-3-mbps-hd-h264.mkv 0 24 h264_mludec h264_mluenc "1920x1080"
#./test-ffmpeg-mlu-cmd-transcode-plus.sh ../data/jellyfish-3-mbps-hd-h264.mkv 0 50 h264_mludec h264_mluenc "352x288"
#2、API接口调用方式
cd /home/share/pytorch2.5/ffmpeg/api
./test-ffmpeg-mlu-api.sh
```

