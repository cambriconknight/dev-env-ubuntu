<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/PyTorch2.5/ffmpeg/data">
        <h1 align="center">FFmpeg-MLU多媒体测试资源下载说明</h1>
    </a>
</p>


**下载方式**

关注微信公众号 AIKnight , 发送文字消息, 包含关键字(不区分大小写): **FFmpeg-MLU多媒体测试资源**, 公众号会自动回复FFmpeg-MLU多媒体测试资源的下载地址；

>![](../../res/note.gif) **备注信息：**
>- FFmpeg-MLU多媒体测试资源 文件都比较大，请根据测试需求则需下载。下载后，可把文件放置到当前目录(data)下，方便根据后续脚本提示进行后续操作。

**公众号**
>![](../../../res/aiknight_wechat_172.jpg)

