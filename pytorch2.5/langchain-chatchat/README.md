
<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/langchain-chatchat">
        <h1 align="center">基于MLU卡的Langchain-Chatchat框架验证教程</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. Xinference 环境搭建](#2-xinference-环境搭建)
  - [2.1. 基础环境搭建](#21-基础环境搭建)
  - [2.2. 启动本地服务](#22-启动本地服务)
  - [2.3. 运行 LLM 模型](#23-运行-llm-模型)
  - [2.4. 运行 embedding 模型](#24-运行-embedding-模型)
  - [2.5. xinference自定义模型参考命令](#25-xinference自定义模型参考命令)
  - [2.6. xinference WebUI 验证](#26-xinference-webui-验证)
- [3. Langchain-chatchat 环境搭建](#3-langchain-chatchat-环境搭建)
  - [3.1. 安装 Langchain-Chatchat](#31-安装-langchain-chatchat)
  - [3.2. 模型推理框架并加载模型](#32-模型推理框架并加载模型)
  - [3.3. 初始化项目配置与数据目录](#33-初始化项目配置与数据目录)
  - [3.4. 初始化知识库](#34-初始化知识库)
  - [3.5. 启动项目](#35-启动项目)
- [4. 保存镜像](#4-保存镜像)

# 1. 环境准备

本文使用了 [Xinference](https://github.com/xorbitsai/inference) 接入 [DeepSeek-R1-Distill Models 系列模型](https://github.com/deepseek-ai/DeepSeek-R1?tab=readme-ov-file#deepseek-r1-distill-models)，并基于 [Langchain-Chatchat](https://github.com/chatchat-space/Langchain-Chatchat) 框架实现本地MLU环境下的全国产化知识库问答系统解决方案。

>![](../../res/note.gif) **备注：**
>- 为避免依赖冲突，请将 Langchain-Chatchat 和 Xinference 模型部署框架 放在不同的 Python 虚拟环境中, 比如 conda, venv, virtualenv 等。

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡     | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-6.2.10-15.el7.x86_64.rpm	        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| Langchain-Chatchat 源码  | https://github.com/chatchat-space/Langchain-Chatchat | commit: 40994eb6 |
| Xinference 源码       | https://github.com/xorbitsai/inference.git | commit: f4e3cee7 |
| LLM 模型-Qwen   | [DeepSeek-R1-Distill-Qwen-14B 模型](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-14B)	   | commit： 6453600 |
| EMBED 模型 | [bge-large-zh-v1.5](https://huggingface.co/BAAI/bge-large-zh-v1.5)    | commit： 79e7739b6ab94 |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**下载地址:**
- 前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。
- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch
- vLLM官方文档: https://docs.vllm.ai/en/latest/getting_started/quickstart.html
- OpenAI官网文档: https://platform.openai.com/docs/introduction
- DeepSeek官网文档: https://api-docs.deepseek.com/zh-cn/news/news1226

**AIKnight公众号**
>![](../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
```

## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#加载Docker镜像
#./load-image-dev.sh /data/ftp/docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#启动Docker容器
./run-container-dev.sh
```

# 2. Xinference 环境搭建

Xinference的详细使用参考[官方文档](https://inference.readthedocs.io/en/latest/getting_started/using_xinference.html#run-xinference-locally)

## 2.1. 基础环境搭建
```bash
#以下操作均在Docker容器中进行。
#pip install httpx==0.27.2
# 1. 切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
# 2. 执行一键适配脚本（首次进入容器环境执行）
cd /home/share/pytorch2.5/langchain-chatchat/tools/ && bash deploy_env_s.sh
```

## 2.2. 启动本地服务

```bash
#以下操作均在Docker容器中进行。
# 1. 切换到vllm推理环境
#source /torch/venv3/pytorch_infer/bin/activate
# 2. 启动 Xinference 的本地实例
#注意要在xinference代码路径下，起xinference-local，否则有报错
cd /workspace/langchain-chatchat/inference/
#--gpu-idx 0,1,2,3
export PYTORCH_CNDEV_BASED_MLU_CHECK=1
export MLU_VISIBLE_DEVICES=0,1,2,3
xinference-local -H 0.0.0.0 -p 9997
#Xinference默认使用<HOME>/.xinference作为主目录存储一些必要信息，如：日志文件和模型文件
```

启动后，终端会打印如下字符：

```bash
INFO     Uvicorn running on http://0.0.0.0:9997 (Press CTRL+C to quit)
```

**注意：本地服务器启动后，当前终端占用，后续运行各个模型的操作，可以另外启动一个终端，进入推理环境操作。**

```bash
source /torch/venv3/pytorch_infer/bin/activate
```

## 2.3. 运行 LLM 模型

**以下操作均在Docker容器vllm推理环境中进行。**

*注：从 v0.14.0 版本开始，如果你需要注册的模型的家族是 Xinference 内置支持的模型，你可以直接通过 launch 接口中的 model_path 参数来启动它，从而免去注册步骤的麻烦。现在非常推荐使用这种方式。详细参考[官网文档](https://inference.readthedocs.io/zh-cn/stable/models/custom.html)，Xinference 已内置了DeepSeek-R1-Distill系列模型*

Xinference 中内置的 LLM 列表： https://inference.readthedocs.io/zh-cn/stable/models/builtin/llm/index.html
Xinference 中内置的嵌入模型列表: https://inference.readthedocs.io/zh-cn/stable/models/builtin/embedding/index.html

下面的例子我们展示了内置LLM模型 DeepSeek-R1-Distill-Qwen-14B ，直接 launch 它。

启动前请确认实际模型路径，并修改目录位置：/data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B

```bash
cd /workspace/langchain-chatchat/inference/
xinference launch --model-engine vllm --model-name deepseek-r1-distill-qwen --size-in-billions 14 --model-format pytorch -e http://0.0.0.0:9997 --model_path /data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B --gpu-idx 0,1,2,3 --dtype float16 --block_size 4096 --max_model_len 4096
```

*注：3系列板卡在launch模型时，可传参给block_size和max_model_len进行设定。*

**服务验证**

*OpenAI接口验证*

```bash
#本地服务器启动后，当前终端占用，后续运行各个模型的操作，可以另外启动一个终端，进入推理环境操作。
source /torch/venv3/pytorch_infer/bin/activate
# 运行如下脚本，验证服务器是否可用：
cd /home/share/pytorch2.5/langchain-chatchat/tools
python test_inference.py
```

## 2.4. 运行 embedding 模型

**以下操作均在Docker容器vllm推理环境中进行。**

*bge-large-zh-v1.5示例*

```bash
xinference launch --model-name bge-large-zh-v1.5 --model-type embedding -e http://0.0.0.0:9997 --model_path /data/models/llm/models/bge-large-zh-v1.5 --gpu-idx 0,1,2,3 --dtype float16
```

## 2.5. xinference自定义模型参考命令

Xinference 启动自定义模型参考教程： https://inference.readthedocs.io/zh-cn/stable/models/custom.html

## 2.6. xinference WebUI 验证

打开浏览器，根据实际`服务器IP`地址，输入以下网址：http://192.168.0.11:9997 ，如下所示：

<p align="left">
    <img alt="langchain-chatchat_xinference_webui_1_s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/langchain-chatchat/res/xinference_webui_1_s.gif" width="640" />
    <img alt="langchain-chatchat_xinference_webui_2_s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/langchain-chatchat/res/xinference_webui_2_s.gif" width="640" />
    <img alt="langchain-chatchat_xinference_webui_3_s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/langchain-chatchat/res/xinference_webui_3_s.gif" width="640" />
</p>

# 3. Langchain-chatchat 环境搭建

Langchain-Chatchat 的详细使用参考[官方文档](https://github.com/chatchat-space/Langchain-Chatchat)。

>![](../../res/note.gif) **备注：**
>- 为避免依赖冲突，请将 Langchain-Chatchat 和模型部署框架如 Xinference 等放在不同的 Python 虚拟环境中, 比如 conda, venv, virtualenv 等。

## 3.1. 安装 Langchain-Chatchat

Langchain-Chatchat环境`不需要切换到推理环境`，且要保证与上一章节中的 `Xinference 虚拟环境隔离`。

```bash
#以下操作均在Docker容器默认的【训练环境】中进行，或者新建一个虚拟环境。
#因模型部署框架 Xinference 接入 Langchain-Chatchat 时需要额外安装对应的 Python 依赖库，因此如需搭配 Xinference 框架使用时，使用如下安装方式：
pip install "langchain-chatchat[xinference]" -U
pip install httpx==0.27.2
```

## 3.2. 模型推理框架并加载模型

详见上一章节[2. Xinference 环境搭建](#2-xinference-环境搭建)。

## 3.3. 初始化项目配置与数据目录

从 0.3.1 版本起，Langchain-Chatchat 使用本地 `yaml` 文件的方式进行配置，用户可以直接查看并修改其中的内容，服务器会自动更新无需重启。

1. 设置 Chatchat 存储配置文件和数据文件的根目录（可选）

```bash
mkdir -p /workspace/langchain-chatchat/chatchat_data
export CHATCHAT_ROOT=/workspace/langchain-chatchat/chatchat_data
```
若不设置该环境变量，则自动使用当前目录。

2. 执行初始化

```bash
cd /workspace/langchain-chatchat/chatchat_data
chatchat init
```

该命令会执行以下操作：

- 创建所有需要的数据目录
- 复制 samples 知识库内容
- 生成默认 `yaml` 配置文件

```bash
(pytorch) root@worker1:/workspace/langchain-chatchat/chatchat_data# chatchat init
2025-02-01 14:13:43.902 | WARNING  | chatchat.server.utils:get_default_llm:205 - default llm model glm4-chat is not found in available llms, using deepseek-r1-distill-qwen instead
2025-02-01 14:13:43.912 | WARNING  | chatchat.server.utils:get_default_embedding:214 - default embedding model bge-m3 is not found in available embeddings, using bge-large-zh-v1.5 instead
2025-02-01 14:13:43.914 | WARNING  | chatchat.server.utils:get_default_embedding:214 - default embedding model bge-m3 is not found in available embeddings, using bge-large-zh-v1.5 instead
2025-02-01 14:13:43.915 | WARNING  | chatchat.server.utils:get_default_embedding:214 - default embedding model bge-m3 is not found in available embeddings, using bge-large-zh-v1.5 instead
2025-02-01 14:13:43.916 | WARNING  | chatchat.server.utils:get_default_embedding:214 - default embedding model bge-m3 is not found in available embeddings, using bge-large-zh-v1.5 instead
2025-02-01 14:13:43.920 | SUCCESS  | chatchat.cli:init:47 - 开始初始化项目数据目录：/workspace/langchain-chatchat/chatchat_data
2025-02-01 14:13:43.921 | SUCCESS  | chatchat.cli:init:49 - 创建所有数据目录：成功。
2025-02-01 14:13:43.923 | SUCCESS  | chatchat.cli:init:52 - 复制 samples 知识库文件：成功。
2025-02-01 14:13:43.945 | SUCCESS  | chatchat.cli:init:54 - 初始化知识库数据库：成功。
2025-02-01 14:13:44.321 | SUCCESS  | chatchat.cli:init:66 - 生成默认配置文件：成功。
2025-02-01 14:13:44.321 | SUCCESS  | chatchat.cli:init:67 - 请先检查确认 model_settings.yaml 里模型平台、LLM模型和Embed模型信息已经正确
2025-02-01 14:13:44.321 | SUCCESS  | chatchat.cli:init:76 - 执行 chatchat kb -r 初始化知识库，然后 chatchat start -a 启动服务。
```

3. 修改配置文件

```bash
#覆盖配置文件
cp -rvf /home/share/pytorch2.5/langchain-chatchat/tools/model_settings.yaml /workspace/langchain-chatchat/chatchat_data/
```

- 配置模型（model_settings.yaml）
需要根据步骤 **上一章节[2. Xinference 环境搭建](#2-xinference-环境搭建)**  中选用的模型推理框架与加载的模型进行模型接入配置，具体参考 `model_settings.yaml` 中的注释。主要修改以下内容：
```yaml
# 默认选用的 LLM 名称
DEFAULT_LLM_MODEL: deepseek-r1-distill-qwen

# 默认选用的 Embedding 名称
DEFAULT_EMBEDDING_MODEL: bge-large-zh-v1.5

# 将 `LLM_MODEL_CONFIG` 中 `llm_model, action_model` 的键改成对应的 LLM 模型
# 在 `MODEL_PLATFORMS` 中修改对应模型平台信息
```

其他配置详细说明参见[官方文档](https://github.com/chatchat-space/Langchain-Chatchat?tab=readme-ov-file#3-%E5%88%9D%E5%A7%8B%E5%8C%96%E9%A1%B9%E7%9B%AE%E9%85%8D%E7%BD%AE%E4%B8%8E%E6%95%B0%E6%8D%AE%E7%9B%AE%E5%BD%95)

## 3.4. 初始化知识库

>![](../../res/note.gif) **备注：**
>- 进行知识库初始化前，请确保已经启动模型推理框架及对应 `embedding` 模型，且已按照上述**步骤3**完成模型接入配置。

```shell
chatchat kb -r
```
如果报错，在xinference推理环境下安装embedding依赖库，命令如下： 【pip install flagembedding】

更多功能可以查看 `chatchat kb --help`

出现以下日志即为成功:

```text

----------------------------------------------------------------------------------------------------
知识库名称      ：samples
知识库类型      ：faiss
向量模型：      ：bge-large-zh-v1.5
知识库路径      ：/workspace/langchain-chatchat/chatchat_data/data/knowledge_base/samples
文件总数量      ：12
入库文件数      ：12
知识条目数      ：755
用时            ：0:00:48.158940
----------------------------------------------------------------------------------------------------

总计用时        ：0:00:48.169585

```

## 3.5. 启动项目

```shell
#安装 httpx ，否则打开webui终端会报错。
#pip install httpx==0.27.2
chatchat start -a
```
出现以下界面即为启动成功, 然后在局域网中任意台PC浏览器上输入部署服务器的IP地址替换0.0.0.0，即如【http://192.168.0.110:8501】:

```bash
  You can now view your Streamlit app in your browser.

  URL: http://0.0.0.0:8501
```

打开浏览器，根据实际`服务器IP`地址，输入网址如：http://192.168.0.11:9997 ，进行后续操作：

<p align="left">
    <img alt="langchain-chatchat_webui_s.gif" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/langchain-chatchat/res/langchain-chatchat_webui_s.gif" width="640" />
</p>

>![](../../res/note.gif) **备注：**
>- 由于 chatchat 配置默认监听地址 `DEFAULT_BIND_HOST` 为 127.0.0.1, 所以无法通过其他 ip 进行访问。
>- 如需通过机器ip 进行访问(如 Linux 系统), 需要到 `basic_settings.yaml` 中将监听地址修改为 0.0.0.0。

# 4. 保存镜像

保存镜像，供后续**环境变更后**或**离线环境下**快速进行测试验证。验证的步骤，详见教程 [README4OfflineImages.md](./README4OfflineImages.md)。

```bash
#./save-image-dev.sh
./save-image-dev.sh langchain_chatchat
```

如果感觉镜像文件太大，不方便后期传输。可参考以下步骤，对镜像文件进行压缩并记录md5值：

```bash
cd /data/gitee/dev-env-ubuntu/pytorch2.5
bash ./gzip.sh langchain_chatchat
```
