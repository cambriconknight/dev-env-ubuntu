#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     2.launch_xinference_models.sh
# UpdateDate:   2025/02/14
# Description:  运行xinference模型。
# Example:      cd /home/share/pytorch2.5/langchain-chatchat/deploy/ && bash 2.launch_xinference_models.sh
# Depends:
# Notes:        以下操作均在Docker容器中进行。
# -------------------------------------------------------------------------------

#以下操作均在Docker容器中进行。
# 1. 切换到vllm推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/langchain-chatchat/inference/
# 1. 运行 LLM 模型
xinference launch --model-engine vllm \
    --model-name deepseek-r1-distill-qwen \
    --size-in-billions 14 --model-format pytorch -e http://0.0.0.0:9997 \
    --model_path /data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B \
    --gpu-idx 0,1,2,3 \
    --dtype float16 \
    --block_size 4096 \
    --max_model_len 4096
# 2. 运行 embedding 模型
xinference launch --model-name bge-large-zh-v1.5 \
    --model-type embedding \
    -e http://0.0.0.0:9997 \
    --model_path /data/models/llm/models/bge-large-zh-v1.5 \
    --gpu-idx 0,1,2,3 \
    --dtype float16

