#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     1.run_xinference_services.sh
# UpdateDate:   2025/02/14
# Description:  一键启动本地xinference服务。
# Example:      cd /home/share/pytorch2.5/langchain-chatchat/deploy/ && bash 1.run_xinference_services.sh
# Depends:
# Notes:        以下操作均在Docker容器中进行。
# -------------------------------------------------------------------------------

#以下操作均在Docker容器中进行。
# 1. 切换到vllm推理环境
source /torch/venv3/pytorch_infer/bin/activate
# 2. 启动 Xinference 的本地实例
#注意要在xinference代码路径下，起xinference-local，否则有报错
cd /workspace/langchain-chatchat/inference/
#--gpu-idx 0,1,2,3
export PYTORCH_CNDEV_BASED_MLU_CHECK=1
export MLU_VISIBLE_DEVICES=0,1,2,3
xinference-local -H 0.0.0.0 -p 9997
#Xinference默认使用<HOME>/.xinference作为主目录存储一些必要信息，如：日志文件和模型文件