#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     3.start_chatchat_services.sh
# UpdateDate:   2025/02/14
# Description:  一键启动Langchain-chatchat服务。
# Example:      cd /home/share/pytorch2.5/langchain-chatchat/deploy/ && bash 3.start_chatchat_services.sh
# Depends:
# Notes:        以下操作均在Docker容器中进行。
# -------------------------------------------------------------------------------

#以下操作均在Docker容器中进行。
cd /workspace/langchain-chatchat/chatchat_data
#设置环境变量
export CHATCHAT_ROOT=/workspace/langchain-chatchat/chatchat_data
#如配置文件有修改，修改后覆盖原来配置文件，参考如下：
#cp -rvf /home/share/pytorch2.5/langchain-chatchat/tools/model_settings.yaml /workspace/langchain-chatchat/chatchat_data/
#配置文件修改后，需要再初始化一遍知识库
#chatchat kb -r
#安装 httpx ，否则打开webui终端会报错。
#pip install httpx==0.27.2
#启动项目
chatchat start -a

