
<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/blob/master/pytorch2.5/langchain-chatchat/README4OfflineImages.md">
        <h1 align="center">基于MLU在Langchain框架上验证DeepSeek-R1-Distill模型</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. Xinference 启动和配置](#2-xinference-启动和配置)
  - [2.1. 模型下载](#21-模型下载)
  - [2.2. 启动本地服务](#22-启动本地服务)
  - [2.3. 运行 LLM 模型](#23-运行-llm-模型)
  - [2.4. 运行 embedding 模型](#24-运行-embedding-模型)
  - [2.5. xinference自定义模型参考命令](#25-xinference自定义模型参考命令)
  - [2.6. xinference WebUI 验证](#26-xinference-webui-验证)
- [3. Langchain-chatchat 启动和配置](#3-langchain-chatchat-启动和配置)
- [4. 执行一键启动脚本](#4-执行一键启动脚本)

# 1. 环境准备

本文使用了 [Xinference](https://github.com/xorbitsai/inference) 接入 [DeepSeek-R1-Distill Models 系列模型](https://github.com/deepseek-ai/DeepSeek-R1?tab=readme-ov-file#deepseek-r1-distill-models)，并基于 [Langchain-Chatchat](https://github.com/chatchat-space/Langchain-Chatchat) 框架实现本地MLU环境下的全国产化知识库问答系统解决方案。

为便于 [DeepSeek-R1-Distill 系列模型](https://github.com/deepseek-ai/DeepSeek-R1?tab=readme-ov-file#deepseek-r1-distill-models) 在MLU卡上进行功能验证，本教程采用已配置并集成好的 **Docker 离线镜像** 进行被测模型验证工作。用户仅需在已安装好驱动的MLU[硬件环境](#11-硬件环境)上，按照[软件环境](#12-软件环境)要求把准备好的 **Docker 离线镜像** 和 **待测模型权重** 放置到指定位置，即可按照后续步骤进行各项功能测试与验证。

>![](../../res/note.gif) **备注：**
>- 为避免依赖冲突，请将 Langchain-Chatchat 和 Xinference 模型部署框架 放在不同的 Python 虚拟环境中, 比如 conda, venv, virtualenv 等。
>- 本教程尤其适用于离线环境下，对被测网络模型进行快速功能验证。

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡     | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                             | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-6.2.10-15.el7.x86_64.rpm	        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| Docker 集成验证镜像          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310-langchain_chatchat.tar.gz     | 环境已配置并集成好的 **Docker 离线镜像**文件； |
| Langchain-Chatchat 源码  | https://github.com/chatchat-space/Langchain-Chatchat | commit: 40994eb6 |
| Xinference 源码       | https://github.com/xorbitsai/inference.git | commit: f4e3cee7 |
| LLM 模型-Qwen   | [DeepSeek-R1-Distill-Qwen-14B 模型](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-14B)	   | commit： 6453600 |
| EMBED 模型       | [bge-large-zh-v1.5](https://huggingface.co/BAAI/bge-large-zh-v1.5)    | commit： 79e7739b6ab94 |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

注：镜像中不包含大模型权重文件，验证前请自行下载后，放置到指定目录下【/data/models/】。`Docker 离线镜像`文件可关注微信公众号 【 AIKnight 】, 发送关键字 【langchain_image】自动获取； 镜像文件大约13G，请合理安排时间下载；

**AIKnight公众号**
>![](../../res/aiknight_wechat_172.jpg)

**参考链接:**
- 前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。
- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch
- vLLM官方文档: https://docs.vllm.ai/en/latest/getting_started/quickstart.html
- OpenAI官网文档: https://platform.openai.com/docs/introduction
- DeepSeek官网文档: https://api-docs.deepseek.com/zh-cn/news/news1226

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
```
修改env.sh 脚本中环境变量【TestModel】值为【"-langchain_chatchat"】：
```bash
cd /data/gitee/dev-env-ubuntu/pytorch2.5
vim env.sh
```
修改内容如下：
```bash
#TestModel=""
TestModel="-langchain_chatchat"
```

## 1.4. 加载镜像

请提前下载好【**Docker 离线镜像**】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#加载Docker镜像
#./load-image-dev.sh /data/ftp/docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310-langchain_chatchat.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#启动Docker容器
./run-container-dev.sh
```

# 2. Xinference 启动和配置

Xinference的详细使用参考[官方文档](https://inference.readthedocs.io/en/latest/getting_started/using_xinference.html#run-xinference-locally)

## 2.1. 模型下载

本教程只以 DeepSeek-R1-Distill 模型为基础进行验证。具体操作步骤均参考[DeepSeek-R1官网模型列表](https://github.com/deepseek-ai/DeepSeek-R1?tab=readme-ov-file#deepseek-r1-distill-models)，下载模型到容器中【/data/models/】指定目录，或者自行下载后拷贝模型文件到此类目录。

**DeepSeek-R1-Distill Models**

| **Model** | **Base Model** | **Download** |
| :------------: | :------------: | :------------: |
| DeepSeek-R1-Distill-Qwen-1.5B  | [Qwen2.5-Math-1.5B](https://huggingface.co/Qwen/Qwen2.5-Math-1.5B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-1.5B)   |
| DeepSeek-R1-Distill-Qwen-7B  | [Qwen2.5-Math-7B](https://huggingface.co/Qwen/Qwen2.5-Math-7B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-7B)   |
| DeepSeek-R1-Distill-Llama-8B  | [Llama-3.1-8B](https://huggingface.co/meta-llama/Llama-3.1-8B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Llama-8B)   |
| DeepSeek-R1-Distill-Qwen-14B   | [Qwen2.5-14B](https://huggingface.co/Qwen/Qwen2.5-14B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-14B)   |
|DeepSeek-R1-Distill-Qwen-32B  | [Qwen2.5-32B](https://huggingface.co/Qwen/Qwen2.5-32B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-32B)   |
| DeepSeek-R1-Distill-Llama-70B  | [Llama-3.3-70B-Instruct](https://huggingface.co/meta-llama/Llama-3.3-70B-Instruct) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Llama-70B)   |

*注: 所有模型的最新更新都会在 Huggingface 率先发布。 ModelScope 和 WiseModel 由于没有与 Huggingface 同步，需要开发人员手动更新，可能会在 Huggingface 更新后一段时间内同步更新。*

## 2.2. 启动本地服务

```bash
#以下操作均在Docker容器中进行。
# 1. 切换到vllm推理环境
source /torch/venv3/pytorch_infer/bin/activate
# 2. 启动 Xinference 的本地实例
#注意要在xinference代码路径下，起xinference-local，否则有报错
cd /workspace/langchain-chatchat/inference/
#--gpu-idx 0,1,2,3
export PYTORCH_CNDEV_BASED_MLU_CHECK=1
export MLU_VISIBLE_DEVICES=0,1,2,3
xinference-local -H 0.0.0.0 -p 9997
#Xinference默认使用<HOME>/.xinference作为主目录存储一些必要信息，如：日志文件和模型文件
```

启动后，终端会打印如下字符：

```bash
INFO     Uvicorn running on http://0.0.0.0:9997 (Press CTRL+C to quit)
```

**注意：本地服务器启动后，当前终端占用，后续运行各个模型的操作，可以另外启动一个终端，进入推理环境操作。**

```bash
source /torch/venv3/pytorch_infer/bin/activate
```

## 2.3. 运行 LLM 模型

**以下操作均在Docker容器vllm推理环境中进行。**

启动前请确认实际模型路径，并修改目录位置：/data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B

```bash
cd /workspace/langchain-chatchat/inference/
xinference launch --model-engine vllm --model-name deepseek-r1-distill-qwen --size-in-billions 14 --model-format pytorch -e http://0.0.0.0:9997 --model_path /data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B --gpu-idx 0,1,2,3 --dtype float16 --block_size 4096 --max_model_len 4096
```

## 2.4. 运行 embedding 模型

**以下操作均在Docker容器vllm推理环境中进行。**

启动前请确认实际模型路径，并修改目录位置：/data/models/llm/models/bge-large-zh-v1.5

```bash
xinference launch --model-name bge-large-zh-v1.5 --model-type embedding -e http://0.0.0.0:9997 --model_path /data/models/llm/models/bge-large-zh-v1.5 --gpu-idx 0,1,2,3 --dtype float16
```

## 2.5. xinference自定义模型参考命令

Xinference 启动自定义模型参考教程： https://inference.readthedocs.io/zh-cn/stable/models/custom.html

## 2.6. xinference WebUI 验证

打开浏览器，根据实际`服务器IP`地址，输入以下网址：http://192.168.0.11:9997 ，如下所示：

<p align="left">
    <img alt="langchain-chatchat_xinference_webui_1_s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/langchain-chatchat/res/xinference_webui_1_s.gif" width="640" />
    <img alt="langchain-chatchat_xinference_webui_2_s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/langchain-chatchat/res/xinference_webui_2_s.gif" width="640" />
    <img alt="langchain-chatchat_xinference_webui_3_s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/langchain-chatchat/res/xinference_webui_3_s.gif" width="640" />
</p>

# 3. Langchain-chatchat 启动和配置

```shell
#设置环境变量
export CHATCHAT_ROOT=/workspace/langchain-chatchat/chatchat_data
#如配置文件有修改，修改后覆盖原来配置文件，参考如下：
#cp -rvf /home/share/pytorch2.5/langchain-chatchat/tools/model_settings.yaml /workspace/langchain-chatchat/chatchat_data/
#配置文件修改后，需要再初始化一遍知识库
#chatchat kb -r
#安装 httpx ，否则打开webui终端会报错。
#pip install httpx==0.27.2
#启动项目
chatchat start -a
```
出现以下界面即为启动成功, 然后在局域网中任意台PC浏览器上输入部署服务器的IP地址替换0.0.0.0，即如【http://192.168.0.110:8501】:

```bash
  You can now view your Streamlit app in your browser.

  URL: http://0.0.0.0:8501
```

打开浏览器，根据实际`服务器IP`地址，输入网址如：http://192.168.0.11:9997 ，进行后续操作：

<p align="left">
    <img alt="langchain-chatchat_webui_s.gif" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/langchain-chatchat/res/langchain-chatchat_webui_s.gif" width="640" />
</p>

>![](../../res/note.gif) **备注：**
>- 由于 chatchat 配置默认监听地址 `DEFAULT_BIND_HOST` 为 127.0.0.1, 所以无法通过其他 ip 进行访问。
>- 如需通过机器ip 进行访问(如 Linux 系统), 需要到 `basic_settings.yaml` 中将监听地址修改为 0.0.0.0。


# 4. 执行一键启动脚本

为方便后续操作，以上第二、三章节容器内操作，也可使用一键启动脚本，启动 Xinference 和 Langchain-chatchat；

*注意：以下三个操作，需要分别在三个终端进入容器内部执行。*

**终端一：**
```bash
#执行一键启动脚本，以下操作均在Docker容器中进行。
cd /home/share/pytorch2.5/langchain-chatchat/deploy/ && \
    bash ./1.run_xinference_services.sh
```
**终端二：**
```bash
#执行一键启动脚本，以下操作均在Docker容器中进行。
cd /home/share/pytorch2.5/langchain-chatchat/deploy/ && \
    bash ./2.launch_xinference_models.sh
```
**终端三：**
```bash
#执行一键启动脚本，以下操作均在Docker容器中进行。
cd /home/share/pytorch2.5/langchain-chatchat/deploy/ && \
    bash ./3.start_chatchat_services.sh
```