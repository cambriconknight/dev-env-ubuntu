#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     deploy_env_s.sh
# UpdateDate:   2024/11/26
# Description:  一键搭建验证环境。
# Example:      cd /home/share/pytorch2.5/langchain-chatchat/tools/ && bash deploy_env_s.sh
# Depends:
# Notes:        本脚本适用于初次进入docker容器环境；不适用于再次进入容器，可能会有意想不到问题
# -------------------------------------------------------------------------------

#切换到推理环境
#source /torch/venv3/pytorch_infer/bin/activate
## 下载代码&安装依赖库
#添加 NodeSource 仓库 并 安装 Node.js 和 npm(并且要在install前安装), 否则xinference的webUI启动不起来。
#apt-get update
#curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash -
apt-get install -y nodejs npm
mkdir -p /workspace/langchain-chatchat
cd /workspace/langchain-chatchat
#cd /home/share/pytorch2.5/langchain-chatchat
#下载Xinference
git clone https://github.com/xorbitsai/inference.git
cd ./inference
git checkout f4e3cee74d67c2caf9087a80271385ce2d431b2a 
#代码适配与配置修改并替换原文件(详细修改内容参考：https://wiki.cambricon.com/display/technology/Xinference-Deepseek-R1-distill-Qwen-20250210)
#cd /workspace/langchain-chatchat/inference
cp -rvf /home/share/pytorch2.5/langchain-chatchat/tools/core_worker.py ./xinference/core/worker.py
cp -rvf /home/share/pytorch2.5/langchain-chatchat/tools/model_rerank_core.py ./xinference/model/rerank/core.py
cp -rvf /home/share/pytorch2.5/langchain-chatchat/tools/model_llm_core.py ./xinference/model/llm/core.py
cp -rvf /home/share/pytorch2.5/langchain-chatchat/tools/model_llm_vllm_core.py ./xinference/model/llm/vllm/core.py
find ./ -type f -exec sed -i 's/CUDA_VISIBLE_DEVICES/MLU_VISIBLE_DEVICES/g' {} +
##安装nodejs npm需要在安装 Xinference 之前，否则还需要重新安装Xinference
#cd /workspace/langchain-chatchat/inference
pip install -e . -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install sentence-transformers tf-keras -i https://pypi.tuna.tsinghua.edu.cn/simple
#安装embedding依赖库，否则会在初始化知识库【chatchat kb -r】时，报错
#pip install bcembedding fastembed flagembedding
pip install flagembedding
#设置环境变量。修复指定gpu_idx失效的问题
export PYTORCH_CNDEV_BASED_MLU_CHECK=1