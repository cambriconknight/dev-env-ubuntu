<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/blob/master/pytorch2.5/langchain-chatchat/tools/FAQ.md">
        <h1 align="center">Langchain-Chatchat模型在MLU上验证期间常见问题汇总</h1>
    </a>
</p>

- [常见问题汇总](#常见问题汇总)
  - [报错【traps: xinference-loca\[15127\] trap invalid opcode ip: 】](#报错traps-xinference-loca15127-trap-invalid-opcode-ip-)
  - [如何停止已经注册的 Xinference 模型？](#如何停止已经注册的-xinference-模型)
  - [启动项目后（chatchat start -a），打开WebUI界面终端报错。报错【AttributeError: API未能返回正确的JSON。'NoneType' object has no attribute 'json'】](#启动项目后chatchat-start--a打开webui界面终端报错报错attributeerror-api未能返回正确的jsonnonetype-object-has-no-attribute-json)
  - [报错【RuntimeError: Failed to launch model, detail: \[address=0.0.0.0:23249, pid=61578\] Model custom-chatglm3-6b cannot be run on engine vllm.】](#报错runtimeerror-failed-to-launch-model-detail-address000023249-pid61578-model-custom-chatglm3-6b-cannot-be-run-on-engine-vllm)
  - [报错【error in sub thread: Error code: 500  - {'detail': "\[   8395,pid=758991 No module named'FlagEmbedding'"}】](#报错error-in-sub-thread-error-code-500----detail----8395pid758991-no-module-namedflagembedding)
  - [报错【启动 Xinference 的本地实例时，webUI启动不起来。】](#报错启动-xinference-的本地实例时webui启动不起来)

# 常见问题汇总

## 报错【traps: xinference-loca[15127] trap invalid opcode ip: 】

- 问题描述： 使用Langchain-Chatchat 离线镜像部署到C86的虚拟机(Hygon C86-3G 7390 32-core Processor)上后，运行xinference-local 报如下错误：
```bash
[Wed Mar 12 19:30:53 2025] traps: xinference-loca[15127] trap invalid opcode ip:7f63321935fa sp:7ffd712775d0 error:0 in libtensorflow_framework.so.2[7f6331a2e000+168e000]
[Wed Mar 12 19:31:42 2025] traps: xinference-loca[15355] trap invalid opcode ip:7f2e5f5935fa sp:7ffe8938cd20 error:0 in libtensorflow_framework.so.2[7f2e5ee2e000+168e000]
```
- 解决方案： 把cpu改直通（HOST PASSTHROUGH）。


## 如何停止已经注册的 Xinference 模型？

- 问题： Xinference 本地服务启动后，注册并启动玩模型后，如何查看并停止已经注册的 Xinference 模型
- 解答： 参考如下 Xinference 命令，可以查看并停止已经注册的 Xinference 模型。
```bash
#查看正在运行模型
xinference list -e http://0.0.0.0:9997
#终止正在运行模型
xinference terminate --model-uid "custom-chatglm3-6b" -e http://0.0.0.0:9997
xinference terminate --model-uid "custom-bge-large-zh-v1.5" -e http://0.0.0.0:9997
#取消注册模型,会删除 /root/.xinference/model/llm/ 或 /root/.xinference/model/embedding/ 目录下的自定义模型配置 json 文件
xinference unregister --model-type LLM --model-name custom-chatglm3-6b -e http://0.0.0.0:9997
xinference unregister --model-type embedding --model-name custom-bge-large-zh-v1.5 -e http://0.0.0.0:9997
```

## 启动项目后（chatchat start -a），打开WebUI界面终端报错。报错【AttributeError: API未能返回正确的JSON。'NoneType' object has no attribute 'json'】
- 问题描述：
```bash
  You can now view your Streamlit app in your browser.

  URL: http://0.0.0.0:8501

2025-02-01 14:19:26.647 | ERROR    | chatchat.webui_pages.utils:get:64 - TypeError: error when get /tools: Client.__init__() got an unexpected keyword argument 'proxies'
2025-02-01 14:19:26.652 | ERROR    | chatchat.webui_pages.utils:get:64 - TypeError: error when get /tools: Client.__init__() got an unexpected keyword argument 'proxies'
2025-02-01 14:19:26.652 | ERROR    | chatchat.webui_pages.utils:get:64 - TypeError: error when get /tools: Client.__init__() got an unexpected keyword argument 'proxies'
2025-02-01 14:19:26.653 | ERROR    | chatchat.webui_pages.utils:to_json:233 - AttributeError: API未能返回正确的JSON。'NoneType' object has no attribute 'json'
```
- 解决措施：安装httpx，命令如下 pip install httpx==0.27.2


## 报错【RuntimeError: Failed to launch model, detail: [address=0.0.0.0:23249, pid=61578] Model custom-chatglm3-6b cannot be run on engine vllm.】

- 问题： 运行测试时【xinference launch --model-name custom-chatglm3-6b --model-format pytorch --model-engine vllm --dtype half --block_size 4096 --max_model_len 4096 -e http://0.0.0.0:9997】，报错【RuntimeError: Failed to launch model, detail: [address=0.0.0.0:23249, pid=61578] Model custom-chatglm3-6b cannot be run on engine vllm.】
- 原因： custom-chatglm3-6b.json文件配置问题。配置文件中的 "model_family": "chatglm3"需要在【 core_llm_vllm.py 】模型列表【 VLLM_SUPPORTED_CHAT_MODELS 】中。

## 报错【error in sub thread: Error code: 500  - {'detail': "[   8395,pid=758991 No module named'FlagEmbedding'"}】

- 问题： 在初始化知识库【chatchat kb -r】时，报错【error in sub thread: Error code: 500  - {'detail': "[   8395,pid=758991 No module named'FlagEmbedding'"}】,详细报错信息如下：
```bash
2024-11-27 19:05:31.605 | ERROR    | chatchat.server.utils:run_in_thread_pool:733 - error in sub thread: Error code: 500 - {'detail': "[address=0.0.0.0:38395, pid=75899] No module named 'FlagEmbedding'"}
2024-11-27 19:05:31.661 | ERROR    | chatchat.server.knowledge_base.kb_cache.faiss_cache:load_vector_store:140 - list index out of range
2024-11-27 19:05:31.662 | ERROR    | chatchat.init_database:worker:61 - 向量库 samples 加载失败。
```
- 原因： 在xinference推理环境下安装embedding依赖库，命令如下： 【pip install flagembedding】

但安装embedding依赖库后，汇报如下冲突，是否对服务有影响，待验证。

```bash
ERROR: pips dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
vllm 0.6.1.post2+mlu0.5.0.pt2.4 requires ruff==0.1.5, but you have ruff 0.8.0 which is incompatible.
vllm 0.6.1.post2+mlu0.5.0.pt2.4 requires transformers==4.43.4, but you have transformers 4.44.2 which is incompatible.
Successfully installed cbor-1.0.0 datasets-2.19.0 flagembedding-1.3.2 fsspec-2024.3.1 ijson-3.3.0 inscriptis-2.5.0 ir-datasets-0.5.9 lz4-4.3.3 pyarrow-hotfix-0.6 transformers-4.44.2 trec-car-tools-2.6 unlzw3-0.2.2 warc3-wet-0.2.5 warc3-wet-clueweb09-0.2.5 zlib-state-0.1.9

[notice] A new release of pip is available: 24.0 -> 24.3.1
[notice] To update, run: pip install --upgrade pip
(pytorch_infer) root@worker1:/home/share#
```

## 报错【启动 Xinference 的本地实例时，webUI启动不起来。】

- 问题： 启动 Xinference 的本地实例时，webUI启动不起来。
- 原因： 安装 Node.js 和 npm, 安装完成后重新install安装服务, 否则xinference的webUI启动不起来。
```bash
# 1. 安装nodejs npm
apt install -y nodejs npm
# 2.安装nodejs npm需要在安装Xinference之前，否则还需要重新安装Xinference
cd /workspace/langchain-chatchat/inference
pip install -e . -i https://pypi.tuna.tsinghua.edu.cn/simple
```
