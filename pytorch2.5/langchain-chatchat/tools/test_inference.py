from openai import OpenAI
client = OpenAI(base_url="http://127.0.0.1:9997/v1", api_key="not used actually")
# def test_chat_completion_stream():
#     response = client.chat.completions.create(
#         model="deepseek-r1-distill-qwen",
#         messages=[
#             {"role": "system", "content": "You are a helpful assistant."},
#             {"role": "user", "content": "What is the largest animal?"}
#         ]
#     )
#     print(response.choices[0].message.content)
##流式服务
def test_chat_completion_stream():
    res = client.chat.completions.create(
                    model="deepseek-r1-distill-qwen",
                    messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": "What is the largest animal?"} ] ,
                    stream=True, temperature=0,max_tokens=1024
    )
    for chunk in res:
        try:
            content = chunk.choices[0].delta.content
            if content is None:
                content = ""
        except Exception as e:
            content = chunk.choices[0].delta.get("content", "")
        print(content, end="", flush=True)
    print()
test_chat_completion_stream()