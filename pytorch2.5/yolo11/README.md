<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/yolov11">
        <h1 align="center">YOLO11模型验证教程</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 安装依赖库](#2-安装依赖库)
- [3. 模型验证](#3-模型验证)

# 1. 环境准备

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡       | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统 |
| Driver                | cambricon-mlu-driver-centos7-6.*.*-10.x86_64.rpm        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| YOLO11 源码              | https://github.com/ultralytics/ultralytics  | branches： v8.3.50 |
| YOLO11n 模型   | https://github.com/ultralytics/assets/releases/download/v8.3.0/yolo11n.pt    | branches： v8.3.0 |
| YOLO11s 模型   | https://github.com/ultralytics/assets/releases/download/v8.3.0/yolo11s.pt    | branches： v8.3.0 |
| YOLO11m 模型   | https://github.com/ultralytics/assets/releases/download/v8.3.0/yolo11m.pt    | branches： v8.3.0 |
| YOLO11l 模型   | https://github.com/ultralytics/assets/releases/download/v8.3.0/yolo11l.pt    | branches： v8.3.0 |
| YOLO11x 模型   | https://github.com/ultralytics/assets/releases/download/v8.3.0/yolo11x.pt    | branches： v8.3.0 |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**参考资料:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 在线文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK下载: https://sdk.cambricon.com/download?component_name=PyTorch
- Ultralytics YOLO11 官方技术文档：https://docs.ultralytics.com/zh

**AIKnight公众号**
>![](../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
```
## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#加载Docker镜像
#./load-image-dev.sh ./docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#启动Docker容器
./run-container-dev.sh
```

# 2. 安装依赖库

```bash
cd /home/share/pytorch2.5/yolo11
#下载仓库
git clone https://github.com/ultralytics/ultralytics.git -b v8.3.50 --depth=1
#执行脚本转换工具，进行一键转换
python /torch/src/torch_mlu/tools/torch_gpu2mlu/torch_gpu2mlu.py -i ultralytics
#安装依赖ls -la pyproject.toml
cd ultralytics_mlu/ && pip install .
```

# 3. 模型验证

```bash
# 进入工作目录
cd /home/share/pytorch2.5/yolo11/ultralytics_mlu
# yolo11验证，根据实际设置设备编号
export MLU_VISIBLE_DEVICES=0
yolo predict model=yolo11n.pt source='https://ultralytics.com/images/bus.jpg'
#yolo predict model=yolo11s.pt source='https://ultralytics.com/images/bus.jpg'
#cp -rvf ../tools/demo.py && python demo.py
#ls -la runs/detect/predict
#cnmon 命令查看MLU硬件资源占用情况
```
