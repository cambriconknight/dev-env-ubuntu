

<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/PyTorch2.5">
        <h1 align="center">基于PyTorch2.5的网络模型验证环境搭建</h1>
    </a>
</p>

**该教程仅仅用于学习，打通流程； 不对效果负责，不承诺商用。**

# 1. 概述
本目录下脚本适配于官方发布的 PyTorch2.5 框架的 Docker 容器。 官方发布的 Docker 容器已经对 PyTorch2.5 框架进行了编译安装。可按照以下步骤可快速基于 Cambricon PyTorch2.5 框架进行各类网络的模型推理和训练等验证工作。

**运行环境**

- 主机系统: CentOS7
- 软件栈版本: 1.22.1
- 深度学习框架: Pytorch
- 镜像文件: cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
- Docker: ⽤⼾需要⾃⾏安装docker（版本要求 >= v19.03）

**硬件环境准备:**

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| MLU370-S4/X4/x8  | 一套       |使用板卡自带的8pin连接器连接主机电源|

**软件环境准备:**

| 名称           | 版本/文件                                                 | 备注                                 |
| :------------ | :-------------------------------                         | :---------------------------------- |
| Linux OS      | CentOS7                                                  | 宿主机操作系统                         |
| Driver_MLU370 | cambricon-mlu-driver-centos7-6.*.*-10.x86_64.rpm        | 依操作系统选择                         |
| Docker Image  | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz    | 官方发布的 PyTorch2.5 框架 Docker 镜像文件 |

注: 以上软件环境中文件名词, 如有版本升级及名称变化, 可以在 [env.sh](./env.sh) 中进行修改。

**下载地址:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

文档: https://developer.cambricon.com/index/document/index/classid/3.html

SDK: https://sdk.cambricon.com/download?component_name=PyTorch

# 2. 网络图谱
Cambricon PyTorch 环境下典型网络的验证教程.
| 网络/框架 | 验证教程 | 备注 |
| ------------------ | --------------------- | --------------------- |
| [EvalScope](https://github.com/modelscope/evalscope)  | [基于EvalScope框架对模型进行快速评测](./evalscope)  | [EvalScope官网文档](https://evalscope.readthedocs.io/zh-cn/latest/get_started/introduction.html) |
| [QwQ-32B](https://qwenlm.github.io/zh/blog/qwq-32b)  | [基于vLLM框架 QwenLM-32B 模型验证教程](./vllm/qwq-32b)  | 官方暂未放出完整技术报告 |
| [QWen2-VL](https://github.com/QwenLM/Qwen2.5/tree/44bf68571ae596c45b43c190b7963b6a6470b10b)  | [基于vLLM框架 Qwen2-VL 模型验证教程](./pytorch2.5/vllm/qwen2-vl)  | commit： 5b25ca8  |
| [DeepSeek-R1-Distill](https://github.com/deepseek-ai/DeepSeek-R1)  | [基于vLLM框架 DeepSeek-R1-Distill 系列模型验证教程](./vllm/deepseek)  |  DeepSeek-R1-Distill 系列模型验证 |
| [Llama-3.1](https://github.com/meta-llama/llama3)  | [基于vLLM框架 Llama-3.1 系列模型验证教程](./vllm/llama3.1)  | commit：d02f94e |
| [QWen2.5](https://github.com/QwenLM/Qwen2.5)  | [基于vLLM框架 Qwen2.5 系列模型验证教程](./vllm/qwen2.5)  | commit： 6aa24d6 |
| [Langchain-Chatchat](https://github.com/chatchat-space/Langchain-Chatchat)  | [基于 Pytorch 框架 Langchain-Chatchat 系列模型验证教程](./langchain-chatchat)  | commit: 40994eb6 |
| [LLaMA-Factory](https://github.com/hiyouga/LLaMA-Factory)  | [基于 Pytorch 框架 LLaMA-Factory 系列模型验证教程](./llamafactory)  |  commit: 1481af5  |
| [QWen2.5-VL](https://github.com/QwenLM/Qwen2.5-VL)  | [基于 Pytorch 框架 Qwen2.5-VL 系列模型验证教程](./qwen2.5-vl)  | commit： 4358bd4 |
| [YOLO11](https://github.com/ultralytics/ultralytics/releases/tag/v8.3.50)  | [基于 Pytorch 框架 YOLO11 系列模型验证教程](./yolo11)  | branches： v8.3.50 |

# 3. 下载镜像

下载官方发布的 PyTorch2.5 框架 Docker 镜像文件。 可前往寒武纪开发者社区注册账号按需下载, 也可在官方提供的专属FTP账户指定路径下载。

以下准备步骤以 cambricon_pytorch_container-<xx.xx.xx>-ubuntu22.04.tar.gz 镜像文件为例，其中 <xx.xx.xx> 表示PyTorch的版本号，例如 cambricon_pytorch_container-1.0.0-ubuntu22.04.tar.gz 。

# 4. 加载镜像
```bash
#加载Docker镜像
./load-image-dev.sh ${FULLNAME_IMAGES}
```

# 5. 启动容器
```bash
#启动容器
./run-container-dev.sh
```

# 6. 保存镜像
```bash
#保存镜像，供后续环境变更直接使用。
./save-image-dev.sh $Suffix_FILENAME
#$Suffix_FILENAME: 保存的镜像文件后缀名。
#如：执行 【./save-image-dev.sh test】 成功后：
#【镜像文件】默认为： cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
#【镜像名称】默认为： yellow.hub.cambricon.com/cambricon_pytorch_container/cambricon_pytorch_container:v25.01-torch2.5.0-ubuntu22.04-py310-test
#【容器名称】默认为： cambricon_pytorch_container-v25.01-torch2.5.0-kang
```
