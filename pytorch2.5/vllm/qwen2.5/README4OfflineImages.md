<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/vllm/qwen2.5">
        <h1 align="center">QWen2.5大模型在vLLM_MLU上推理验证</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 模型下载](#2-模型下载)
- [3. 7B模型验证](#3-7b模型验证)
  - [3.1. OpenAI测试](#31-openai测试)
  - [3.2. 性能测试](#32-性能测试)

# 1. 环境准备

​为便于 [Qwen2.5](https://github.com/QwenLM/Qwen2.5) 在MLU卡上进行功能验证，本教程采用已配置并集成好的 **vLLM_MLU Docker 离线镜像**进行被测模型的**vLLM推理**验证工作。用户仅需在已安装好驱动的MLU[硬件环境](#11-硬件环境)上，按照[软件环境](#12-软件环境)要求把准备好的 **Docker 离线镜像**和**待测模型权重**放置到指定位置，即可按照后续步骤进行各项功能测试与验证。

*注：本教程尤其适用于离线环境下，对被测网络模型进行快速功能验证。*

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡       | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-centos7-6.*.*-10.x86_64.rpm        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v24.10-torch2.4.0-torchmlu1.23.1-ubuntu22.04-py310-vllm_qwen2_5.tar.gz    | **需自行下载**，环境已配置并集成好的 **Docker 离线镜像**文件；此镜像文件可关注微信公众号 【 AIKnight 】, 发送关键字 **vllm_qwen2_5_offline_image** 自动获取； 镜像文件大约7G，请合理安排时间下载； |
| vLLM 项目代码         | https://github.com/vllm-project/vllm  | branch： vllm0.6.4.post1       |
| vLLM_MLU 适配代码     | https://github.com/Cambricon/vllm_mlu  | 暂未开源，如有需要请联系官方技术支持人员 |
| Qwen2.5 源码              | https://github.com/QwenLM/Qwen2.5  | commit： 6aa24d6 |
| Qwen2.5-7B-Instruct 模型    | https://huggingface.co/Qwen/Qwen2.5-7B-Instruct     | commit： bb46c15 |
| Qwen2.5-14B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-14B-Instruct    | commit： cf98f3b |
| Qwen2.5-32B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-32B-Instruct    | commit： 5ede1c9 |
| Qwen2.5-72B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-72B-Instruct    | commit： d3d9511 |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**参考资料:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 在线文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK下载: https://sdk.cambricon.com/download?component_name=PyTorch
- vLLM官方文档: https://docs.vllm.ai/en/latest/getting_started/quickstart.html
- OpenAI官网文档: https://platform.openai.com/docs/introduction

**AIKnight公众号**
>![](../../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
```

修改env.sh 脚本中环境变量【TestModel】值为【"-test"】：
```bash
cd /data/gitee/dev-env-ubuntu/pytorch2.5
vim env.sh
```
修改内容如下：
```bash
#TestModel=""
TestModel="-qwen2.5"
```

## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#加载Docker镜像
./load-image-dev.sh ./docker/cambricon_pytorch_container-v24.10-torch2.4.0-torchmlu1.23.1-ubuntu22.04-py310-vllm_qwen2_5.tar.gz
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#启动Docker容器
./run-container-dev.sh
```

# 2. 模型下载

具体操作步骤均参考[Qwen2.5官网说明文档](https://github.com/QwenLM/Qwen2.5)，下载模型到容器中指定目录【/workspace/qwen2.5/models/】，或者自行下载后拷贝模型文件到此目录。

*注： 考虑到离线镜像的大小，本教程只集成 **7B** 模型。*

下载链接见下表：
| 名称                   | 模型文件                             | 备注                              |
| :-------------------- | :-------------------------------  | :---------------------------------- |
| Qwen2.5-7B-Instruct 模型    | https://huggingface.co/Qwen/Qwen2.5-7B-Instruct     | commit： bb46c15 |
| Qwen2.5-14B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-14B-Instruct    | commit： cf98f3b |
| Qwen2.5-32B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-32B-Instruct    | commit： 5ede1c9 |
| Qwen2.5-72B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-72B-Instruct    | commit： d3d9511 |

可参考以下两种方式对各个模型进行下载：
```bash
# 下载模型到指定目录【/workspace/qwen2.5/models/】，或者自行下载后拷贝到此目录。
cd /workspace/qwen2.5/models/
# 安装git-lfs工具
apt install git-lfs
git lfs install
# 1> huggingface下载模型（时间较长）
git clone https://huggingface.co/Qwen/Qwen2-72B-Instruct
cd Qwen2-72B-Instruct && git checkout 1af63c6 && cd -
# 2> 魔搭下载模型
#pip install modelscope
#from modelscope import snapshot_download
##model_dir = snapshot_download('Qwen/Qwen2-72B-Instruct')
```

# 3. 7B模型验证

## 3.1. OpenAI测试

本实例是基于OpenAI进行推理验证。OpenAI使用详见[OpenAI官网文档](https://platform.openai.com/docs/introduction)： https://platform.openai.com/docs/introduction 。

1. 启动服务

```bash
# 切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/qwen2.5/tools/
# 根据实际环境，设置模型路径和服务端口号
#export MODEL_PATH=/data/models/llm/models/Qwen2.5-7B-Instruct
export MODEL_PATH=/workspace/qwen2.5/models/Qwen2.5-7B-Instruct
export SERVER_PORT=12345
###############################
export MLU_VISIBLE_DEVICES=0,1
export MLU_TP_NUM=2
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
###############################
#export MLU_VISIBLE_DEVICES=0
#export MLU_TP_NUM=1
#./run_openai_api_server.sh
```


2. 启动客户端测试

*服务启动完成后，可另启一个终端，运行以下测试实例，用来测试API调用的效果。*
*修改测试脚本*
```bash
vim /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```
按照前文内容修改如下：
```bash
# Modify OpenAI's API key and API base to use vLLM's API server.
openai_api_key = "EMPTY"
openai_api_base = "http://localhost:12345/v1"
```
或直接拷贝已经修改好的文件替换原来文件
```bash
#备份
#cp -rvf /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client-bk.py
#替换
cp -rvf /workspace/qwen2.5/tools/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```

*启动client*
```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/vllm-v0.6.2/examples
python openai_chat_completion_client.py
```

*测试结果*
```bash
(pytorch_infer) root@worker1:/workspace/vllm-v0.6.2/examples# python openai_chat_completion_client.py
Chat completion results:
ChatCompletion(id='chat-d5ab96a360cd4965ad19a3477f7adbe0', choices=[Choice(finish_reason='stop', index=0, logprobs=None, message=ChatCompletionMessage(content='2020年世界职业棒球大赛在佛罗里达州的两座球馆举行，由于新冠疫情，比赛移师到佛罗里达州的佛罗里达大西洋大学球馆（Fab-ulous）和坦帕湾阳光体育场进行。这两座球馆都是中立场地，以便确保比赛能够在疫情期间安全进行。', refusal=None, role='assistant', audio=None, function_call=None, tool_calls=[]), stop_reason=None)], created=1732004159, model='/data/models/llm/models/Qwen2.5-7B-Instruct', object='chat.completion', service_tier=None, system_fingerprint=None, usage=CompletionUsage(completion_tokens=76, prompt_tokens=65, total_tokens=141, completion_tokens_details=None, prompt_tokens_details=None), prompt_logprobs=None)
```

## 3.2. 性能测试

**测试步骤**

```bash
# 进入工作目录
cd /workspace/qwen2.5/tools/
# 根据实际环境，设置模型路径和运行板卡
export MODEL_PATH=/workspace/qwen2.5/models/Qwen2.5-7B-Instruct
export MODEL_Name=qwen2.5-7b
###############################
#for 300
export MLU_VISIBLE_DEVICES=0,1
export MLU_TP_NUM=2
# 执行多组参数的性能测试脚本
./benchmark4vllm_7b_300.sh
###############################
#for >300
export MLU_VISIBLE_DEVICES=0
export MLU_TP_NUM=1
# 执行多组参数的性能测试脚本
./benchmark4vllm_7b.sh
###############################
# 查看性能测试结果
ls -la ../log/
```
测试完后，性能测试结果见【/workspace/qwen2.5/log】

**汇总结果**

如测试多组性能数据后，可使用[一键导出脚本](./tools/export_testlog2excel.py)对其所有日志文件导出汇总到一个Excel文件中，方便对比性能数据，

```bash
# 进入工作目录
cd /workspace/qwen2.5/tools
# 执行一键导出脚本，导出 Excel 格式的汇总数据。
python export_testlog2excel.py
# 查看文件(以下为默认文件，请以实际为准)
ls -la ../log/report.xlsx
```
*注：如需修改【--gpu-memory-utilization】参数，可以对比修改如下文件：*
```bash
#对比修改benchmark_latency.py
vimdiff /workspace/vllm-v0.6.2/benchmarks/benchmark_latency.py /home/share/pytorch2.5/vllm/tools/benchmark_latency.py
#cp -rvf /workspace/vllm-v0.6.2/benchmarks/benchmark_latency.py /workspace/vllm-v0.6.2/benchmarks/benchmark_latency.py.bk
#cp -rvf /home/share/pytorch2.5/vllm/tools/benchmark_latency.py /workspace/vllm-v0.6.2/benchmarks/benchmark_latency.py
#测试脚本【python benchmarks/benchmark_latency.py】中增加参数 【--gpu-memory-utilization=0.98】
```
