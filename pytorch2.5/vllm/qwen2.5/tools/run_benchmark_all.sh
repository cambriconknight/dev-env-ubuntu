#!/bin/bash
#set -ex
set -e
# -------------------------------------------------------------------------------
# Filename:     run_benchmark_all.sh
# UpdateDate:   2024/08/15
# Description:  一键运行所有规模模型的性能测试，并保存性能测试日志及测试期间的cnmon信息到各自log。
# Example:      export MLU_VISIBLE_DEVICES=0,1,2,3 && bash ./run_benchmark_all.sh
# Depends:
# Notes:        本脚本适用于docker容器环境下运行；
# -------------------------------------------------------------------------------
source "../../lib/time.sh"
#################################################################################
# 1. 72B 性能测试
main_time_start=$(date +"%s.%N")
cd /home/share/vllm_mlu/qwen2/tools
export MODEL_PATH=/data/models/Qwen2-72B-Instruct
export MODEL_Name=qwen2-72b
#export MLU_VISIBLE_DEVICES=0,1,2,3
export MLU_TP_NUM=4
./benchmark4vllm_72b.sh
#计算测试所用的时间
main_time_end=$(date +"%s.%N")
ret=$(timediff $main_time_start $main_time_end)
echo "(${MODEL_Name}) Total time: $ret s"
echo "(${MODEL_Name}) Total time: $ret s" > ../log/${MODEL_Name}_total_time.log
echo "=================================================="
#整理测试日志
sync && sync
python export_testlog2excel.py
mv ../log/report.xlsx ../log/${MODEL_Name}_report.xlsx
if [ -d "./log_vllm_mlu_${MODEL_Name}" ]; then rm -rvf "./log_vllm_mlu_${MODEL_Name}";fi
cp -rvf ../log/ ../log_vllm_mlu_${MODEL_Name} && sync && sync
pushd ../ && tar zcvf log_vllm_mlu_${MODEL_Name}.tar.gz log_vllm_mlu_${MODEL_Name} && popd
rm -rvf ../log/*.log ../log/*.xlsx ../log/*.csv
#################################################################################
