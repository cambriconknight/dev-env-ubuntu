from openai import OpenAI

# Modify OpenAI's API key and API base to use vLLM's API server.
openai_api_key = "EMPTY"
openai_api_base = "http://localhost:12345/v1"

client = OpenAI(
    # defaults to os.environ.get("OPENAI_API_KEY")
    api_key=openai_api_key,
    base_url=openai_api_base,
)

models = client.models.list()
model = models.data[0].id

chat_completion = client.chat.completions.create(
    messages=[{
        "role": "system",
        "content": "你是一个能干的助手."
    }, {
        "role": "user",
        "content": "谁赢得了2020年的世界职业棒球大赛?"
    }, {
        "role":
        "assistant",
        "content":
        "洛杉矶道奇队在2020年赢得了世界职业棒球大赛冠军."
    }, {
        "role": "user",
        "content": "它在哪里举办的?"
    }],
    model=model,
)


print("Chat completion results:")
print(chat_completion)
