<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/vllm/deepseek">
        <img alt="continuously_updating" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/res/continuously_updating.gif" height="140" />
        <h1 align="center">基于vLLM_MLU的DeepSeek-R1-Distill系列模型验证教程</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 切换到vLLM推理环境](#2-切换到vllm推理环境)
- [3. Qwen-14B	模型验证](#3-qwen-14b模型验证)
  - [3.1. OpenAI测试](#31-openai测试)
  - [3.2. 性能测试](#32-性能测试)
- [4. Qwen-32B	模型验证](#4-qwen-32b模型验证)
  - [4.1. OpenAI测试](#41-openai测试)
  - [4.2. 性能测试](#42-性能测试)
- [5. Llama-8B	模型验证](#5-llama-8b模型验证)
  - [5.1. OpenAI测试](#51-openai测试)
  - [5.2. 性能测试](#52-性能测试)
- [6. Llama-70B	模型验证](#6-llama-70b模型验证)
  - [6.1. OpenAI测试](#61-openai测试)
  - [6.2. 性能测试](#62-性能测试)

# 1. 环境准备

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡       | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-6.2.10-15.el7.x86_64.rpm	        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| vLLM 项目代码         | https://github.com/vllm-project/vllm  | branch： v0.6.2       |
| vLLM_MLU 适配代码     | https://github.com/Cambricon/vllm_mlu  | 暂未开源，如有需要请联系官方技术支持人员 |
| DeepSeek-V3 源码  | https://github.com/deepseek-ai/DeepSeek-V3  |  |
| DeepSeek-R1 源码  | https://github.com/deepseek-ai/DeepSeek-R1  |  |
| DeepSeek-R1-Distill-Qwen-14B 模型   | https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-14B	   | commit： 6453600 |
| DeepSeek-R1-Distill-Qwen-32B 模型   | https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-32B	   | commit： b950d47 |
| DeepSeek-R1-Distill-Llama-8B 模型   | https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Llama-8B	   | commit： 74fbf13 |
| DeepSeek-R1-Distill-Llama-70B 模型   | https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Llama-70B	   | commit： 0d6d11a |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**DeepSeek-R1-Distill Models**

| **Model** | **Base Model** | **Download** |
| :------------: | :------------: | :------------: |
| DeepSeek-R1-Distill-Qwen-1.5B  | [Qwen2.5-Math-1.5B](https://huggingface.co/Qwen/Qwen2.5-Math-1.5B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-1.5B)   |
| DeepSeek-R1-Distill-Qwen-7B  | [Qwen2.5-Math-7B](https://huggingface.co/Qwen/Qwen2.5-Math-7B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-7B)   |
| DeepSeek-R1-Distill-Llama-8B  | [Llama-3.1-8B](https://huggingface.co/meta-llama/Llama-3.1-8B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Llama-8B)   |
| DeepSeek-R1-Distill-Qwen-14B   | [Qwen2.5-14B](https://huggingface.co/Qwen/Qwen2.5-14B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-14B)   |
|DeepSeek-R1-Distill-Qwen-32B  | [Qwen2.5-32B](https://huggingface.co/Qwen/Qwen2.5-32B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-32B)   |
| DeepSeek-R1-Distill-Llama-70B  | [Llama-3.3-70B-Instruct](https://huggingface.co/meta-llama/Llama-3.3-70B-Instruct) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Llama-70B)   |

**参考资料:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 在线文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK下载: https://sdk.cambricon.com/download?component_name=PyTorch
- vLLM官方文档: https://docs.vllm.ai/en/latest/getting_started/quickstart.html
- OpenAI官网文档: https://platform.openai.com/docs/introduction
- DeepSeek官网文档: https://api-docs.deepseek.com/zh-cn/news/news1226

**AIKnight公众号**
>![](../../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/vllm
```
## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#加载Docker镜像
#./load-image-dev.sh /data/ftp/docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#启动Docker容器
./run-container-dev.sh
```

# 2. 切换到vLLM推理环境

```bash
# 修改系统时区
cp -rvf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
```

# 3. Qwen-14B	模型验证

## 3.1. OpenAI测试

本实例是基于OpenAI进行推理验证。OpenAI使用详见[OpenAI官网文档](https://platform.openai.com/docs/introduction)： https://platform.openai.com/docs/introduction 。

1. 启动服务

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools/
# 根据实际环境，设置模型路径和服务端口号
export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B
export SERVER_PORT=12345
###############################
export MLU_VISIBLE_DEVICES=0,1,2,3
export MLU_TP_NUM=4
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
###############################
```

2. 启动客户端测试

*服务启动完成后，可另启一个终端，运行以下测试实例，用来测试API调用的效果。*
*修改测试脚本*
```bash
vim /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```
按照前文内容修改如下：
```bash
# Modify OpenAI's API key and API base to use vLLM's API server.
openai_api_key = "EMPTY"
openai_api_base = "http://localhost:12345/v1"
```
或直接拷贝已经修改好的文件替换原来文件
```bash
#备份
#cp -rvf /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client-bk.py
#替换
cp -rvf /home/share/pytorch2.5/vllm/deepseek/tools/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```

*启动client*
```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/vllm-v0.6.2/examples
python openai_chat_completion_client.py
```

*测试结果*
```bash
(pytorch_infer) root@worker1:/workspace/vllm-v0.6.2/examples# python openai_chat_completion_client.py
Chat completion results:
ChatCompletion(id='chatcmpl-1af3838f6e7241708102192845a329c0', choices=[Choice(finish_reason='stop', index=0, logprobs=None, message=ChatCompletionMessage(content='<think>\n嗯，用户问的是“它在哪里举办的？”这里的“它”指的是2020年的世界职业棒球大赛。首先，我需要确认用户指的是棒球大赛的决赛，也就是世界大赛。在之前的对话中，我已经告诉用户洛杉矶道奇队赢得了冠军，所以用户现在想知道比赛在哪里举行。\n\n我应该先回想一下2020年世界大赛的信息。我记得由于疫情的原因，那一年的举办地点有些特别。通常世界大赛是在赢了美国联盟的球队的主场进行，但2020年情况不同。\n\n2020年世界大赛是在美国职棒大联盟（MLB）的中立球场举行的，这有点不同寻常。中立球场通常是棒球季后赛中的一个概念，比如在分区赛或冠军赛中使用，而世界大赛通常是在一支球队的主场进行。所以，用户可能对这个安排感到好奇。\n\n我需要具体说明举办地点，应该是美国职棒大联盟的中立球场。可能用户还想了解为什么选择中立场，所以我可以补充一下原因，比如疫情限制了球迷到场，或者为了公平起见。\n\n另外，考虑到用户可能对棒球不太熟悉，我应该用简单的语言解释，避免过于技术化的术语。这样用户更容易理解，也能满足他们的好奇心。\n\n总结一下，我应该回答说2020年世界大赛是在中立的美国职棒大联盟球场举行的，并简要说明原因，这样用户就能全面了解情况了。\n</think>\n\n2020年世界职业棒球大赛（World Series）的举办地点是美国职棒大联盟（Major League Baseball）的中立球场。由于COVID-19疫情的影响，比赛没有在任何一支球队的主场进行，而是选择在中立的棒球场举办。这些比赛是在美国佛罗里达州的**坦帕湾（Tampa, Florida）**进行的，坦帕湾是洛杉矶道奇队的对手——坦帕湾光芒队的主场。', refusal=None, role='assistant', audio=None, function_call=None, tool_calls=[]), stop_reason=None)], created=1738840453, model='/data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B', object='chat.completion', service_tier=None, system_fingerprint=None, usage=CompletionUsage(completion_tokens=416, prompt_tokens=48, total_tokens=464, completion_tokens_details=None, prompt_tokens_details=None), prompt_logprobs=None)
```

## 3.2. 性能测试

**测试步骤**

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools/
# 根据实际环境，设置模型路径和运行板卡
#export MODEL_PATH=/data/models/llm/models/Qwen2.5-14B-Instruct
#export MODEL_Name=Qwen2.5-14B-Instruct
################################
#export MODEL_PATH=/data/models/llm/models/Qwen2.5-14B
#export MODEL_Name=Qwen2.5-14B
###############################
export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B
export MODEL_Name=DeepSeek-R1-Distill-Qwen-14B
###############################
#for 300
export MLU_VISIBLE_DEVICES=0,1,2,3
export MLU_TP_NUM=4
# 执行多组参数的性能测试脚本
./benchmark4vllm_14b_300.sh
###############################
# 查看性能测试结果
ls -la ../log/
```
测试完后，性能测试结果见【/home/share/pytorch2.5/vllm/deepseek/log】

**汇总结果**

如测试多组性能数据后，可使用[一键导出脚本](./tools/export_testlog2excel.py)对其所有日志文件导出汇总到一个Excel文件中，方便对比性能数据，

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools
# 执行一键导出脚本，导出 Excel 格式的汇总数据。
python export_testlog2excel.py
# 查看文件(以下为默认文件，请以实际为准)
ls -la ../log/report.xlsx
```

# 4. Qwen-32B	模型验证

## 4.1. OpenAI测试

本实例是基于OpenAI进行推理验证。OpenAI使用详见[OpenAI官网文档](https://platform.openai.com/docs/introduction)： https://platform.openai.com/docs/introduction 。

1. 启动服务

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools/
# 根据实际环境，设置模型路径和服务端口号
export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Qwen-32B
export SERVER_PORT=12345
###############################
export MLU_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
export MLU_TP_NUM=8
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
###############################
```

2. 启动客户端测试

*服务启动完成后，可另启一个终端，运行以下测试实例，用来测试API调用的效果。*
*修改测试脚本*
```bash
vim /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```
按照前文内容修改如下：
```bash
# Modify OpenAI's API key and API base to use vLLM's API server.
openai_api_key = "EMPTY"
openai_api_base = "http://localhost:12345/v1"
```
或直接拷贝已经修改好的文件替换原来文件
```bash
#备份
#cp -rvf /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client-bk.py
#替换
cp -rvf /home/share/pytorch2.5/vllm/deepseek/tools/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```

*启动client*
```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/vllm-v0.6.2/examples
python openai_chat_completion_client.py
```

*测试结果*
```bash
(pytorch_infer) root@worker1:/workspace/vllm-v0.6.2/examples# python openai_chat_completion_client.py
Chat completion results:
ChatCompletion(id='chatcmpl-cf7d1531333c42f48312198e0fa2dcf1', choices=[Choice(finish_reason='stop', index=0, logprobs=None, message=ChatCompletionMessage(content='<think>\n嗯，用户之前问过2020年世界职业棒球大赛的冠军是谁，我回答了洛杉矶道奇队。现在用户又问“它在哪里举办的？”，看起来是在继续探讨那届比赛的情况。\n\n首先，我需要确认2020年比赛的举办地点。我记得那年因为疫情，很多棒球比赛都改在封闭的场地进行，可能没有现场观众。世界大赛通常会在两支参赛队的主场轮流举办，但2020年可能有特殊情况。\n\n查了一下资料，2020年世界大赛确实是在阿灵顿的美国国家棒球公园举办的，而不是传统的轮流主场。这是因为疫情的影响，主办方可能为了减少旅行和控制疫情而选择一个中立的地点。\n\n用户可能是棒球迷，想了解那届比赛的细节，或者对棒球赛事的举办地点感兴趣。也可能他们想计划去现场观看比赛，了解往年的举办地。考虑到疫情，用户可能也对赛事的安排和安全措施有疑问，但这次问题只问了地点，所以重点放在那里。\n\n总结一下，我需要回答比赛地点是阿灵顿的美国国家棒球公园，并且提到这是因为在疫情期间的特殊安排。这样用户就能全面了解那届比赛的情况了。\n</think>\n\n2020年的世界职业棒球大赛（World Series）在美国得克萨斯州的阿灵顿（Arlington）举办，比赛场地是**美国国家棒球公园（ Globe Life Field）**。这届赛事由于受到 COVID-19 疫情的影响，采取了特殊的赛制和安排，所有比赛都在一个中立场地进行，而不是传统的轮流在两个参赛队的主场举办。洛杉矶道奇队最终赢得了冠军。', refusal=None, role='assistant', audio=None, function_call=None, tool_calls=[]), stop_reason=None)], created=1738908395, model='/data/models/llm/models/DeepSeek-R1-Distill-Qwen-32B', object='chat.completion', service_tier=None, system_fingerprint=None, usage=CompletionUsage(completion_tokens=367, prompt_tokens=48, total_tokens=415, completion_tokens_details=None, prompt_tokens_details=None), prompt_logprobs=None)
```

## 4.2. 性能测试

**测试步骤**

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools/
# 根据实际环境，设置模型路径和运行板卡
#export MODEL_PATH=/data/models/llm/models/Qwen2.5-32B-Instruct
#export MODEL_Name=Qwen2.5-32B-Instruct
###############################
export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Qwen-32B
export MODEL_Name=DeepSeek-R1-Distill-Qwen-32B
###############################
#for 300
export MLU_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
export MLU_TP_NUM=8
# 执行多组参数的性能测试脚本
./benchmark4vllm_32b_300.sh
###############################
# 查看性能测试结果
ls -la ../log/
```
测试完后，性能测试结果见【/home/share/pytorch2.5/vllm/deepseek/log】

**汇总结果**

如测试多组性能数据后，可使用[一键导出脚本](./tools/export_testlog2excel.py)对其所有日志文件导出汇总到一个Excel文件中，方便对比性能数据，

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools
# 执行一键导出脚本，导出 Excel 格式的汇总数据。
python export_testlog2excel.py
# 查看文件(以下为默认文件，请以实际为准)
ls -la ../log/report.xlsx
```

*执行一键导出脚本实例：*

<p align="left">
    <img alt="DeepSeek-R1-Distill-Qwen-32B-benchmark-log2report-s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/vllm/deepseek/res/DeepSeek-R1-Distill-Qwen-32B-benchmark-log2report-s.gif" width="640" />
</p>

<p align="left">
    <img alt="export_testlog2excel" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/vllm/deepseek/res/export_testlog2excel.gif" width="640" />
</p>

*注：图中数据已做屏蔽处理，真实性能数据以实测为准。*


# 5. Llama-8B	模型验证

## 5.1. OpenAI测试

本实例是基于OpenAI进行推理验证。OpenAI使用详见[OpenAI官网文档](https://platform.openai.com/docs/introduction)： https://platform.openai.com/docs/introduction 。

1. 启动服务

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools
# 根据实际环境，设置模型路径和服务端口号
export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Llama-8B
export SERVER_PORT=12345
###############################
export MLU_VISIBLE_DEVICES=0,1
export MLU_TP_NUM=2
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
###############################
```

2. 启动客户端测试

*服务启动完成后，可另启一个终端，运行以下测试实例，用来测试API调用的效果。*
*修改测试脚本*
```bash
vim /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```
按照前文内容修改如下：
```bash
# Modify OpenAI's API key and API base to use vLLM's API server.
openai_api_key = "EMPTY"
openai_api_base = "http://localhost:12345/v1"
```
或直接拷贝已经修改好的文件替换原来文件
```bash
#备份
#cp -rvf /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client-bk.py
#替换
cp -rvf /home/share/pytorch2.5/vllm/deepseek/tools/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```

*启动client*
```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/vllm-v0.6.2/examples
python openai_chat_completion_client.py
```

*测试结果*
```bash
(pytorch_infer) root@worker1:/workspace/vllm-v0.6.2/examples# python openai_chat_completion_client.py
Chat completion results:
ChatCompletion(id='chatcmpl-416296af165a48d7a1636504d4fd54c1', choices=[Choice(finish_reason='stop', index=0, logprobs=None, message=ChatCompletionMessage(content='<think>\n好的，我现在需要帮助用户解答关于2020年世界职业棒球大赛的相关问题。用户之前问了谁赢得了比赛，我回答洛杉矶道奇队在2020年赢得了冠军。现在用户接着问：“它在哪里举办的？”\n\n首先，我要确认用户的问题。用户想知道这场比赛是在哪里进行的，还是在哪里举办的。我之前回答的是洛杉矶道奇队赢得了冠军，所以我需要明确是哪个地点。用户可能对棒球比赛的举办地点不太熟悉，因此需要详细回答。\n\n接下来，我需要回忆2020年世界职业棒球大赛的情况。我记得2020年世界棒球经典赛是在美国进行的，主要是在亚特兰大和洛杉矶举办的。亚特兰大是主场地，而洛杉矶是其他比赛的举办地，特别是世界棒球经典赛的决赛场所。\n\n然后，我要组织语言，确保回答准确且易于理解。用户可能对棒球比赛的术语不太熟悉，所以我需要明确地区名称，并解释亚特兰大和洛杉矶的地理位置。同时，说明洛杉矶道奇队在这些场地的表现和胜利。\n\n最后，我要确保回答简洁，不冗长，满足用户的信息需求。检查是否有遗漏的信息，比如比赛的具体名称、举办时间或其他相关细节，但根据当前的问题，主要关注地点和胜利队伍已经足够。\n</think>\n\n2020年世界职业棒球大赛（World Baseball Classic）于2020年2月和3月在美国的多个城市举办，主要在亚特兰大（Atlanta）和洛杉矶（Los Angeles）进行。洛杉矶道奇队（Los Angeles Dodgers）在决赛中击败了日本代表队，赢得了冠军。', refusal=None, role='assistant', audio=None, function_call=None, tool_calls=[]), stop_reason=None)], created=1738997417, model='/data/models/llm/models/DeepSeek-R1-Distill-Llama-8B', object='chat.completion', service_tier=None, system_fingerprint=None, usage=CompletionUsage(completion_tokens=434, prompt_tokens=58, total_tokens=492, completion_tokens_details=None, prompt_tokens_details=None), prompt_logprobs=None)
```

## 5.2. 性能测试

**测试步骤**

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools
# 根据实际环境，设置模型路径和运行板卡
export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Llama-8B
export MODEL_Name=DeepSeek-R1-Distill-Llama-8B
###############################
#for 300
export MLU_VISIBLE_DEVICES=0,1
export MLU_TP_NUM=2
# 执行多组参数的性能测试脚本
./benchmark4vllm_8b_300.sh
###############################
# 查看性能测试结果
ls -la ../log/
```
测试完后，性能测试结果见【/home/share/pytorch2.5/vllm/deepseek/log】

**汇总结果**

如测试多组性能数据后，可使用[一键导出脚本](./tools/export_testlog2excel.py)对其所有日志文件导出汇总到一个Excel文件中，方便对比性能数据，

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools
# 执行一键导出脚本，导出 Excel 格式的汇总数据。
python export_testlog2excel.py
# 查看文件(以下为默认文件，请以实际为准)
ls -la ../log/report.xlsx
```

*执行一键导出脚本实例：*

<p align="left">
    <img alt="DeepSeek-R1-Distill-Llama-8B-benchmark-log2report-s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/vllm/deepseek/res/DeepSeek-R1-Distill-Llama-8B-benchmark-log2report-s.gif" width="640" />
</p>

<p align="left">
    <img alt="export_testlog2excel" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/vllm/deepseek/res/export_testlog2excel.gif" width="640" />
</p>

*注：图中数据已做屏蔽处理，真实性能数据以实测为准。*

# 6. Llama-70B	模型验证

## 6.1. OpenAI测试

本实例是基于OpenAI进行推理验证。OpenAI使用详见[OpenAI官网文档](https://platform.openai.com/docs/introduction)： https://platform.openai.com/docs/introduction 。

1. 启动服务

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools
# 根据实际环境，设置模型路径和服务端口号
export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Llama-70B
export SERVER_PORT=12345
###############################
export MLU_VISIBLE_DEVICES=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
export MLU_TP_NUM=16
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
###############################
```

2. 启动客户端测试

*服务启动完成后，可另启一个终端，运行以下测试实例，用来测试API调用的效果。*
*修改测试脚本*
```bash
vim /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```
按照前文内容修改如下：
```bash
# Modify OpenAI's API key and API base to use vLLM's API server.
openai_api_key = "EMPTY"
openai_api_base = "http://localhost:12345/v1"
```
或直接拷贝已经修改好的文件替换原来文件
```bash
#备份
#cp -rvf /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client-bk.py
#替换
cp -rvf /home/share/pytorch2.5/vllm/deepseek/tools/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```

*启动client*
```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/vllm-v0.6.2/examples
python openai_chat_completion_client.py
```

*测试结果*
```bash
(pytorch_infer) root@worker1:/workspace/vllm-v0.6.2/examples# python openai_chat_completion_client.py
Chat completion results:
ChatCompletion(id='chatcmpl-ee1f5db97d1148509411e44824d6beee', choices=[Choice(finish_reason='stop', index=0, logprobs=None, message=ChatCompletionMessage(content='<think>\n好，现在我要处理用户的问题。用户问的是“谁赢得了2020年的世界职业棒球大赛？”我之前已经回答过，洛杉矶道奇队赢了。现在用户的新问题是“它在哪里举办的？”\n\n首先，我需要理解用户的问题。用户可能想知道2020年世界大赛的举办地点，或者是决赛的具体场地。我记得2020年因为COVID-19疫情，世界大赛的举办方式有些不同，可能是在一个固定的球场，而不是像往年那样在两队主场之间交替进行。\n\n接下来，我应该回忆一下具体的信息。2020年世界大赛是在 Globe Life Field 在德克萨斯州阿灵顿举办的。这是道奇队和坦帕湾光芒队之间的比赛，而道奇队最终赢得了冠军。\n\n然后，我需要用中文准确地表达这个信息，确保用户能清楚理解。同时，保持回答的简洁明了，不需要过多的解释，除非用户有进一步的问题。\n\n最后，检查一下是否有拼写或语法错误，确保回答专业且准确。这样用户就能得到他们需要的信息，了解2020年世界大赛的举办地点。\n</think>\n\n2020年世界职业棒球大赛（世界大赛）在位于美国德克萨斯州阿灵顿的 **Globe Life Field** 举行。这是由于COVID-19疫情的影响， MLB决定在中立场地举办世界大赛。', refusal=None, role='assistant', audio=None, function_call=None, tool_calls=[]), stop_reason=None)], created=1739176899, model='/data/models/llm/models/DeepSeek-R1-Distill-Llama-70B', object='chat.completion', service_tier=None, system_fingerprint=None, usage=CompletionUsage(completion_tokens=345, prompt_tokens=58, total_tokens=403, completion_tokens_details=None, prompt_tokens_details=None), prompt_logprobs=None)
```

## 6.2. 性能测试

**测试步骤**

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools
# 根据实际环境，设置模型路径和运行板卡
export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Llama-70B
export MODEL_Name=DeepSeek-R1-Distill-Llama-70B
###############################
#for 300
export MLU_VISIBLE_DEVICES=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
export MLU_TP_NUM=16
# 执行多组参数的性能测试脚本
./benchmark4vllm_70b_300.sh
###############################
# 查看性能测试结果
ls -la ../log/
```
测试完后，性能测试结果见【/home/share/pytorch2.5/vllm/deepseek/log】

**汇总结果**

如测试多组性能数据后，可使用[一键导出脚本](./tools/export_testlog2excel.py)对其所有日志文件导出汇总到一个Excel文件中，方便对比性能数据，

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/deepseek/tools
# 执行一键导出脚本，导出 Excel 格式的汇总数据。
python export_testlog2excel.py
# 查看文件(以下为默认文件，请以实际为准)
ls -la ../log/report.xlsx
```

*执行一键导出脚本实例：*

<p align="left">
    <img alt="DeepSeek-R1-Distill-Llama-70B-benchmark-log2report-s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/vllm/deepseek/res/DeepSeek-R1-Distill-Llama-70B-benchmark-log2report-s.gif" width="640" />
</p>

<p align="left">
    <img alt="export_testlog2excel_70b" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/vllm/deepseek/res/export_testlog2excel_70b.gif" width="640" />
</p>

*注：图中数据已做屏蔽处理，真实性能数据以实测为准。*
