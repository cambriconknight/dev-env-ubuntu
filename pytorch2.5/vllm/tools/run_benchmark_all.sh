#!/bin/bash
#set -ex
set -e
# -------------------------------------------------------------------------------
# Filename:     run_benchmark_all.sh
# UpdateDate:   2024/08/15
# Description:  一键运行所有模型的性能测试，并保存性能测试日志及测试期间的cnmon信息到各自log。
# Example:      ./run_benchmark_all.sh
# Depends:
# Notes:        本脚本适用于docker容器环境下运行；
# 监控关键参数：
# while true; do cnmon info | grep -E "Frequency|MLU Average|IPU|Power|Usage";echo "=======================================================";sleep 1;done
# -------------------------------------------------------------------------------
source "../lib/time.sh"
#if [ ! -d "./log" ]; then mkdir -p "./log";fi
# 根据实际测试需要修改模型测试范围(模型测试目录)
ARRAY_TEST_MODEL=("baichuan2" "chatglm3" "qwen1.5")
export MLU_VISIBLE_DEVICES=0
#################################################################################
# 执行一键测试
test_model_time_start=$(date +"%s.%N")
for TEST_MODEL in ${ARRAY_TEST_MODEL[@]}
do
    echo "=================================================="
    echo "[# ${TEST_MODEL}: "
    cd "/home/share/vllm_mlu/${TEST_MODEL}/tools"
    ./run_benchmark_all.sh
    echo "=================================================="
done
test_model_time_end=$(date +"%s.%N")
ret=$(timediff $test_model_time_start $test_model_time_end)
echo "(ARRAY_TEST_MODEL) Total time: $ret s"
#echo "(ARRAY_TEST_MODEL) Total time: $ret s" > ./log/ARRAY_TEST_MODEL_total_time.log

