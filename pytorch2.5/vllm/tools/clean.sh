#!/bin/bash
set -e

# 1. 清除本目录下临时文件
rm -rvf ./log/*.log ./log/*.xlsx ./log/*.csv
# 2. 根据实际测试需要修改模型测试范围
ARRAY_TEST_MODEL=("baichuan2" "chatglm3" "qwen1.5")
#################################################################################
for TEST_MODEL in ${ARRAY_TEST_MODEL[@]}
do
    #echo "[# ${TEST_MODEL}: "
    pushd "../${TEST_MODEL}"
    ./clean.sh
    sync && sync
    ls -la "../${TEST_MODEL}/log"
    popd
done
