<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/blob/master/vllm/baichuan2/tools/FAQ.md">
        <h1 align="center">Baichuan2模型在vLLM-MLU上验证期间常见问题汇总</h1>
    </a>
</p>

- [常见问题汇总](#常见问题汇总)
  - [报错【RuntimeError: first dim of key\_cache must be great than or equal to batch\_size】](#报错runtimeerror-first-dim-of-key_cache-must-be-great-than-or-equal-to-batch_size)
  - [LLM 推理性能测试指标项如何理解？](#llm-推理性能测试指标项如何理解)
  - [vLLM OpenAI引擎参数如何理解？](#vllm-openai引擎参数如何理解)

# 常见问题汇总

## 报错【RuntimeError: first dim of key_cache must be great than or equal to batch_size】

- 问题： 运行性能测试时【python benchmarks/benchmark_latency.py】，报错【RuntimeError: first dim of key_cache must be great than or equal to batch_size】
- 原因： 300系列，性能测试时，需要增加参数【--enforce-eager】

## LLM 推理性能测试指标项如何理解？
想要优化 LLM 推理，首先要了解 LLM 推理的核心指标：
- Time To First Token (TTFT): 首 Token 延迟，即从输入到输出第一个 token 的延迟。在在线的流式应用中，TTFT 是最重要的指标，因为它决定了用户体验。
- Time Per Output Token (TPOT)： 每个输出 token 的延迟（不含首个Token）。在离线的批处理应用中，TPOT 是最重要的指标，因为它决定了整个推理过程的时间。
- Latency：延迟，即从输入到输出最后一个 token 的延迟。 Latency = (TTFT) + (TPOT) * (the number of tokens to be generated). Latency 可以转换为 Tokens Per Second (TPS)：TPS = (the number of tokens to be generated) / Latency。
- Throughput：吞吐量，即每秒针对所有请求生成的 token 数。以上三个指标都针对单个请求，而吞吐量是针对所有并发请求的。

我们将 LLM 应用分为两种：
- 在线流式应用：对 TTFT、TPOT、Latency 敏感，需要尽可能快的生成 token。
- 离线批量应用：对 Throughput 敏感，需要在单位时间内尽可能多的生成 token。

而实际在某种应用（如在线流式应用），我们也应该在Latency 和 Throughput 之间进行权衡，提高 Throughtput 可以提高单个 GPU 承担的并发数，从而降低推理成本。

context latency(ms)：  首 Token 延迟。
per token latency(ms)： 每个输出 token 的延迟（不含首个Token）。
e2e latency(ms)： 延迟， 即从输入到输出最后一个 token 的延迟。
e2e throughput(tokens/s)： 端到端的吞吐性能。
decoder throughput(tokens/s)：

## vLLM OpenAI引擎参数如何理解？

完整说明见官网：https://docs.vllm.ai/en/latest/serving/openai_compatible_server.html

在使用vLLM引擎时，我们可以通过一系列丰富的参数进行细致的配置以优化模型性能和资源利用。以下是对每个关键引擎参数的详细解释：

- --model <model_name_or_path>：指定要使用的HuggingFace模型的名称或路径。
- --tokenizer <tokenizer_name_or_path>：设置所使用的HuggingFace分词器的名称或路径。
- --revision <revision>：选择特定的模型版本，可以是分支名、标签名或提交ID。如果不指定，则默认使用最新版本。
- --tokenizer-revision <revision>：指定特定的分词器版本，同样支持分支名、标签名或提交ID，默认使用默认版本。
- --tokenizer-mode {auto,slow}：设置分词器模式。“auto”模式会优先选用快速分词器（若可用），“slow”模式始终使用慢速分词器。
- --trust-remote-code：信任来自HuggingFace的远程代码。
- --download-dir <directory>：定义权重下载和加载的目录，默认为HuggingFace的默认缓存目录。
- --load-format {auto,pt,safetensors,npcache,dummy}：设置模型权重加载格式。"auto"会尝试以safetensors格式加载并降级到PyTorch二进制格式；其他选项分别对应不同的加载方式，包括PyTorch二进制、safetensors格式、带有numpy缓存的PyTorch格式以及随机初始化权重（主要用于性能分析）。
- --dtype {auto,half,float16,bfloat16,float,float32}：设定模型权重和激活的数据类型。“auto”会根据模型类型自动选择精度；其他选项则对应不同的浮点数精度。
- --max-model-len <length>：模型上下文长度，未指定时将根据模型配置自动确定。
- --worker-use-ray：使用Ray进行分布式服务，当使用超过1个GPU时会自动启用。
- --pipeline-parallel-size (-pp) <size>：管道并行阶段的数量。
- --tensor-parallel-size (-tp) <size>：张量并行副本的数量。
- --max-parallel-loading-workers <workers>：通过分批次加载模型以避免在使用张量并行和大型模型时出现内存溢出问题。
- --block-size {8,16,32}：连续令牌块大小。
- --enable-prefix-caching：开启自动前缀缓存功能。
- --seed <seed>：用于操作的随机种子。
- --swap-space <size>：每块GPU的CPU交换空间大小（以GiB计）。
- --gpu-memory-utilization <fraction>：设置模型执行器使用的GPU内存比例，范围从0到1，如0.5表示使用50%的GPU内存。
- --max-num-batched-tokens <tokens>：每迭代的最大批处理令牌数量。
- --max-num-seqs <sequences>：每迭代的最大序列数量。
- --max-paddings <paddings>：每批数据中的最大填充数量。
- --disable-log-stats：禁用统计日志记录。
- --quantization (-q) {awq,squeezellm,None}：指定权重量化的方法，可以选择自适应权重量化（AWQ）、SqueezeLLM或其他无量化方法。

其他参考资料：

https://juejin.cn/post/7358633811514654758

https://juejin.cn/post/7353069220827578418

