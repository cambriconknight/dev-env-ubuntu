<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/vllm/qwen2-vl">
        <h1 align="center">基于vLLM-MLU的QWen2-VL模型验证教程</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 切换到vLLM推理环境](#2-切换到vllm推理环境)
- [3. 7B模型验证](#3-7b模型验证)
  - [3.1. OpenAI接口测试](#31-openai接口测试)
  - [3.2. qwen-vl推理测试](#32-qwen-vl推理测试)
    - [3.2.1 安装依赖库](#321-安装依赖库)
    - [3.2.2 推理测试](#322-推理测试)

# 1. 环境准备

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡       | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                          | 备注                                 |
| :-------------------- | :-------------------------------  | :---------------------------------- |
| Linux OS              | CentOS7                           | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-centos7-6.*.*-10.x86_64.rpm   | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| vLLM 项目代码         | https://github.com/vllm-project/vllm  | branch： vllm0.6.4.post1       |
| Qwen2.5-VL 源码           | https://github.com/QwenLM/Qwen2.5-VL/tree/5b25ca8b690644cf11d0254f2e47e062e996e08e  | commit： 5b25ca8 |
| Qwen2-VL-7B-Instruct 模型   | https://modelscope.cn/models/Qwen/Qwen2-VL-7B-Instruct    | commit： 40826b8 |
| Qwen2-VL-72B-Instruct 模型  | https://modelscope.cn/models/Qwen/Qwen2-VL-72B-Instruct   | commit： c598eba |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu  | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**参考资料:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 在线文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK下载: https://sdk.cambricon.com/download?component_name=PyTorch
- vLLM官方文档: https://docs.vllm.ai/en/latest/getting_started/quickstart.html
- OpenAI官网文档: https://platform.openai.com/docs/introduction

**AIKnight公众号**
>![](../../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/vllm
```
## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#加载Docker镜像
#./load-image-dev.sh ./docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#启动Docker容器
./run-container-dev.sh
```

# 2. 切换到vLLM推理环境

```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
```

# 3. 7B模型验证

## 3.1. OpenAI接口测试

本实例是基于OpenAI进行推理验证。OpenAI使用详见[OpenAI官网文档](https://platform.openai.com/docs/introduction)： https://platform.openai.com/docs/introduction 。

1. 启动服务

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/qwen2-vl/tools/
# 根据实际环境，设置模型路径和服务端口号
export MODEL_PATH=/data/models/llm/models/Qwen2-VL-7B-Instruct
export SERVER_PORT=12345
###############################
export MLU_VISIBLE_DEVICES=0,1
export MLU_TP_NUM=2
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
```

2. 启动客户端测试

*服务启动完成后，可另启一个终端，运行以下测试实例，用来测试API调用的效果。*

*测试命令*

```bash
# Call the server using curl:
curl -X POST "http://localhost:12345/v1/chat/completions" \
    -H "Content-Type: application/json" \
    --data '{
        "model": "/data/models/llm/models/Qwen2-VL-7B-Instruct",
        "messages": [
            {
                "role": "user",
                "content": [
                    {
                        "type": "text",
                        "text": "Describe this image in one sentence."
                    },
                    {
                        "type": "image_url",
                        "image_url": {
                            "url": "https://cdn.britannica.com/61/93061-050-99147DCE/Statue-of-Liberty-Island-New-York-Bay.jpg"
                        }
                    }
                ]
            }
        ]
    }'
```

*测试结果*

```bash
{"id":"chatcmpl-286248b7eb8a497a93e1e40ccec4ee14","object":"chat.completion","created":1739936174,"model":"/data/models/llm/models/Qwen2-VL-7B-Instruct","choices":[{"index":0,"message":{"role":"assistant","content":"The image features the Statue of Liberty standing on Liberty Island, with the iconic New York City skyline in the background, including the Empire State Building.","tool_calls":[]},"logprobs":null,"finish_reason":"stop","stop_reason":null}],"usage":{"prompt_tokens":2195,"total_tokens":2225,"completion_tokens":30,"prompt_tokens_details":null},"prompt_logprobs":null}
```

## 3.2. qwen-vl推理测试

### 3.2.1 安装依赖库

```bash
pip install qwen-vl-utils
```

### 3.2.2 推理测试

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/qwen2-vl/tools/
export PYTORCH_CNDEV_BASED_MLU_CHECK=1
# test cmd for 2B： 
python test_qwen2vl.py --model_path /data/models/llm/models/Qwen2-VL-2B-Instruct
# test cmd for 7B： 
python test_qwen2vl.py --model_path /data/models/llm/models/Qwen2-VL-7B-Instruct
```
