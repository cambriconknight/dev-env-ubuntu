#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     run_openai_api_server_300.sh
# UpdateDate:   2024/05/11
# Description:  一键启动vLLM OpenAI APIServer。
# Example:      ./run_openai_api_server_300.sh
# Depends:
# Notes:        本脚本适用于初次进入docker容器环境；不适用于再次进入容器，可能会有意想不到问题
# -------------------------------------------------------------------------------

# 进入工作目录
CUR_DIR=$(cd $(dirname $0);pwd)
pushd "${CUR_DIR}/.."
#设置模型路径与服务端口
export USE_PAGED=false
export BLOCK_SIZE=32768
export MAX_TOTAL_TOKENS=4096

#启动服务
python -m vllm.entrypoints.openai.api_server  \
        --host localhost \
        --port ${SERVER_PORT} \
        --block-size ${BLOCK_SIZE} \
        --max-model-len ${MAX_TOTAL_TOKENS} \
        --trust-remote-code \
        --dtype float16 \
        --model ${MODEL_PATH}  \
        --tensor-parallel-size ${MLU_TP_NUM}
