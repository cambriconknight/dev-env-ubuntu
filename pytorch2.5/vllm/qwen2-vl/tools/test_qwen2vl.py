# test cmd for 2B： python test_qwen2vl.py --model_path /data/models/llm/models/Qwen2-VL-2B-Instruct
# test cmd for 7B： python test_qwen2vl.py --model_path /data/models/llm/models/Qwen2-VL-7B-Instruct

# image case
img1_messages = [
    {
        "role": "user",
        "content": [
            {
                "type": "image",
                "image": "https://qianwen-res.oss-cn-beijing.aliyuncs.com/Qwen-VL/assets/demo.jpeg",
            },
            {"type": "text", "text": "Describe this image."},
        ],
    }
]
# image case
img2_messages = [
     {
         "role": "user",
         "content": [
             {
                 "type": "image",
                 "image": "https://modelscope.oss-cn-beijing.aliyuncs.com/resource/qwen.png"
             },
             {"type": "text", "text": "What is the text in the illustrate?"},
         ],
     }
]
# video case
video1_messages = [
    {
        "role": "user",
        "content": [
            {
                "type": "video",
                "video": "https://qianwen-res.oss-cn-beijing.aliyuncs.com/Qwen2-VL/space_woaudio.mp4",
            },
            {"type": "text", "text": "Describe this video."},
        ],
    }
]
# video case
video2_messages = [
    {
        "role": "user",
        "content": [
            {
                "type": "video",
                "video": "https://media.w3.org/2010/05/sintel/trailer.mp4"
            },
            {"type": "text", "text": "Describe this video."},
        ],
    }
]

from PIL import Image
from transformers import AutoProcessor
from vllm import LLM, SamplingParams
from qwen_vl_utils import process_vision_info
from torch.distributed import destroy_process_group
from torch.distributed import init_process_group
import argparse
import gc
import sys
#import tempfile
#import shutil

def multimodal_input(processor, messages):  
    prompt = processor.apply_chat_template(
        messages, tokenize=False, add_generation_prompt=True,
    )
    image_inputs, video_inputs = process_vision_info(messages)
    mm_data = {}
    if image_inputs is not None:
        mm_data['image'] = image_inputs
    if video_inputs is not None:
        mm_data['video'] = video_inputs
      
    llm_inputs = {
        'prompt': prompt,
        'multi_modal_data': mm_data,
    }
    return llm_inputs

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--model_path', type=str, default='/data/models/llm/models/Qwen2-VL-7B-Instruct', help='Path to the model')
    #parser.add_argument('--model_path', type=str, default='/data/models/llm/models/Qwen2-VL-2B-Instruct', help='Path to the model')

    args = parser.parse_args()
    MODEL_PATH = args.model_path
    #MODEL_PATH = '/data/models/llm/models/Qwen2-VL-7B-Instruct'
    #MODEL_PATH = '/data/models/llm/models/Qwen2-VL-2B-Instruct'

    #init_process_group()
    #tmpdir = tempfile.TemporaryDirectory()
    #tmpdir = '/tmp/tmp*'

    #multimodal_cases = [img1_messages, img2_messages, video1_messages, video2_messages]
    multimodal_cases = [img1_messages, img2_messages]

    llm = LLM(
        model=MODEL_PATH,
        dtype='float16',
        tensor_parallel_size=2,
        max_seq_len_to_capture=25600,
        block_size=32768,
        max_model_len=32768,
        enforce_eager=True,
        gpu_memory_utilization=0.98,
        limit_mm_per_prompt={'image': 10, 'video': 10},
    )

    processor = AutoProcessor.from_pretrained(MODEL_PATH)
    sampling_params = SamplingParams(
        temperature=0.1, top_p=0.001, repetition_penalty=1.05, max_tokens=256,
        stop_token_ids=[],
    )

    results = []
    try:
        for case in multimodal_cases:
            llm_input = multimodal_input(processor, case)
            outputs = llm.generate([llm_input], sampling_params=sampling_params)
            generated_text = outputs[0].outputs[0].text
            print(generated_text)
            results.append([case, generated_text])
            print("==============Done================")
    finally:
        # 显式释放资源
        if 'llm' in locals():
            del llm
            gc.collect()
        # 刷新标准输出
        sys.stdout.flush()
    # destroy
    destroy_process_group()

    for item in results:
        print("case:", item[0])
        print("results:", item[1])

    # 显式地清理临时目录
    #shutil.rmtree("/tmp/tmp*")
    print("==============ALL Done================")
