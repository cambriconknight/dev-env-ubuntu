#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     run_openai_api_server.sh
# UpdateDate:   2024/05/11
# Description:  一键启动vLLM OpenAI APIServer。
# Example:      ./run_openai_api_server.sh
# Depends:
# Notes:        本脚本适用于初次进入docker容器环境；不适用于再次进入容器，可能会有意想不到问题
# -------------------------------------------------------------------------------

# 进入工作目录
CUR_DIR=$(cd $(dirname $0);pwd)
pushd "${CUR_DIR}/.."
#设置模型路径与服务端口
#export USE_PAGED=false
#export MAX_TOTAL_TOKENS=8192
#启动服务
python -m vllm.entrypoints.openai.api_server  \
        --host localhost \
        --port ${SERVER_PORT} \
        --block-size 16 \
        --trust-remote-code \
        --dtype float16 \
        --enforce-eager \
        --model ${MODEL_PATH}  \
        --tensor-parallel-size ${MLU_TP_NUM}
