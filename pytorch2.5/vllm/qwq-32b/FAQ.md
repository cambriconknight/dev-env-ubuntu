<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/vllm/qwq-32b/FAQ.md">
        <h1 align="center">QwQ-32B模型在MLU上验证期间常见问题汇总</h1>
    </a>
</p>

- [常见问题汇总](#常见问题汇总)
  - [一键启动vLLM OpenAI APIServer（./run\_openai\_api\_server\_300.sh），报错【ValueError: Total number of attention heads (40) must be divisible by tensor parallel size (16).】](#一键启动vllm-openai-apiserverrun_openai_api_server_300sh报错valueerror-total-number-of-attention-heads-40-must-be-divisible-by-tensor-parallel-size-16)

# 常见问题汇总

## 一键启动vLLM OpenAI APIServer（./run_openai_api_server_300.sh），报错【ValueError: Total number of attention heads (40) must be divisible by tensor parallel size (16).】
- 问题描述：
使用8卡16芯，VLLM加速大模型推理, 启动模型QWQ-32B时，报错信息如下：

```bash
ValueError: Total number of attention heads (40) must be divisible by tensor parallel size (16).
/opt/py3.10/lib/python3.10/tempfile.py:860: ResourceWarning: Implicitly cleaning up <TemporaryDirectory '/tmp/tmputp3_1p_'>
  _warnings.warn(warn_message, ResourceWarning)
sys:1: ResourceWarning: unclosed <socket.socket fd=13, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('127.0.0.1', 12345)>
ResourceWarning: Enable tracemalloc to get the object allocation traceback
```
- 解决措施：根据报错信息中的提示，修改卡芯数量为可被40整除的卡芯数量（4、8、10）后，可正常运行。


