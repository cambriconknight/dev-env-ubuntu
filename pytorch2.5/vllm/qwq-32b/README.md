<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/vllm/qwq-32b">
        <h1 align="center">基于vLLM_MLU的QwQ-32B模型验证教程</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 切换到vLLM推理环境](#2-切换到vllm推理环境)
- [3. Qwen-32B	模型验证](#3-qwen-32b模型验证)
  - [3.1. OpenAI测试](#31-openai测试)
  - [3.2. 性能测试](#32-性能测试)

# 1. 环境准备

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡       | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-centos7-6.*.*-10.x86_64.rpm        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| vLLM 项目代码         | https://github.com/vllm-project/vllm  | branch： v0.6.4.post1       |
| vLLM_MLU 适配代码     | https://github.com/Cambricon/vllm_mlu  | 暂未开源，如有需要请联系官方技术支持人员 |
| QwQ-32B 博客          | https://qwenlm.github.io/zh/blog/qwq-32b  | 注：目前还未放出完整的技术报告 |
| QwQ-32B 模型   | https://modelscope.cn/models/Qwen/QwQ-32B    | commit： b8bda72 |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/vllm/qwq-32b        | [仓库地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**参考资料:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 在线文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK下载: https://sdk.cambricon.com/download?component_name=PyTorch
- vLLM官方文档: https://docs.vllm.ai/en/latest/getting_started/quickstart.html
- OpenAI官网文档: https://platform.openai.com/docs/introduction
- QwQ-32B技术博客: https://qwenlm.github.io/zh/blog/qwq-32b

**AIKnight公众号**
>![](../../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/vllm
```
## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#加载Docker镜像
#./load-image-dev.sh ./docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#启动Docker容器
./run-container-dev.sh
```

# 2. 切换到vLLM推理环境

```bash
# 修改系统时区
cp -rvf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
```

# 3. Qwen-32B	模型验证

## 3.1. OpenAI测试

本实例是基于OpenAI进行推理验证。OpenAI使用详见[OpenAI官网文档](https://platform.openai.com/docs/introduction)： https://platform.openai.com/docs/introduction 。

1. 启动服务

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/qwq-32b/tools/
# 根据实际环境，设置模型路径和服务端口号
export MODEL_PATH=/data/models/llm/models/QwQ-32B
export SERVER_PORT=12345
###############################
#TP8 export MAX_TOTAL_TOKENS=32768
export MLU_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
export MLU_TP_NUM=8
#TP4  export MAX_TOTAL_TOKENS=4096
#export MLU_VISIBLE_DEVICES=0,1,2,3
#export MLU_TP_NUM=4
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
###############################
```

启动成功后，终端会有如下输出：

```bash
INFO:     Started server process [40336]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
INFO:     Uvicorn running on http://localhost:12345 (Press CTRL+C to quit)
```

2. 启动客户端测试

*服务启动完成后，可另启一个终端，运行以下测试实例，用来测试API调用的效果。*

2.1. 官网测试代码

*启动client*

```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /home/share/pytorch2.5/vllm/qwq-32b/tools/
python test_qwq-32b.py
```

*测试结果*

```bash
(pytorch_infer) root@worker1:/home/share/pytorch2.5/vllm/qwq-32b/tools# python test_qwq-32b.py 

====================reasoning content====================


====================content====================

Okay, so I need to figure out which is larger between 9.9 and 9.11. Hmm, let me think. Both numbers start with 9, right? So the whole number part is the same. That means I have to look at the decimal parts to see which one is bigger.

Alright, starting with 9.9. The decimal part here is 0.9, which is like nine tenths. And then 9.11 has a decimal part of 0.11, which is eleven hundredths. Wait a second, tenths and hundredths are different denominations. Maybe I should convert them to the same unit to compare properly. 

Let me write both numbers with the same number of decimal places. 9.9 is the same as 9.90, right? Because adding a zero at the end of a decimal doesn't change its value. So now I have 9.90 versus 9.11. 

Looking at the tenths place first: 9.90 has a 9 in the tenths place, and 9.11 has a 1 in the tenths place. Since 9 is definitely bigger than 1, that should mean 9.90 is larger than 9.11. Wait, but hold on, maybe I made a mistake here. Let me double-check.

Breaking it down: 9.9 is equal to 9 + 0.9, and 9.11 is 9 + 0.11. Comparing the decimal parts, 0.9 versus 0.11. If I convert 0.9 to hundredths, that would be 90 hundredths (since 0.9 = 90/100), and 0.11 is 11 hundredths. Comparing 90 to 11, 90 is way bigger. So yes, 0.9 is greater than 0.11, so 9.9 must be larger than 9.11.

But maybe I'm mixing up the places? Let me think again. The tenths place is the first digit after the decimal. So in 9.9, the first digit after the decimal is 9 (tenths), and the second digit is 0 (hundredths). In 9.11, the first digit after the decimal is 1 (tenths), and the second is 1 (hundredths). So when comparing digit by digit, the tenths place is more significant than the hundredths. Since 9 tenths is more than 1 tenth, regardless of the hundredths, 9.9 is bigger. 

Alternatively, maybe converting both numbers to fractions could help? Let's see. 9.9 is the same as 99/10, and 9.11 is 911/100. To compare 99/10 and 911/100, I can get a common denominator. The common denominator would be 100. So 99/10 is equal to 990/100. Comparing 990/100 vs. 911/100, clearly 990 is bigger than 911, so 9.9 is larger. 

Another way: if I subtract 9.11 from 9.9, the result should be positive if 9.9 is larger. Let's do that: 9.9 minus 9.11. To subtract, it's easier if both have the same number of decimal places. 9.90 minus 9.11. Subtract the hundredths: 0 - 1, can't do that, so borrow from the tenths. The tenths place is 9, so it becomes 8, and the hundredths becomes 10. Now subtract 1 from 10 hundredths: 9 hundredths. Then subtract the tenths: 8 - 1 = 7 tenths. Then subtract the whole numbers: 9 - 9 = 0. So total is 0.79. Since the result is positive, 9.9 is indeed larger. 

Wait, but maybe someone could think that 9.11 is 9 dollars and 11 cents, while 9.90 is 9 dollars and 90 cents. So obviously 90 cents is more than 11 cents. So that makes sense. 

Alternatively, thinking in terms of place value, the first number after the decimal is more significant. So even though 9.11 has two digits after the decimal, the first digit is only 1, which is less than 9 in 9.9. So the key is the tenths place. 

Hmm, seems like all the methods point to 9.9 being larger. I don't think I made a mistake here. Let me see if there's another angle. Maybe if we consider them as numbers on a number line. 9.9 is closer to 10, while 9.11 is closer to 9.1. Since 10 is greater than 9.1, 9.9 is further to the right on the number line, hence larger. 

Yeah, that's another way to visualize it. So all these different approaches confirm that 9.9 is larger than 9.11. Maybe the confusion could come from the number of decimal places? Like, 9.11 has more digits, so someone might think it's bigger, but actually, the value of each digit matters more based on their place. 

So, in conclusion, after checking multiple ways, 9.9 is definitely larger than 9.11. The key was looking at the tenths place first, which is higher in 9.9, making it the larger number.
</think>

To determine which is larger between 9.9 and 9.11, we can compare their decimal parts systematically:

1. **Align Decimal Places**:  
   Rewrite 9.9 as 9.90 to match the number of decimal places in 9.11.  
   Now we compare **9.90** vs. **9.11**.

2. **Compare Tenths Place**:  
   - The tenths digit of 9.90 is **9**.  
   - The tenths digit of 9.11 is **1**.  
   Since **9 > 1**, 9.90 is already larger than 9.11 at the tenths place.

3. **Verification (Optional)**:  
   - **Fraction Conversion**:  
     \( 9.9 = \frac{99}{10} = \frac{990}{100} \)  
     \( 9.11 = \frac{911}{100} \).  
     Since \( 990 > 911 \), \( 9.9 \) is larger.  
   - **Subtraction Test**:  
     \( 9.90 - 9.11 = 0.79 \), which is positive, confirming \( 9.9 > 9.11 \).

**Conclusion**:  
9.9 is larger than 9.11.  

(pytorch_infer) root@worker1:/home/share/pytorch2.5/vllm/qwq-32b/tools# python test_qwq-32b.py 

====================reasoning content====================


====================content====================

Okay, so I need to figure out which number is larger between 9.9 and 9.11. Hmm, let me think. Both numbers start with 9, so the whole number part is the same. That means I have to look at the decimal parts to decide which is bigger. 

First, let me write them down again to make sure I have them right: 9.9 and 9.11. Wait a second, 9.9 is the same as 9.90, right? Because adding a zero at the end of a decimal doesn't change its value. So if I convert both to the same number of decimal places, it might be easier to compare. 

So 9.9 is 9.90, and 9.11 is already 9.11. Now comparing the tenths place: both have 9 in the units place. The tenths place for 9.90 is 9, and for 9.11 it's 1. Wait, is that right? Let me check again. 

The first number is 9.9, so after the decimal, the first digit is 9, which is the tenths place. The second number is 9.11, so the first digit after the decimal is 1, which is also the tenths place. Oh! So the tenths place for 9.11 is 1, whereas for 9.9 it's 9. That's a big difference. 

Since 9 tenths is more than 1 tenth, that would mean 9.9 is larger than 9.11. But wait, hold on, maybe I made a mistake here. Because sometimes when numbers have different decimal places, you have to look at all the digits. Let me think again. 

Let me break it down step by step. 

Comparing 9.9 and 9.11:

1. Both numbers have the same whole number part: 9. So we move to the decimal parts.

2. The first decimal digit (tenths place):
   - 9.9 has 9 in the tenths place.
   - 9.11 has 1 in the tenths place.

Since 9 is greater than 1, that would mean 9.9 is larger, right? Because even though 9.11 has another digit in the hundredths place (the second 1), the tenths place is more significant. 

Wait, but maybe I'm mixing up the places. Let me confirm: 

In the number 9.11, the digits after the decimal are 1 (tenths) and 1 (hundredths). So the tenths place is indeed 1, which is less than 9. So even though 9.11 has two decimal places, the value of the tenths place is smaller. Therefore, 9.9 is larger. 

Alternatively, maybe converting them to fractions would help. Let's see:

9.9 is the same as 9 and 9/10, which is 99/10.

9.11 is 9 and 11/100, which is 911/100.

To compare 99/10 and 911/100, we can get a common denominator. The common denominator would be 100.

So 99/10 is equal to (99 * 10)/100 = 990/100.

Comparing 990/100 vs 911/100. Since 990 is greater than 911, 9.9 is larger. 

Another way: if I subtract 9.11 from 9.9, the result should be positive if 9.9 is larger. Let's do that:

9.9 minus 9.11. 

First, write them aligned by decimal:

 9.90
-9.11
------
 0.79

So the result is 0.79, which is positive, meaning 9.9 is indeed larger. 

Hmm, so all these methods point to 9.9 being the larger number. Wait, but sometimes people might get confused because 11 is more than 9, but in this case, the placement of the digits matters. Since 11 is in the hundredths place, it's actually 0.11, whereas the 9 is in the tenths place, which is 0.9. Since 0.9 is more than 0.11, that's why 9.9 is bigger. 

I think that's solid. Let me just think of another example to test my understanding. Like, is 1.3 larger than 1.12? Yes, because 0.3 is 0.30 which is more than 0.12. Similarly, 2.5 is more than 2.49, even though 49 is more than 5 in the hundredths place, but the tenths place is more significant. So same logic here applies. 

Therefore, I feel confident now that 9.9 is larger than 9.11.
</think>

9.9 is larger than 9.11. Here's the step-by-step explanation:

1. **Compare the whole numbers**: Both are 9, so move to the decimal parts.
2. **Compare tenths place**:
   - 9.9 has **9** in the tenths place (equivalent to 0.9).
   - 9.11 has **1** in the tenths place (equivalent to 0.1).
3. **Conclusion**: Since 9 tenths (0.9) is greater than 1 tenth (0.1), **9.9 is larger**.

**Verification**:
- Convert to fractions:  
  9.9 = 99/10 = 990/100  
  9.11 = 911/100  
  990/100 > 911/100  
- Subtract: 9.90 − 9.11 = 0.79 (positive result confirms 9.9 is larger).

**Answer**: 9.9 is larger than 9.11.
```


2.2. QWen测试代码

*修改测试脚本*
```bash
vim /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```
按照前文内容修改如下：
```bash
# Modify OpenAI's API key and API base to use vLLM's API server.
openai_api_key = "EMPTY"
openai_api_base = "http://localhost:12345/v1"
```
或直接拷贝已经修改好的文件替换原来文件
```bash
#备份
#cp -rvf /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client-bk.py
#替换
cp -rvf /home/share/pytorch2.5/vllm/qwq-32b/tools/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```

*启动client*
```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/vllm-v0.6.2/examples
python openai_chat_completion_client.py
```

*测试结果*
```bash
(pytorch_infer) root@worker1:/workspace/vllm-v0.6.2/examples# python openai_chat_completion_client.py
Chat completion results:
ChatCompletion(id='chatcmpl-9f3d67019b8046c487463ec6a9dd714e', choices=[Choice(finish_reason='stop', index=0, logprobs=None, message=ChatCompletionMessage(content='好的，用户现在问的是2020年世界职业棒球大赛的举办地点。首先我需要确认之前的问题已经正确回答，用户已经知道冠军是洛杉矶道奇队。接下来要处理“它在哪里举办的？”这个问题。\n\n首先，我需要回忆一下2020年世界大赛的信息。由于2020年是受疫情影响的一年，赛季结构可能有变化。通常世界大赛的举办地点是两支参赛队的主场轮流进行，但疫情可能导致了赛程调整，比如可能集中在一个中立场地。\n\n我记得2020年世界大赛的对阵双方是洛杉矶道奇队和坦帕湾光芒队。通常情况下，世界大赛会在两队的主场交替进行，但那年由于疫情，可能比赛地点有所调整。需要查证具体的举办城市。\n\n洛杉矶道奇队的主场在洛杉矶的道奇体育场，而坦帕湾光芒队的主场在坦帕的Tropicana Field。不过2020赛季因为疫情，很多比赛是在指定的“泡泡”环境中进行，季后赛可能集中在特定城市。例如，国家联盟的季后赛可能在亚利桑那州的某个地点，而美联在得克萨斯或佛罗里达？\n\n不过世界大赛的举办地通常还是两队的主场。但需要确认2020年是否有例外。查资料的话，2020年世界大赛的前两场比赛是在坦帕举行，后移师到洛杉矶吗？或者是否因为疫情，所有比赛都在一个地方进行？\n\n或者可能因为当时佛罗里达州的疫情情况，导致光芒队的主场无法作为主场比赛地？我记得坦帕湾光芒队在2020年世界大赛中最终获胜，成为冠军，但比赛地点可能因为防疫规定而调整。\n\n查证一下，实际上2020年世界大赛的前两场是在坦帕湾的主场，接下来的比赛因为佛罗里达州的防疫规定，导致光芒队无法在主场继续比赛，所以后面的比赛都转移到洛杉矶的道奇体育场进行。因此，世界大赛的举办城市包括坦帕和洛杉矶，但大部分比赛可能在洛杉矶进行。\n\n不过用户的问题可能只需要知道具体的举办城市，可能需要指出两个地点，或者主要地点。需要明确回答用户的问题，同时说明可能的情况变化。\n\n另外，用户之前的问题已经知道道奇队赢了，现在问地点，可能需要简洁回答，但也要准确。因此，正确的答案应该是坦帕湾和洛杉矶，但具体细节需要确认。\n\n再仔细回想，2020年世界大赛因为疫情，季后赛的安排比较特殊。美联冠军光芒队和国联冠军道奇队对决。原计划是前两场在坦帕，之后第三到第五场在洛杉矶，如果需要的话第六七场可能回到坦帕。但因为佛罗里达州的防疫规定，不允许在坦帕进行任何中立场地的比赛，所以所有比赛都在洛杉矶进行了。所以2020年世界大赛的所有比赛实际上都集中在洛杉矶的道奇体育场进行，这是非常罕见的情况。\n\n所以正确的回答应该是：2020年世界职业棒球大赛的举办地是洛杉矶的道奇体育场，因为疫情原因，所有比赛都集中在那里进行。虽然原本计划有在坦帕的比赛，但最终都改到了洛杉矶。\n\n需要确保这个信息准确，可能需要快速验证。例如，确认2020年世界大赛的赛地调整。根据资料，确实因为佛罗里达州的卫生部门不允许在坦帕进行比赛，所以所有比赛都在洛杉矶，所以最终的举办地是洛杉矶道奇体育场。\n\n因此，回答用户的问题时，要说明原计划的两个地点，但实际因疫情全部在洛杉矶举办。\n</think>\n\n2020年世界职业棒球大赛的举办地点原本计划在两支参赛队的主场交替进行：首场和第二场在坦帕湾光芒队的主场——佛罗里达州坦帕市的**特罗皮坎纳球场（Tropicana Field）**，接下来的比赛则移师洛杉矶道奇队的主场——加利福尼亚州洛杉矶市的**道奇体育场（Dodger Stadium）**。\n\n然而，由于新冠疫情相关的旅行限制，佛罗里达州卫生部门禁止非本州居民进入该州参加比赛，导致光芒队无法在主场进行后续比赛。因此，从第三场到第六场的所有比赛最终都改在**洛杉矶道奇体育场**进行，这是世界大赛历史上首次因疫情原因导致所有比赛集中在一个城市完成。最终，洛杉矶道奇队在主场以4胜2负击败坦帕湾光芒队，赢得冠军。', refusal=None, role='assistant', audio=None, function_call=None, tool_calls=[]), stop_reason=None)], created=1741745463, model='/data/models/llm/models/QwQ-32B', object='chat.completion', service_tier=None, system_fingerprint=None, usage=CompletionUsage(completion_tokens=975, prompt_tokens=67, total_tokens=1042, completion_tokens_details=None, prompt_tokens_details=None), prompt_logprobs=None)
```

## 3.2. 性能测试

**测试步骤**

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/qwq-32b/tools/
# 根据实际环境，设置模型路径和运行板卡
export MODEL_PATH=/data/models/llm/models/QwQ-32B
export MODEL_Name=QwQ-32B
###############################
#for 300
export MLU_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
export MLU_TP_NUM=8
# 执行多组参数的性能测试脚本
./benchmark4vllm_32b_300.sh
###############################
##for >300
#export MLU_VISIBLE_DEVICES=0
#export MLU_TP_NUM=1
## 执行多组参数的性能测试脚本
#./benchmark4vllm_32b.sh
###############################
# 查看性能测试结果
ls -la ../log/
```
测试完后，性能测试结果见【/home/share/pytorch2.5/vllm/qwq-32b/log】

**汇总结果**

如测试多组性能数据后，可使用[一键导出脚本](./tools/export_testlog2excel.py)对其所有日志文件导出汇总到一个Excel文件中，方便对比性能数据，

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/qwq-32b/tools
# 执行一键导出脚本，导出 Excel 格式的汇总数据。
python export_testlog2excel.py
# 查看文件(以下为默认文件，请以实际为准)
ls -la ../log/report.xlsx
```