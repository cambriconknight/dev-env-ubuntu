<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/vllm/llama3.1">
        <h1 align="center">基于vLLM_MLU的Meta-Llama-3.1系列模型验证教程</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 切换到vLLM推理环境](#2-切换到vllm推理环境)
- [3. Llama-3.1-8B	模型验证](#3-llama-31-8b模型验证)
  - [3.1. OpenAI测试](#31-openai测试)
  - [3.2. 性能测试](#32-性能测试)

# 1. 环境准备

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡       | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-6.2.10-15.el7.x86_64.rpm	        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| vLLM 项目代码         | https://github.com/vllm-project/vllm  | branch： v0.6.2       |
| vLLM_MLU 适配代码     | https://github.com/Cambricon/vllm_mlu  | 暂未开源，如有需要请联系官方技术支持人员 |
| Llama-3 项目代码  | https://github.com/meta-llama/llama3  | commit：d02f94e |
| Meta-Llama-3.1-8B-Instruct 模型   | https://modelscope.cn/models/LLM-Research/Meta-Llama-3.1-8B-Instruct	   | commit：d02f94e |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**参考资料:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 在线文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK下载: https://sdk.cambricon.com/download?component_name=PyTorch
- vLLM官方文档: https://docs.vllm.ai/en/latest/getting_started/quickstart.html
- OpenAI官网文档: https://platform.openai.com/docs/introduction

**AIKnight公众号**
>![](../../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/vllm
```
## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#加载Docker镜像
#./load-image-dev.sh ./docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#启动Docker容器
./run-container-dev.sh
```

# 2. 切换到vLLM推理环境

```bash
# 修改系统时区
cp -rvf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
```

# 3. Llama-3.1-8B	模型验证

## 3.1. OpenAI测试

本实例是基于OpenAI进行推理验证。OpenAI使用详见[OpenAI官网文档](https://platform.openai.com/docs/introduction)： https://platform.openai.com/docs/introduction 。

1. 启动服务

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/llama3.1/tools
# 根据实际环境，设置模型路径和服务端口号
export MODEL_PATH=/data/models/llm/models/Meta-Llama-3.1-8B-Instruct
export SERVER_PORT=12345
###############################
export MLU_VISIBLE_DEVICES=0,1
export MLU_TP_NUM=2
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
###############################
```

2. 启动客户端测试

*服务启动完成后，可另启一个终端，运行以下测试实例，用来测试API调用的效果。*
*修改测试脚本*
```bash
vim /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```
按照前文内容修改如下：
```bash
# Modify OpenAI's API key and API base to use vLLM's API server.
openai_api_key = "EMPTY"
openai_api_base = "http://localhost:12345/v1"
```
或直接拷贝已经修改好的文件替换原来文件
```bash
#备份
#cp -rvf /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client-bk.py
#替换
cp -rvf /home/share/pytorch2.5/vllm/llama3.1/tools/openai_chat_completion_client.py /workspace/vllm-v0.6.2/examples/openai_chat_completion_client.py
```

*启动client*
```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
cd /workspace/vllm-v0.6.2/examples
python openai_chat_completion_client.py
```

*测试结果*
```bash
(pytorch_infer) root@worker1:/workspace/vllm-v0.6.2/examples# python openai_chat_completion_client.py
Chat completion results:
ChatCompletion(id='chatcmpl-f48ce61dd62343ba90a4b205c8749493', choices=[Choice(finish_reason='stop', index=0, logprobs=None, message=ChatCompletionMessage(content='2020年的世界职业棒球大赛在美国多个城市进行，包括阿利兹, 亚利桑那州; 洛杉矶, 加利福尼亚州; 巴克莱中心, 内华达州; 和芝加哥, 伊利诺伊州.', refusal=None, role='assistant', audio=None, function_call=None, tool_calls=[]), stop_reason=None)], created=1738982582, model='/data/models/llm/models/Meta-Llama-3.1-8B-Instruct', object='chat.completion', service_tier=None, system_fingerprint=None, usage=CompletionUsage(completion_tokens=61, prompt_tokens=77, total_tokens=138, completion_tokens_details=None, prompt_tokens_details=None), prompt_logprobs=None)
```

## 3.2. 性能测试

**测试步骤**

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/llama3.1/tools
# 根据实际环境，设置模型路径和运行板卡
export MODEL_PATH=/data/models/llm/models/Meta-Llama-3.1-8B-Instruct
export MODEL_Name=Meta-Llama-3.1-8B-Instruct
###############################
#for 300
export MLU_VISIBLE_DEVICES=0,1
export MLU_TP_NUM=2
# 执行多组参数的性能测试脚本
./benchmark4vllm_8b_300.sh
###############################
# 查看性能测试结果
ls -la ../log/
```
测试完后，性能测试结果见【/home/share/pytorch2.5/vllm/llama3.1/log】

**汇总结果**

如测试多组性能数据后，可使用[一键导出脚本](./tools/export_testlog2excel.py)对其所有日志文件导出汇总到一个Excel文件中，方便对比性能数据，

```bash
# 进入工作目录
cd /home/share/pytorch2.5/vllm/llama3.1/tools
# 执行一键导出脚本，导出 Excel 格式的汇总数据。
python export_testlog2excel.py
# 查看文件(以下为默认文件，请以实际为准)
ls -la ../log/report.xlsx
```

*执行一键导出脚本实例：*

<p align="left">
    <img alt="Meta-Llama-3.1-8B-Instruct-benchmark-log2report-s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/vllm/llama3.1/res/Meta-Llama-3.1-8B-Instruct-benchmark-log2report-s.gif" width="640" />
</p>

<p align="left">
    <img alt="export_testlog2excel" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/vllm/llama3.1/res/export_testlog2excel.gif" width="640" />
</p>

*注：图中数据已做屏蔽处理，真实性能数据以实测为准。*
