#!/bin/bash
set -e
# -------------------------------------------------------------------------------
# Filename:     run-container-dev.sh
# Update:       2025/02/06
# Description:  Run docker image for IDE.
# Example:      ./run-container-dev.sh
# Depends:      container-${PATH_WORK}-${VERSION}-${Owner}
# Notes:
# -------------------------------------------------------------------------------
# Source env
source "./env.sh"
MY_CONTAINER_NEW="${MY_CONTAINER}"
######Modify according to your development environment#####
#Share path on the host
PATH_SHARE_HOST="$PWD/../"
#Share path on the docker container
PATH_SHARE_DOCKER="/home/share"
#Data path on the host
PATH_DATA_HOST="/data"
#Data path on the docker container
PATH_DATA_DOCKER="/data"

##########################################################
#Check docker images
[ ! -z $(docker images -q $NAME_IMAGE) ] || (echo -e "${red}ERROR: Images(${NAME_IMAGE}) does not exist! ${none}" &&  exit -1)

#Check docker container
num=`docker ps -a|grep -w "$MY_CONTAINER_NEW$"|wc -l`
echo $num
echo $MY_CONTAINER_NEW

#Run docker
if [ 0 -eq $num ];then
    #sudo xhost +
    docker run -e DISPLAY=unix$DISPLAY --privileged=true \
        --device /dev/cambricon_ctl \
        --device /dev/cambricon_ipcm0 \
        --net=host --ipc=host --pid=host \
        -v /usr/bin/cnmon:/usr/bin/cnmon \
        -v /sys/kernel/debug:/sys/kernel/debug \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v /mnt/data/opt/share:/opt/share \
        -p 8888:8888 \
        -p 5025:22 \
        -w $PATH_SHARE_DOCKER \
        -it -v $PATH_SHARE_HOST:$PATH_SHARE_DOCKER \
        -it -v $PATH_DATA_HOST:$PATH_DATA_DOCKER \
        --name $MY_CONTAINER_NEW $NAME_IMAGE /bin/bash
else
    docker start $MY_CONTAINER_NEW
    docker exec -ti $MY_CONTAINER_NEW /bin/bash
fi
