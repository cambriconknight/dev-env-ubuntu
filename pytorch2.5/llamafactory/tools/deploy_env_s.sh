#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     deploy_env_s.sh
# UpdateDate:   2025/02/25
# Description:  一键搭建验证环境。
# Example:      cd /home/share/pytorch2.5/llamafactory/tools/ && bash deploy_env_s.sh
# Depends:      commit id： 1481af5dc9bc99807ae0ee5a438bf0a279cafb66
# Notes:        本脚本适用于初次进入docker容器环境；不适用于再次进入容器，可能会有意想不到问题
# -------------------------------------------------------------------------------

## 下载代码&安装依赖库
cd /workspace && sleep 1 && git clone https://gitee.com/cambriconknight/LLaMA-Factory.git \
    && cd /workspace/LLaMA-Factory && git checkout 1481af5 && cd - \
    && cd /workspace/LLaMA-Factory && pip install -e ".[torch,metrics]"
#该环境可以获得更好的内存表现
export PYTORCH_MLU_ALLOC_CONF="expandable_segments:True"
#启动web界面，指定加速卡
#MLU_VISIBLE_DEVICES=0,1 llamafactory-cli webui

#安装依赖时【 pip install -e ".[metrics]" 】忽略以下报错信息：
#ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
#nni 2.10.1 requires filelock<3.12, but you have filelock 3.16.0 which is incompatible.
#torchdump 0.6.0 requires dill==0.3.4, but you have dill 0.3.8 which is incompatible.