
<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/llamafactory">
        <h1 align="center">LLaMA-Factory框架大模型在MLU上验证</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 模型下载](#2-模型下载)
- [3. 模型验证](#3-模型验证)
  - [3.1. Qwen2模型验证](#31-qwen2模型验证)
  - [3.2. Baichuan2 模型验证](#32-baichuan2-模型验证)
  - [3.3. DeepSeek-R1-Distill 系列模型验证](#33-deepseek-r1-distill-系列模型验证)
  - [3.4. Qwen2.5 系列模型验证](#34-qwen25-系列模型验证)

# 1. 环境准备

[LLaMA-Factory](https://github.com/hiyouga/LLaMA-Factory)是一个统一的框架，集成了一套先进的高效训练方法。它允许用户通过内置的Web UI灵活定制100多个LLMs的微调，而无需编写代码。本教程采用已配置并集成好的 **Docker 离线镜像**在MLU卡上进行模型的验证工作。用户仅需在已安装好驱动的MLU[硬件环境](#11-硬件环境)上，按照[软件环境](#12-软件环境)要求把准备好的 **Docker 离线镜像**和**待测模型权重**放置到指定位置，即可按照后续步骤进行各项功能测试与验证。

*注：本教程尤其适用于离线环境下，对被测网络模型进行快速功能验证。*

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡     | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                             | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-6.2.10-15.el7.x86_64.rpm	        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310-llamafactory.tar.gz     | **需自行下载**，环境已配置并集成好的 **Docker 离线镜像**文件；此镜像文件可关注微信公众号 【 AIKnight 】, 发送关键字 **llamafactory_offline_image** 自动获取； 镜像文件大约8G，请合理安排时间下载； |
| DeepSpeed-MLU         | deepspeed_mlu-0.11.0-py3-none-any.whl    | v0.11.0 联系官方技术支持人员获取              |
| LLaMA-Factory 源码       | https://github.com/hiyouga/LLaMA-Factory | commit: 1481af5            |
| DeepSeek-R1-Distill-Qwen-14B 模型   | [DeepSeek-R1-Distill-Qwen-14B 模型](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-14B)	   | commit： 6453600 |
| Qwen2.5-32B-Instruct 模型   | [Qwen2.5-32B-Instruct 模型](https://huggingface.co/Qwen/Qwen2.5-32B-Instruct)    | commit： 5ede1c9 |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu  | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

注：镜像中不包含大模型权重文件，验证前请自行下载后，放置到指定目录下【/data/models/】。

**技术参考:**
- 前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。
- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch
- LLaMA-Factory入门教程：https://zhuanlan.zhihu.com/p/695287607
- LLaMA-Factory框架文档：https://llamafactory.readthedocs.io/zh-cn/latest
- DeepSeek官网文档: https://api-docs.deepseek.com/zh-cn/news/news1226
- Qwen2.5参考教程：[Qwen2.5大语言模型的微调、评测与部署实践](https://help.aliyun.com/zh/pai/use-cases/deploy-fine-tune-and-evaluate-a-qwen2-5-model?spm=a2c4g.11186623.help-menu-30347.d_4_2_0.34a06ab48yIydh)；[DistilQwen2蒸馏小模型的训练、评测、压缩与部署](https://help.aliyun.com/zh/pai/use-cases/training-evaluation-compression-and-deployment-of-distilqwen2?spm=a2c4g.11186623.help-menu-30347.d_4_2_2.57fe5ae8BKwi3G)

**AIKnight公众号**
>![](../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
```
修改env.sh 脚本中环境变量【TestModel】值为【"-llamafactory"】：
```bash
cd /data/gitee/dev-env-ubuntu/pytorch2.5
vim env.sh
```
修改内容如下：
```bash
#TestModel=""
TestModel="-llamafactory"
```

## 1.4. 加载镜像

请提前下载好【**Docker 离线镜像**】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#加载Docker镜像
sudo ./load-image-dev.sh cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310-llamafactory.tar.gz
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
#启动Docker容器
./run-container-dev.sh
```

# 2. 模型下载

具体操作步骤均参考官网[LLaMA-Factory模型下载](https://github.com/hiyouga/LLaMA-Factory/blob/main/README_zh.md#%E6%A8%A1%E5%9E%8B)，下载模型到容器中指定目录【/data/models/】，或者自行下载后拷贝模型文件到此目录。


**模型下载**

可参考以下两种方式对各个模型进行下载：

```bash
# 下载 Qwen1.5 7B Chat模型到指定目录【/data/models/】，或者自行下载后拷贝到此目录。
cd /data/models/
# 安装git-lfs工具
apt install git-lfs
git lfs install
# 1> huggingface下载模型（时间较长）
git clone https://huggingface.co/Qwen/Qwen1.5-7B-Chat
cd Qwen1.5-7B-Chat && git checkout 294483a && cd -
# 2> 魔搭下载模型
#pip install modelscope
#from modelscope import snapshot_download
##model_dir = snapshot_download('Qwen/Qwen1.5-7B-Chat')
```

*注： 使用者需根据实际环境修改模型路径。建议保持容器内部路径不变，只通过修改容器启动脚本 [run-container-dev.sh](../run-container-dev.sh) 中的`容器外(物理机)实际模型存放路径`, 修改后使用[clean.sh](../clean.sh)脚本清理现有容器，再使用此启动脚本[run-container-dev.sh](../run-container-dev.sh) 重新启动容器*


**模型精度转换**

有些模型精度默认为bf16，如果产品不支持bf16，需要转换为fp16。请[参考这里](https://gitee.com/cambriconknight/dev-env-ubuntu/tree/v1.22.1/pytorch2.1/qwen1.5#23-%E7%B2%BE%E5%BA%A6%E8%BD%AC%E6%8D%A2)。


# 3. 模型验证

## 3.1. Qwen2模型验证

本节以 Qwen2-7B-Instruct 模型验证为例，其他规模模型验证类似；

- 1. 启动Web

```bash
#注意，在LLaMA-Factory路径下启动，否则找不到数据集
cd /workspace/LLaMA-Factory
#启动web页面
llamafactory-cli webui
#启动web页面，指定卡启动
#MLU_VISIBLE_DEVICES=0,1 llamafactory-cli webui
```
- 2. 配置参数

配置如下参数

<p align="left">
    <img alt="llamafactory-qwen2-7b-instruct-demo" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.1/llamafactory/res/llamafactory-qwen2-7b-instruct-demo.png" width="640" />
</p>

- 3. 启动验证

参数配置完成后，即可点击Web界面启动按钮。

<p align="left">
    <img alt="llamafactory-startwebui" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.1/llamafactory/res/llamafactory-startwebui.png" width="640" />
</p>
<p align="left">
    <img alt="llamafactory-qwen2-7b-instruct-demo" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.1/llamafactory/res/llamafactory-qwen2-7b-instruct-demo.gif" width="640" />
</p>

## 3.2. Baichuan2 模型验证

本节以 Baichuan2-7B-Chat 模型验证为例，其他规模模型验证类似；

- 1. 启动Web

```bash
#注意，在LLaMA-Factory路径下启动，否则找不到数据集
cd /workspace/LLaMA-Factory
#启动web页面
llamafactory-cli webui
#启动web页面，指定卡启动
#MLU_VISIBLE_DEVICES=0,1 llamafactory-cli webui
```
- 2. 配置参数

WebUI配置如下参数

<p align="left">
    <img alt="llamafactory-baichuan2-7b-chat-webui" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.1/llamafactory/res/llamafactory-baichuan2-7b-chat-webui.png" width="640" />
</p>

- 3. 启动验证

参数配置完成后，即可点击Web界面启动按钮。

<p align="left">
    <img alt="llamafactory-baichuan2-7b-chat-fp16-demo-cnmon" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.1/llamafactory/res/llamafactory-baichuan2-7b-chat-fp16-demo-cnmon.gif" width="640" />
</p>
<p align="left">
    <img alt="llamafactory-baichuan2-7b-chat-fp16-demo" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.1/llamafactory/res/llamafactory-baichuan2-7b-chat-fp16-demo.gif" width="640" />
</p>

## 3.3. DeepSeek-R1-Distill 系列模型验证

本节以 DeepSeek-R1-Distill-Qwen-14B 模型验证为例，其他规模模型验证类似；

- 1. 启动Web

```bash
#非必须，该环境可以获得更好的内存表现
#export PYTORCH_MLU_ALLOC_CONF="expandable_segments:True"
#1.模型转换（如已转可跳过这步）官网模型精度默认为bf16，如果产品不支持bf16，需要转换为fp16。
cd /home/share/pytorch2.5/llamafactory/tools
python convert_bf16_to_fp16.py --src_ckpt_path /data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B --dst_ckpt_path /data/models/llm/models/DeepSeek-R1-Distill-Qwen-14B-FP16
#2.安装 deepspeed_mlu ，请联系官方技术支持人员获取
#pip install deepspeed_mlu-0.11.0-py3-none-any.whl
#3.启动web页面。注意需在LLaMA-Factory路径下启动，否则找不到数据集
cd /workspace/LLaMA-Factory && llamafactory-cli webui
#启动web页面，指定卡启动
#MLU_VISIBLE_DEVICES=0,1,2,3 llamafactory-cli webui
#启动后打开浏览器，输入服务器对应的IP地址，http://192.168.0.110:7860
```

- 2. 配置参数

配置如下参数

<p align="left">
    <img alt="llamafactory-ds-7b-demo-1" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/llamafactory/res/llamafactory-ds-7b-demo-1.png" width="640" />
    <img alt="llamafactory-ds-7b-demo-2" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/llamafactory/res/llamafactory-ds-7b-demo-2.png" width="640" />
</p>

- 3. 启动验证

参数配置完成后，即可点击Web界面启动按钮。

<p align="left">
    <img alt="llamafactory-startwebui" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/llamafactory/res/llamafactory-startwebui-s.png" width="640" />
</p>
<p align="left">
    <img alt="llamafactory-ds-7b-demo-s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/llamafactory/res/llamafactory-ds-7b-demo-s.gif" width="640" />
</p>

**DeepSeek-R1-Distill Models**

| **Model** | **Base Model** | **Download** |
| :------------: | :------------: | :------------: |
| DeepSeek-R1-Distill-Qwen-1.5B  | [Qwen2.5-Math-1.5B](https://huggingface.co/Qwen/Qwen2.5-Math-1.5B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-1.5B)   |
| DeepSeek-R1-Distill-Qwen-7B  | [Qwen2.5-Math-7B](https://huggingface.co/Qwen/Qwen2.5-Math-7B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-7B)   |
| DeepSeek-R1-Distill-Llama-8B  | [Llama-3.1-8B](https://huggingface.co/meta-llama/Llama-3.1-8B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Llama-8B)   |
| DeepSeek-R1-Distill-Qwen-14B   | [Qwen2.5-14B](https://huggingface.co/Qwen/Qwen2.5-14B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-14B)   |
|DeepSeek-R1-Distill-Qwen-32B  | [Qwen2.5-32B](https://huggingface.co/Qwen/Qwen2.5-32B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-32B)   |
| DeepSeek-R1-Distill-Llama-70B  | [Llama-3.3-70B-Instruct](https://huggingface.co/meta-llama/Llama-3.3-70B-Instruct) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Llama-70B)   |

*注: 所有模型的最新更新都会在 Huggingface 率先发布。 ModelScope 和 WiseModel 由于没有与 Huggingface 同步，需要开发人员手动更新，可能会在 Huggingface 更新后一段时间内同步更新。*


## 3.4. Qwen2.5 系列模型验证

本节以 Qwen2.5-32B-Instruct 模型验证为例，其他规模模型验证类似；

- 1. 启动Web

```bash
#非必须，该环境可以获得更好的内存表现
#export PYTORCH_MLU_ALLOC_CONF="expandable_segments:True"
#1.模型转换（如已转可跳过这步）官网模型精度默认为bf16，如果产品不支持bf16，需要转换为fp16。
cd /home/share/pytorch2.5/llamafactory/tools
python convert_bf16_to_fp16.py --src_ckpt_path /data/models/llm/models/Qwen2.5-32B-Instruct --dst_ckpt_path /data/models/llm/models/Qwen2.5-32B-Instruct-FP16
#2.安装 deepspeed_mlu ，请联系官方技术支持人员获取
#pip install deepspeed_mlu-0.11.0-py3-none-any.whl
#3.启动web页面。注意需在LLaMA-Factory路径下启动，否则找不到数据集
cd /workspace/LLaMA-Factory && llamafactory-cli webui
#启动web页面，指定卡启动
#MLU_VISIBLE_DEVICES=0,1,2,3 llamafactory-cli webui
#启动后打开浏览器，输入服务器对应的IP地址，http://192.168.0.110:7860
```

- 2. 配置参数

配置如下参数

<p align="left">
    <img alt="llamafactory-qwen2_5-32b-instruct-demo-1" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/llamafactory/res/llamafactory-qwen2_5-32b-instruct-demo-1.png" width="640" />
    <img alt="llamafactory-qwen2_5-32b-instruct-demo-2" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/llamafactory/res/llamafactory-qwen2_5-32b-instruct-demo-2.png" width="640" />
</p>

- 3. 启动验证

参数配置完成后，即可点击Web界面启动按钮。

<p align="left">
    <img alt="llamafactory-startwebui" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/llamafactory/res/llamafactory-startwebui-s.png" width="640" />
</p>
<p align="left">
    <img alt="llamafactory-ds-7b-demo-s" src="https://gitee.com/cambriconknight/open-env-res/raw/master/dev-env-ubuntu/pytorch2.5/llamafactory/res/llamafactory-ds-7b-demo-s.gif" width="640" />
</p>

模型下载链接见下表：

| 名称                   | 模型文件                             | 备注                              |
| :-------------------- | :-------------------------------  | :---------------------------------- |
| Qwen2.5-7B-Instruct 模型    | https://huggingface.co/Qwen/Qwen2.5-7B-Instruct     | commit： bb46c15 |
| Qwen2.5-14B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-14B-Instruct    | commit： cf98f3b |
| Qwen2.5-32B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-32B-Instruct    | commit： 5ede1c9 |
| Qwen2.5-72B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-72B-Instruct    | commit： d3d9511 |
