#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     deploy_env_s.sh
# UpdateDate:   2025/03/19
# Description:  一键搭建验证环境。
# Example:      cd /home/share/pytorch2.5/evalscope/tools/ && bash deploy_env_s.sh
# Depends:      
# Notes:        本脚本适用于初次进入docker容器环境；不适用于再次进入容器，可能会有意想不到问题
# -------------------------------------------------------------------------------

## 安装依赖库
pip install 'evalscope[app,perf]' -U
pip install wandb
pip install gradio
pip install modelscope
