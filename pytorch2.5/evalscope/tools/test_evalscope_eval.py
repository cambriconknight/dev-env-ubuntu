import os
from evalscope import TaskConfig, run_task
from evalscope.constants import EvalType
from transformers import AutoTokenizer

# 环境变量处理
env_model_fullname = os.getenv('MODEL_PATH', '/data/models/llm/models/Qwen2.5-32B-Instruct')
env_max_tokens = int(os.getenv('MAX_TOTAL_TOKENS', '3800')) 

# 初始化 tokenizer
tokenizer = AutoTokenizer.from_pretrained(env_model_fullname)
#计算给定提示（prompt）的最大令牌数。目的是为了确保生成的文本不会超过环境允许的最大令牌数
def dynamic_max_tokens(prompt: str) -> int:
    input_ids = tokenizer.encode(prompt, return_tensors="pt")
    input_len = input_ids.shape[1]
    return max(0, env_max_tokens - input_len - 1000) 

task_config = TaskConfig(
    model=env_model_fullname,
    api_url='http://127.0.0.1:12345/v1',
    api_key='EMPTY',
    eval_type=EvalType.SERVICE,
    datasets=['math_500'],
    dataset_args={'math_500': {'few_shot_num': 0}},
    eval_batch_size=32,
    generation_config={
        'max_tokens': dynamic_max_tokens(""), # 动态设置 max_tokens
        'temperature': 0.6,
        'top_p': 0.95,
        'top_k': 40,
        'n': 1,
    },
    stream=True
)

if __name__ == "__main__":
    run_task(task_config)
