import os
from evalscope.perf.main import run_perf_benchmark

# 环境变量处理
env_model_fullname = os.getenv('MODEL_PATH', '/data/models/llm/models/Qwen2.5-32B-Instruct')
env_model_name = os.getenv('MODEL_Name', 'Qwen2.5-32B-Instruct')

task_cfg = {"url": "http://127.0.0.1:12345/v1/chat/completions/",
            "parallel": 16,
            "model": env_model_fullname,
            "number": 160,
            "api": "openai",
            "temperature": "0.9",
            "max_tokens": "1024",
            "attn_implementation": "eager",
            "name": env_model_name,
            "prompt": "写一个科幻小说，请开始你的表演"}
run_perf_benchmark(task_cfg)
