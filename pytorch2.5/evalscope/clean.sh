#!/bin/bash
set -e

#clean
rm -rvf ./log/*.log ./log/*.xlsx ./log/*.csv
rm -rvf ./log_vllm_mlu_*
rm -rvf ./outputs ./tools/outputs