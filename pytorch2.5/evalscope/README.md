<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/evalscope">
        <h1 align="center">基于EvalScope框架对模型进行快速评测</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 安装依赖库](#2-安装依赖库)
- [3. 启动vLLM服务](#3-启动vllm服务)
- [4. 性能压测](#4-性能压测)
  - [4.1. 性能压测指令](#41-性能压测指令)
  - [4.2. 性能压测结果可视化](#42-性能压测结果可视化)
- [5. 基准测试](#5-基准测试)
  - [5.1. 基准测试指令](#51-基准测试指令)
  - [5.2. 基准测试结果可视化](#52-基准测试结果可视化)
- [6. 保存镜像](#6-保存镜像)

# 1. 环境准备

EvalScope是[魔搭社区](https://modelscope.cn)官方推出的模型评测与性能基准测试框架，内置多个常用测试基准和评测指标；支持多种类型的模型评测，包括LLM、多模态LLM、embedding模型和reranker模型。本文以 **Qwen2.5-32B-Instruct** 和 **DeepSeek-R1-Distill-Qwen-32B** 模型为例，介绍如何在 **vLLM推理引擎**上使用 **EvalScope** 进行模型性能压测和基准测试。

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡       | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统                         |
| Driver                | cambricon-mlu-driver-centos7-6.*.*-10.x86_64.rpm        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| vLLM 项目代码         | https://github.com/vllm-project/vllm  | branch： vllm0.6.4.post1       |
| vLLM_MLU 适配代码     | https://github.com/Cambricon/vllm_mlu  | 暂未开源，如有需要请联系官方技术支持人员 |
| EvalScope 源码              | https://github.com/modelscope/evalscope  | [官网文档](https://evalscope.readthedocs.io/zh-cn/latest/get_started/introduction.html) |
| Qwen2.5-32B-Instruct 模型   | https://huggingface.co/Qwen/Qwen2.5-32B-Instruct    | commit： 5ede1c9 |
| DeepSeek-R1-Distill-Qwen-32B  | [Qwen2.5-32B](https://huggingface.co/Qwen/Qwen2.5-32B) | [HuggingFace](https://huggingface.co/deepseek-ai/DeepSeek-R1-Distill-Qwen-32B)   |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**参考资料:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 在线文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK下载: https://sdk.cambricon.com/download?component_name=PyTorch
- vLLM官方文档: https://docs.vllm.ai/en/latest/getting_started/quickstart.html
- OpenAI官网文档: https://platform.openai.com/docs/introduction
- EvalScope官网文档: https://evalscope.readthedocs.io/zh-cn/latest/get_started/introduction.html

**AIKnight公众号**
>![](../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库

```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/evalscope
```

## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#加载Docker镜像
#./load-image-dev.sh ./docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#启动Docker容器
./run-container-dev.sh
```

# 2. 安装依赖库

进入docker容器后，可在**训练环境**下，**安装EvalScope及依赖库**。

```bash
#安装可视化所需的依赖，包括gradio、plotly等。
#pip install 'evalscope[app]' -U
pip install 'evalscope[app,perf]' -U
#安装依赖（可使用wandb记录测试结果）
#pip install evalscope[perf] -U
pip install wandb
pip install gradio
```

# 3. 启动vLLM服务

本实例是基于vLLM推理引擎进行性能压测。进入docker容器后，进入**推理环境**，启动vLLM服务。

```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
# 进入工作目录
cd /home/share/pytorch2.5/evalscope/tools/
# 根据实际环境，设置模型路径和服务端口号
## Qwen2.5-32B-Instruct
export MODEL_PATH=/data/models/llm/models/Qwen2.5-32B-Instruct
## DeepSeek-R1-Distill-Qwen-32B
#export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Qwen-32B
export SERVER_PORT=12345
###############################
export MLU_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
export MLU_TP_NUM=8
# 一键启动vLLM OpenAI APIServer
./run_openai_api_server_300.sh
###############################
```

# 4. 性能压测

进入docker容器后，需在**已安装EvalScope及依赖库的训练环境**下进行 EvalScope 性能压测。

## 4.1. 性能压测指令
```bash
# 进入工作目录
cd /home/share/pytorch2.5/evalscope/tools/
# 根据实际环境，设置模型路径
## Qwen2.5-32B-Instruct
export MODEL_PATH=/data/models/llm/models/Qwen2.5-32B-Instruct
## DeepSeek-R1-Distill-Qwen-32B
#export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Qwen-32B
export MODEL_Name=DeepSeek-R1-Distill-Qwen-32B
# 1.命令行启动：性能压测（4卡16并发）
evalscope perf \
  --url 'http://127.0.0.1:12345/v1/chat/completions' \
  --parallel 16 \
  --model ${MODEL_PATH} \
  --log-every-n-query 10 \
  --number 160 \
  --api openai \
  --temperature 0.9 \
  --max-tokens 1024 \
  --attn-implementation eager \
  --name ${MODEL_Name} \
  --prompt '写一个科幻小说，请开始你的表演'

###############################
# 2.Python脚本启动：也可直接使用 python 脚本进行性能压测
cd /home/share/pytorch2.5/evalscope/tools/
python test_evalscope_perf.py
###############################
# 查看性能测试结果: 详细输出结果json文件及日志将会输出在运行命令的当前目录下的 'outputs' 处，文件按照日期时间命名。
ls -la ./outputs/
```

- 性能压测参数说明：https://evalscope.readthedocs.io/zh-cn/latest/user_guides/stress_test/parameters.html
- 性能压测使用示例：https://evalscope.readthedocs.io/zh-cn/latest/user_guides/stress_test/examples.html
- 性能压测指标说明：https://evalscope.readthedocs.io/zh-cn/latest/user_guides/stress_test/quick_start.html#id5

## 4.2. 性能压测结果可视化

性能压测结果可视化说明：https://evalscope.readthedocs.io/zh-cn/latest/user_guides/stress_test/examples.html#wandb

*注：记录默认会上传到wandb官网，如有敏感数据，勿用！！！*

1、安装wandb：

```bash
pip install wandb
```

2、官网注册并登录

wandb安装完成后，我们需要在【[官网](https://wandb.ai/)】 注册一个账号并复制下自己的API keys，然后在本地使用下面【wandb login】命令登录。

```bash
wandb login
```
*注：官网： https://wandb.ai*

3、在启动命令中，添加以下参数（注意wandb-api-key是你自己的，名称自定义），再次启动压测：

```bash
--wandb-api-key 'wandb_api_key' # 需在官网注册一个账号并复制下自己的API keys
--name 'name_of_wandb_log'      # 名称自定义
```

压测完成后记录会上传到wandb官网账户，在官网账户可点击左上角的 project 查看数据如下：
<p align="left">
    <img alt="wandb_sample" src="https://modelscope.oss-cn-beijing.aliyuncs.com/resource/wandb_sample.png" width="640" />
</p>

# 5. 基准测试

## 5.1. 基准测试指令

```bash
# 进入工作目录
cd /home/share/pytorch2.5/evalscope/tools/
# 根据实际环境，设置模型路径
## Qwen2.5-32B-Instruct
export MODEL_PATH=/data/models/llm/models/Qwen2.5-32B-Instruct
## DeepSeek-R1-Distill-Qwen-32B
#export MODEL_PATH=/data/models/llm/models/DeepSeek-R1-Distill-Qwen-32B
# 1.命令行启动：基准测试命令 gsm8k math_500
evalscope eval \
  --model ${MODEL_PATH} \
  --api-url  http://127.0.0.1:12345/v1 \
  --api-key EMPTY \
  --eval-type service \
  --datasets gsm8k math_500 \
  --dataset-args '{"gsm8k": {"few_shot_num": 0, "few_shot_random": false}, "math_500": {"few_shot_num": 0, "few_shot_random": false}, "ifeval": {"filters": {"remove_until": "</think>"}}}' \
  --limit 10

###############################
# 2.Python脚本启动：也可直接使用 python 脚本进行基准测试 math_500
cd /home/share/pytorch2.5/evalscope/tools/
python test_evalscope_eval.py
###############################
# 查看性能测试结果: 详细输出结果json文件及日志将会输出在运行命令的当前目录下的 'outputs' 处，文件按照日期时间命名。
ls -la ./outputs/
```

- `evalscope eval`命令参数说明：https://evalscope.readthedocs.io/zh-cn/latest/get_started/parameters.html
- 支持的数据集参考数据集列表：https://evalscope.readthedocs.io/zh-cn/latest/get_started/supported_dataset.html

## 5.2. 基准测试结果可视化

EvalScope基准测试结果也支持可视化，可以通过Web浏览器查看模型基准测试的具体输出。

*注：基准测试可视化说明：https://evalscope.readthedocs.io/zh-cn/latest/get_started/visualization.html*

```bash
#安装可视化所需的依赖，包括gradio、plotly等。
pip install 'evalscope[app]' -U
#运行以下命令，可以启动可视化界面：
cd /home/share/pytorch2.5/evalscope/tools/
evalscope app --lang zh
```

`evalscope app`参数说明：https://evalscope.readthedocs.io/zh-cn/latest/get_started/visualization.html

输出如下内容即可在浏览器中【 http://192.168.0.110:7860 】访问可视化服务。

```bash
* Running on local URL:  http://0.0.0.0:7860

To create a public link, set `share=True` in `launch()`.
```
<p align="left">
    <img alt="collection" src="https://evalscope.readthedocs.io/zh-cn/latest/_images/collection.png" width="640" />
</p>

# 6. 保存镜像

执行`一键环境搭建脚本`，完成基础环境搭建。之后再另起终端在Host主机环境下，执行`一键保存镜像脚本`，保存当前容器镜像。如有需要还可使用`一键压缩脚本`压缩镜像文件。

1. 环境准备：为了最小化镜像，可在首次进入容器后，执行一键环境搭建脚本，完成基础环境搭建。

```bash
# 以下操作均在Docker容器中进行，执行一键适配脚本。
cd /home/share/pytorch2.5/evalscope/tools/ && bash deploy_env_s.sh
```

2. 保存镜像：保存当前容器镜像供后续**环境变更后**或**离线环境下**快速进行测试验证。验证的步骤，详见教程 [README4OfflineImages.md](./README4OfflineImages.md)。

```bash
#另起终端在Host主机环境下，保存当前容器镜像。以下工作目录需根据实际环境进行修改。
cd /data/gitee/dev-env-ubuntu/pytorch2.5
./save-image-dev.sh evalscope
```

3. 压缩镜像：如果感觉镜像文件太大，不方便后期传输。可参考以下步骤，对镜像文件进行压缩并记录md5值：

```bash
#在host主机环境下，使用gzip压缩镜像文件。以下工作目录需根据实际环境进行修改。
cd /data/gitee/dev-env-ubuntu/pytorch2.5
bash ./gzip.sh evalscope
```

