<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/pytorch2.5/qwen2.5-vl">
        <h1 align="center">QWen2.5-VL模型验证教程</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载仓库](#13-下载仓库)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. 安装依赖库](#2-安装依赖库)
- [3. 模型推理验证](#3-模型推理验证)

# 1. 环境准备

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| AI加速卡       | 8卡       | 需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统 |
| Driver                | cambricon-mlu-driver-centos7-6.*.*-10.x86_64.rpm        | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)  |
| Docker Image          | cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz     | 官方镜像 [下载地址](https://sdk.cambricon.com/download?sdk_version=V1.15.0&component_name=PyTorch)或联系官方技术支持人员 |
| Qwen2.5-VL 源码              | https://github.com/QwenLM/Qwen2.5-VL  | commit： 4358bd4 |
| Qwen2.5-VL-3B-Instruct 模型   | https://modelscope.cn/models/Qwen/Qwen2.5-VL-3B-Instruct    | commit： cc9b0f0 |
| Qwen2.5-VL-7B-Instruct 模型   | https://modelscope.cn/models/Qwen/Qwen2.5-VL-7B-Instruct    | commit： 78d6bca |
| Qwen2.5-VL-72B-Instruct 模型   | https://modelscope.cn/models/Qwen/Qwen2.5-VL-72B-Instruct    | commit： b7e6e2f |
| 验证工具包  | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**参考资料:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 在线文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK下载: https://sdk.cambricon.com/download?component_name=PyTorch
- Qwen2.5-VL 官方技术博客：https://qwenlm.github.io/zh/blog/qwen2.5-vl

**AIKnight公众号**
>![](../../res/aiknight_wechat_172.jpg)

## 1.3. 下载仓库
```bash
#进入裸机工作目录，以【/data/gitee】工作目录为例
cd /data/gitee
#下载仓库
git clone --depth=1 https://gitee.com/cambriconknight/dev-env-ubuntu.git
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5
```
## 1.4. 加载镜像

请提前下载好【Docker镜像】，方便以下操作加载使用。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#加载Docker镜像
#./load-image-dev.sh ./docker/cambricon_pytorch_container-v25.01-torch2.5.0-torchmlu1.24.1-ubuntu22.04-py310.tar.gz
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器

镜像加载完成后，运行脚本，进入Docker容器。

```bash
#进入【工具包目录】
cd ./dev-env-ubuntu/pytorch2.5/
#启动Docker容器
./run-container-dev.sh
```

# 2. 安装依赖库

```bash
#切换到推理环境
source /torch/venv3/pytorch_infer/bin/activate
mkdir -p /workspace/third_modules && cd /workspace/third_modules
pip uninstall -y transformers
cd /workspace/third_modules/transformers && pip install -e .
pip install qwen_vl_utils
```

# 3. 模型推理验证

```bash
# 进入工作目录
cd /home/share/pytorch2.5/qwen2.5-vl/tools/
# Qwen2.5-VL-3B-Instruct
export MLU_VISIBLE_DEVICES=0
python demo.py /data/AE/llm/models/Qwen/Qwen2.5-VL-3B-Instruct
# Qwen2.5-VL-7B-Instruct
export MLU_VISIBLE_DEVICES=0,1
python demo.py /data/AE/llm/models/Qwen/Qwen2.5-VL-7B-Instruct
# Qwen2.5-VL-72B-Instruct
export MLU_VISIBLE_DEVICES=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
python demo.py /data/AE/llm/models/Qwen/Qwen2.5-VL-72B-Instruct
```
