# 1. PyTorch概述

PyTorch是一款开源的机器学习编程框架，提供了Python和C++编程接口，主要用于计算机视觉、自然语言处理等领域应用的开发，具有轻松扩展、快速实现、生产部署稳定性强等优点。

PyTorch主要提供了以下功能：

- 加速Tensor计算（类似NumPy）
- 基于自动微分系统构建深度神经网络

(关于PyTorch的更多内容，请参见[PyTorch官方文档](https://pytorch-cn.readthedocs.io/zh/latest/)。)

# 2. Cambricon PyTorch概述
为使PyTorch支持寒武纪MLU，寒武纪对机器学习框架PyTorch进行了部分定制（以下简称Cambricon PyTorch）：

- 增加MLU DispatchKey，以支持后续为PyTorch扩展MLU后端。
- 对PyTorch某些功能模块打patch（如Profiler、DDP、Storage等），以支持MLU后端。

若要在寒武纪MLU上运行PyTorch，需要安装并使用寒武纪定制的Cambricon PyTorch。

# 3. Cambricon CATCH概述
Cambricon CATCH(Cambricon Accelerated TorCH)是寒武纪发布的一款Python包（包名torch_mlu），提供了在MLU设备上进行张量计算的能力。Cambricon PyTorch主要为原生PyTorch增加了MLU DispatchKey，使得MLU DispatchKey可以融入PyTorch的Dispatcher系统，而Cambricon CATCH则是利用PyTorch Dispatcher系统提供的动态扩展能力，将MLU张量计算库通过MLU DispatchKey注册到PyTorch Dispatcher系统中，以插件的形式动态支持PyTorch在MLU设备上运行。

安装好Cambricon CATCH后（具体流程请参见 [源码编译安装](https://www.cambricon.com/docs/sdk_1.15.0/cambricon_pytorch_1.17.0/user_guide_1.13/pytorch_4_installation/Pytorch_installation.html#id3) 章节），便可使用torch_mlu模块：
```bash
import torch # 需安装Cambricon PyTorch
import torch_mlu # 动态扩展MLU后端
t0 = torch.randn(2, 2, device='mlu') # 在MLU设备上生成Tensor
t1 = torch.randn(2, 2, device='mlu')
result = t0 + t1 # 在MLU设备上完成加法运算
```
import torch_mlu 之后，在PyTorch中便可像使用其他设备一样使用MLU。

# 4. 参考资料

- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch