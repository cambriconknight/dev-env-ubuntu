

<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/paddle">
        <h1 align="center">基于Paddle的网络模型验证环境搭建</h1>
    </a>
</p>

**该教程仅仅用于学习，打通流程； 不对效果负责，不承诺商用。**

# 1. 概述
本目录下脚本适配于官方发布的 PaddlePaddle 框架的 Docker 容器。[Paddle MLU 版](https://www.paddlepaddle.org.cn/documentation/docs/zh/guides/hardware_support/mlu_docs/index_cn.html)当前可以支持在 MLU 系列板卡上进行模型训练。[Paddle Lite](https://www.paddlepaddle.org.cn/lite/v2.12/demo_guides/cambricon_mlu.html) 已支持 MLU 系列板卡在 X86 服务器上进行预测部署。


**运行环境**

- 主机系统: CentOS7
- 软件栈版本: 1.18.0
- 深度学习框架: PaddlePaddle
- 镜像: registry.baidubce.com/device/paddle-mlu:ctr2.15.0-ubuntu20-gcc84-py310
- Docker: ⽤⼾需要⾃⾏安装docker（版本要求 >= v19.03）

**硬件环境准备:**

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| MLU370        | 一套       |使用板卡自带的8pin连接器连接主机电源|

**软件环境准备:**

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统                         |
| Driver_MLU370         | cambricon-mlu-driver-centos7-5.10.34-1.x86_64.rpm        | 依操作系统选择                         |

注: 以上软件环境中文件名词, 如有版本升级及名称变化, 可以在 [env.sh](./env.sh) 中进行修改。

**下载地址:**

前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。

- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch
- Paddle MLU 版训练使用指南： https://www.paddlepaddle.org.cn/documentation/docs/zh/guides/hardware_support/mlu_docs/index_cn.html
- Paddle Lite 推理使用指南： https://www.paddlepaddle.org.cn/lite/v2.12/demo_guides/cambricon_mlu.html

# 2. 网络图谱
Paddle 环境下典型网络的验证教程.
|网络名称|操作目录|备注|
|----|-------|-------|
| **resnet50_vd** | [resnet50_vd](./resnet50_vd)  | [图像分类-ResNet](https://www.paddlepaddle.org.cn/modelbasedetail/resnet)  |

# 3. 下载镜像

```bash
#下载Docker镜像
#docker pull registry.baidubce.com/device/paddle-mlu:ctr2.15.0-ubuntu20-gcc84-py310
./pull-image-dev.sh
```

# 4. 加载镜像
```bash
#加载Docker镜像
./load-image-dev.sh ${FULLNAME_IMAGES}
```

# 5. 启动容器
```bash
#启动容器
./run-container-dev.sh
```
**运行实例**
```bash
[username@worker1~/nfs/gitee/dev-env-ubuntu/paddle]$sudo ./load-image-dev.sh
WARNING: Load images(./docker/paddle-mlu-dev-ctr2.15.0-ubuntu20-gcc84-py310.tar.gz) by default.
ERROR: Images(./docker/paddle-mlu-dev-ctr2.15.0-ubuntu20-gcc84-py310.tar.gz) does not exist!
[username@worker1~/nfs/gitee/dev-env-ubuntu/paddle]$sudo ./run-container-dev.sh
0
container-paddle-mlu-dev-ctr2.15.0-kang
WARNING: Published ports are discarded when using host network mode
λ worker1 /home/share
```

# 6. 保存镜像
```bash
#保存镜像，供后续环境变更直接使用。
./save-image-dev.sh $Suffix_FILENAME
```
