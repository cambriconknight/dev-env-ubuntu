
<p align="center">
    <a href="https://gitee.com/cambriconknight/dev-env-ubuntu/tree/master/paddle/resnet50_vd">
        <h1 align="center">基于Paddle框架的Resnet50_vd模型验证教程</h1>
    </a>
</p>

**该教程仅用于学习，打通流程； 不对效果负责，不承诺商用。**

- [1. 环境准备](#1-环境准备)
  - [1.1. 硬件环境](#11-硬件环境)
  - [1.2. 软件环境](#12-软件环境)
  - [1.3. 下载镜像](#13-下载镜像)
  - [1.4. 加载镜像](#14-加载镜像)
  - [1.5. 启动容器](#15-启动容器)
- [2. Paddle 安装](#2-paddle-安装)
- [3. Paddle 推理](#3-paddle-推理)

# 1. 环境准备

图像分类是计算机视觉的重要领域，它的目标是将图像分类到预定义的标签。本文将介绍如何在MLU上使用PaddlePaddle进行图像分类模型。

## 1.1. 硬件环境

| 名称           | 数量      | 备注                  |
| :------------ | :--------- | :------------------ |
| 服务器         | 一台       | 采用已完成适配的服务器；PCIe Gen.4 x16 |
| MLU370-X8     | 8卡       | X8需使用板卡自带的8pin连接器连接主板电源 |

## 1.2. 软件环境

| 名称                   | 版本/文件                                                 | 备注                                 |
| :-------------------- | :-------------------------------                         | :---------------------------------- |
| Linux OS              | CentOS7                                                  | 宿主机操作系统                         |
| Driver_MLU370         | cambricon-mlu-driver-centos7-5.10.31-1.x86_64.rpm         | 依操作系统选择，[驱动安装教程](https://sdk.cambricon.com/download?component_name=Driver)                         |
| Docker Image          | registry.baidubce.com/device/paddle-mlu:ctr2.15.0-ubuntu20-gcc84-py310    | 从飞桨镜像库拉取编译镜像 |
| Resnet50_vd 模型      | [预训练模型下载地址](https://paddle-imagenet-models-name.bj.bcebos.com/dygraph/legendary_models/ResNet50_vd_pretrained.pdparams) ; [inference模型下载地址](https://paddle-imagenet-models-name.bj.bcebos.com/dygraph/inference/ResNet50_vd_infer.tar) |  |
| 验证工具包             | https://gitee.com/cambriconknight/dev-env-ubuntu        | [下载地址](https://gitee.com/cambriconknight/dev-env-ubuntu) |

**下载地址:**
- 前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载， 也可在官方提供的专属FTP账户指定路径下载。
- 文档: https://developer.cambricon.com/index/document/index/classid/3.html
- SDK: https://sdk.cambricon.com/download?component_name=PyTorch
- ImageNet 预训练模型库: https://github.com/PaddlePaddle/PaddleClas/blob/release/2.5/docs/zh_CN/models/ImageNet1k/model_list.md

**AIKnight公众号**
>![](../../res/aiknight_wechat_172.jpg)


## 1.3. 下载镜像

```bash
#下载Docker镜像
#docker pull registry.baidubce.com/device/paddle-mlu:ctr2.15.0-ubuntu20-gcc84-py310
./pull-image-dev.sh
```

## 1.4. 加载镜像
```bash
#加载Docker镜像
./load-image-dev.sh ${FULLNAME_IMAGES}
```

## 1.5. 启动容器
```bash
#启动容器
./run-container-dev.sh
```
**运行实例**
```bash
[username@worker1~/nfs/gitee/dev-env-ubuntu/paddle]$sudo ./load-image-dev.sh
WARNING: Load images(./docker/paddle-mlu-dev-ctr2.15.0-ubuntu20-gcc84-py310.tar.gz) by default.
ERROR: Images(./docker/paddle-mlu-dev-ctr2.15.0-ubuntu20-gcc84-py310.tar.gz) does not exist!
[username@worker1~/nfs/gitee/dev-env-ubuntu/paddle]$sudo ./run-container-dev.sh
0
container-paddle-mlu-dev-ctr2.15.0-kang
WARNING: Published ports are discarded when using host network mode
λ worker1 /home/share
```

# 2. Paddle 安装

以下操作均在Docker容器中进行。

```bash
#进到容器后，创建并切换到工作目录
mkdir -p /workspace/paddle/resnet50_vd && cd /workspace/paddle/resnet50_vd
#下载已编译好的 paddle custom 插件包到 /home/share/dependent_files：
#paddle_custom_mlu-0.0.0-cp310-cp310-linux_x86_64.whl
#paddle_custom_mlu-0.0.0-cp310-cp310-linux_x86_64.whl.md5sum

#1. paddle cpu 主框架安装
python -m pip install paddlepaddle==2.6.1 -i https://pypi.tuna.tsinghua.edu.cn/simple

#2. Paddle custom 插件安装
pip install /home/share/dependent_files/paddle_custom_mlu-0.0.0-cp310-cp310-linux_x86_64.whl
```

# 3. Paddle 推理

按照如下步骤，以官方 Resnet50_vd inference 模型为例，进行 Paddle 框架推理。

```bash
# 1.PaddleClas 开放套件下载
cd /workspace/paddle/resnet50_vd
#git clone --depth 1 -b release/2.5.1 https://github.com/PaddlePaddle/PaddleClas.git
git clone --depth 1 -b release/2.5.1 https://gitee.com/paddlepaddle/PaddleClas.git

# 2. 环境依赖安装
cd PaddleClas
pip install -r requirements.txt

# 3.准备 ImageNet2012 数据集
mkdir -p /workspace/paddle/resnet50_vd/PaddleClas/dataset/ILSVRC2012
cd /workspace/paddle/resnet50_vd/PaddleClas/dataset/ILSVRC2012

ln -s /data/datasets/imagenet_training/train ./train
ln -s /data/datasets/ImageNet2012/train_shuffled.txt train_shuffled.txt
ln -s train_shuffled.txt train_list.txt

ln -s /data/datasets/imagenet_training/val ./val
ln -s /data/datasets/ImageNet2012/val_shuffled.txt val_shuffled.txt
ln -s val_shuffled.txt val_list.txt

# 4.准备预测模型
#方式一：可根据 https://github.com/PaddlePaddle/PaddleClas/blob/release/2.3/docs/zh_CN/inference_deployment/export_model.md 文档导出 Inference 模型
cd PaddleClas
python tools/export_model.py \
    -c ppcls/configs/ImageNet/ResNet/ResNet50_vd.yaml \
    -o Global.pretrained_model=./output/ResNet50_vd/latest \
    -o Global.save_inference_dir=./deploy/models/ResNet50_vd_infer \
    -o Global.device=mlu
#方式二：直接获取官方Resnet50_vd inference 模型
cd /workspace/paddle/resnet50_vd/PaddleClas
wget https://paddle-imagenet-models-name.bj.bcebos.com/dygraph/inference/ResNet50_vd_infer.tar
tar -xvf ResNet50_vd_infer.tar

# 5.下载 PaddleSlim 代码
#git clone https://github.com/PaddlePaddle/PaddleSlim.git
git clone https://gitee.com/PaddlePaddle/PaddleSlim.git
#添加 Device 设备
cd PaddleSlim && git fetch origin pull/1770/head && git checkout FETCH_HEAD && cd -

# 6.预测部署
cd /workspace/paddle/resnet50_vd/PaddleClas/PaddleSlim/example/auto_compression/image_classification
python paddle_inference_eval.py \
        --model_path=/workspace/paddle/resnet50_vd/PaddleClas/ResNet50_vd_infer \
        --data_path=/workspace/paddle/resnet50_vd/PaddleClas/dataset/ILSVRC2012 \
        --use_custom_device=True
```
**数据集目录结构如下:**
```bash
λ worker1 /workspace/paddle/resnet50_vd/PaddleClas/dataset/ILSVRC2012 {release/2.5.1} ls -la
total 8
drwxr-xr-x 2 root root 4096 May 30 19:06 ./
drwxr-xr-x 3 root root 4096 May 30 18:58 ../
lrwxrwxrwx 1 root root   38 May 30 19:00 train -> /data/datasets/imagenet_training/train
lrwxrwxrwx 1 root root   18 May 30 19:03 train_list.txt -> train_shuffled.txt
lrwxrwxrwx 1 root root   46 May 30 19:02 train_shuffled.txt -> /data/datasets/ImageNet2012/train_shuffled.txt
lrwxrwxrwx 1 root root   36 May 30 19:06 val -> /data/datasets/imagenet_training/val
lrwxrwxrwx 1 root root   16 May 30 19:06 val_list.txt -> val_shuffled.txt
lrwxrwxrwx 1 root root   44 May 30 19:06 val_shuffled.txt -> /data/datasets/ImageNet2012/val_shuffled.txt
λ worker1 /workspace/paddle/resnet50_vd/PaddleClas/dataset/ILSVRC2012 {release/2.5.1}
```
**推理实例如下：**
```bash
λ worker1 /workspace/paddle/resnet50_vd/PaddleClas/PaddleSlim/example/auto_compression/image_classification {(0c08712...)} python paddle_inference_eval.py \
>         --model_path=/workspace/paddle/resnet50_vd/PaddleClas/ResNet50_vd_infer \
>         --data_path=/workspace/paddle/resnet50_vd/PaddleClas/dataset/ILSVRC2012 \
>         --use_custom_device=True
I0531 15:24:17.564503 74515 init.cc:233] ENV [CUSTOM_DEVICE_ROOT]=/usr/local/lib/python3.10/dist-packages/paddle_custom_device
I0531 15:24:17.564548 74515 init.cc:142] Try loading custom device libs from: [/usr/local/lib/python3.10/dist-packages/paddle_custom_device]
I0531 15:24:17.777717 74515 custom_device.cc:1108] Successed in loading custom runtime in lib: /usr/local/lib/python3.10/dist-packages/paddle_custom_device/libpaddle-custom-mlu.so
I0531 15:24:17.786226 74515 custom_kernel.cc:63] Successed in loading 260 custom kernel(s) from loaded lib(s), will be used like native ones.
I0531 15:24:17.786342 74515 init.cc:154] Finished in LoadCustomDevice with libs_path: [/usr/local/lib/python3.10/dist-packages/paddle_custom_device]
I0531 15:24:17.786415 74515 init.cc:239] CustomDevice: mlu, visible devices count: 16
I0531 15:24:18.270903 74515 analysis_predictor.cc:1626] MKLDNN is enabled
I0531 15:24:18.270958 74515 analysis_predictor.cc:1658] CustomDevice is enabled
--- Running analysis [ir_graph_build_pass]
I0531 15:24:18.282105 74515 executor.cc:187] Old Executor is Running.
--- Running analysis [ir_analysis_pass]
I0531 15:24:18.354241 74515 ir_analysis_pass.cc:46] argument has no fuse statis
--- Running analysis [save_optimized_model_pass]
--- Running analysis [ir_params_sync_among_devices_pass]
I0531 15:24:18.354424 74515 ir_params_sync_among_devices_pass.cc:144] Sync params from CPU to mlu:0
--- Running analysis [adjust_cudnn_workspace_size_pass]
--- Running analysis [inference_op_replace_pass]
--- Running analysis [memory_optimize_pass]
I0531 15:24:23.289978 74515 memory_optimize_pass.cc:118] The persistable params in main graph are : 97.972MB
I0531 15:24:23.302870 74515 memory_optimize_pass.cc:246] Cluster name : batch_norm_48.tmp_0  size: 8192
I0531 15:24:23.302922 74515 memory_optimize_pass.cc:246] Cluster name : inputs  size: 602112
I0531 15:24:23.302946 74515 memory_optimize_pass.cc:246] Cluster name : relu_1.tmp_0  size: 3211264
I0531 15:24:23.302991 74515 memory_optimize_pass.cc:246] Cluster name : batch_norm_12.tmp_2  size: 3211264
I0531 15:24:23.303009 74515 memory_optimize_pass.cc:246] Cluster name : relu_0.tmp_0  size: 3211264
I0531 15:24:23.303033 74515 memory_optimize_pass.cc:246] Cluster name : conv2d_67.tmp_0  size: 3211264
--- Running analysis [ir_graph_to_program_pass]
I0531 15:24:23.433363 74515 analysis_predictor.cc:1838] ======= optimize end =======
I0531 15:24:23.436587 74515 naive_executor.cc:200] ---  skip [feed], feed -> inputs
I0531 15:24:23.439878 74515 naive_executor.cc:200] ---  skip [save_infer_model/scale_0.tmp_1], fetch -> fetch
[2024-5-31 15:24:23] [CNNL] [Warning]:[cnnlGetConvolutionForwardAlgorithm] is deprecated and will be removed in the future release. See cnnlFindConvolutionForwardAlgorithm() API for replacement.
[2024-5-31 15:24:23] [CNNL] [Warning]:When calculating multiplication of complex_float data, it is required to use [cnnlGetOpTensorWorkspaceSize_v2] to apply for workspace.
[2024-5-31 15:24:23] [CNNL] [Warning]:[cnnlAdaptivePoolingForward] is deprecated and will be removed in the future release, please use [cnnlAdaptivePoolingForward_v2] instead.
Eval iter: 0
Eval iter: 100
Eval iter: 200
Eval iter: 300
......
......
......
Eval iter: 49200
Eval iter: 49300
Eval iter: 49400
Eval iter: 49500
Eval iter: 49600
Eval iter: 49700
Eval iter: 49800
Eval iter: 49900
[Benchmark]Paddle       FP32    batch size: 1.Inference time(ms): min=18.03, max=76.3, avg=35.0
[Benchmark] Evaluation acc result: 0.79046
λ worker1 /workspace/paddle/resnet50_vd/PaddleClas/PaddleSlim/example/auto_compression/image_classification {(0c08712...)}
```
