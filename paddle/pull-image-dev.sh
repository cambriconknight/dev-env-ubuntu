#!/bin/bash
set -e
# -------------------------------------------------------------------------------
# Filename:     pull-image-dev.sh
# Update:       2024/03/18
# Description:  pulling docker image for IDE.
# Example:      ./pull-image-dev.sh
# Depends:      ${NAME_IMAGE}
# Notes:
# -------------------------------------------------------------------------------
# Source env
source "./env.sh"
#################### main ####################
# 1) 拉取镜像，注意此镜像仅为开发环境，镜像中不包含预编译的飞桨安装包
#    此镜像的构建脚本与 dockerfile 位于 tools/dockerfile 目录下
docker pull $NAME_IMAGE
